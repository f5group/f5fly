<?php
$sessid = session_id();
$_SESSION['time']     = time();
date_default_timezone_set('Asia/Yekaterinburg');
if(empty($sessid)) session_start();
require('rb.php'); R::setup('sqlite:/' . __DIR__ . '/f5db.sl3'); $mngmt = R::dispense( 'f5opt' );
if ( !R::testConnection() ) { exit('error db connection'); }
$f5srv_ipv4 = R::getCell( 'select value from f5opt WHERE id=3'); $f5srv_port = R::getCell( 'select value from f5opt WHERE id=4');

set_time_limit(0);
// include the web sockets server script (the server is started at the far bottom of this file)
require 'class.PHPWebSocket.php';

// when a client sends data to the server
function wsOnMessage($clientID, $message, $messageLength, $binary) {
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	// check if message length is 0
	if ($messageLength == 0) {
		$Server->wsClose($clientID);
		return;
	}

	//The speaker is the only person in the room. Don't let them feel lonely.
	if ( sizeof($Server->wsClients) == 1 )
		$Server->wsSend($clientID, "clients count is 1");
		//	if ( sizeof($Server->wsClients) == 2 )
		//	$Server->wsSend($clientID, "clients count is 2");
	else
		//Send the message to everyone but the person who said it
		foreach ( $Server->wsClients as $id => $client )
			if ( $id != $clientID )
				///////////////////////////////////////////////////////////////////
				$Server->wsSend($id, "$clientID ($ip) send-MSG: \"$message\"");
				//$Server->wsSend($id, "Visitor ----- $clientID ($ip) said \"$message\"");
}

// when a client connects
function wsOnOpen($clientID)
{
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	$Server->log( "$ip ($clientID) has connected." );

	//Send a join notice to everyone but the person who joined
	foreach ( $Server->wsClients as $id => $client )
		if ( $id != $clientID )
			$Server->wsSend($id, "$clientID ($ip) connected now!");
}

// when a client closes or lost connection
function wsOnClose($clientID, $status) {
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	$Server->log( "$ip ($clientID) has disconnected." );

	//Send a user left notice to everyone in the room
	foreach ( $Server->wsClients as $id => $client )
		$Server->wsSend($id, "Visitor $clientID ($ip) has disconnected now!");
}

// start the server
$Server = new PHPWebSocket();
echo "***********************************************************************************\r\n";
echo "!!! 			 f5core websocket service was LAUNCHED done 		!!!\r\n";
echo "*** 			do NOT CLOSE this terminal windows until wrk		***\r\n";
echo "***********************************************************************************\r\n";
$Server->bind('message', 'wsOnMessage');
$Server->bind('open', 'wsOnOpen');
$Server->bind('close', 'wsOnClose');
echo "-== f5/fly SERVER ==-\r\n IP:". $f5srv_ipv4 ."\r\n port:". $f5srv_port."\r\n -> just DO it йопта! ...\r\n";
$Server->wsStartServer($f5srv_ipv4, $f5srv_port);
?>