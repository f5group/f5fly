<?php
header('Content-type: text/html; charset=UTF-8');
date_default_timezone_set('Asia/Yekaterinburg');
// Include the Router class
// @note: it's recommended to just use the composer autoloader when working with other packages too
require_once __DIR__ . '/core/f5router.php';
// Create a Router
$router = new \Bramus\Router\Router();
// Custom 404 Handler
$router->set404(function () {
  try {
    $srvc_prtc = $_SERVER['SERVER_PROTOCOL'];
    header($srvc_prtc. ' 404 Not Found');
  } catch (Exception $e) {
    echo "_ 404 Not Found !!!";
  }
  echo '404, route not found!'; echo '404, route not found!';
  echo 'AFTER 7 SECOND YOU WILL BE GO ON f5fly #home';
});
// Before Router Middleware
$router->before('GET', '/.*', function () {
  header('X-Powered-By: f5fly v5.5.7');
  });
// 01 Static route: / (homepage of f5fly) ALL (desktop,tab,mob) client DeviceType
$router->get('/', function () {
  require_once __DIR__ .'/core/model-f5start.php'; $f5start = new f5start;
  $getHTMLpageTITLE = $f5start->getTITLEpage('index'); $getGUIpage1HEADER = $f5start->getHEADER1page('index');
  $getGUIpage1HEADERico = $f5start->getHEADER1pageIco('index');
  require_once __DIR__ . '/v/nb-f5index.php';
  $f5mainLogo = '/img/f5-logo-blue-el.png';
  require_once 'v/v-f5index.php';
});
// 02 Static route: /m -> Marshal desktop client DeviceType
$router->get('/m', function () {
  require_once __DIR__ . '/v/nb-f5marshal.php';
  //require_once __DIR__ . '/v/nb-f5index.php';
  require_once __DIR__ . '/core/model-mngmt.php';
  //require_once __DIR__ . '/core/class.f5.main.php';
  require_once 'v/v-f5marshal.php';
});

// Static route: /t
$router->get('/t', function () {
  //require_once __DIR__ .'/core/model-f5start.php'; $f5start = new f5start;
  //$getHTMLpageTITLE = $f5start->getTITLEpage('tablo');  
  require_once __DIR__ . '/core/class.f5.main.php';
  //require_once 'v/v-f5tablo.php';
  require_once 'v/v-f5tabloN.php';

});

// Static route: /d
$router->get('/d', function () {
  require_once __DIR__ .'/core/model-f5start.php';
  require_once __DIR__ . '/v/v-f5double-top.php';
  require_once 'v/v-f5double.php';
});



// XXX Static route: /j - Judge Area
//$router->get('/j', function () {
//  require_once __DIR__ . '/core/model-mngmt.php';
//  require_once __DIR__ . '/v/v-f5judge.php';
//  //require_once 'v/v-f5sht.php';
//});
// XXX Static route: /j - Judge Area

$router->get('/j', function () {
  //require_once __DIR__ .'/core/model-f5start.php'; //$f5start = new f5start;
  require_once __DIR__ .'/core/model-f5start.php';
  $f5start = new f5start;
  $f5config = new f5config;
  //require_once __DIR__ . '/v/nb-f5judge.php';
  //require_once __DIR__ . '/core/model-mngmt.php';
  require_once __DIR__ . '/v/nb-f5jjj.php';
  require_once __DIR__ . '/v/v-f5judge.php';
  //require_once 'v/v-f5sht.php';
});
$router->get('/j1', function () {
  //require_once __DIR__ . '/core/model-mngmt.php';
  require_once __DIR__ .'/core/model-f5start.php';
  $f5start = new f5start;
  $f5config = new f5config;
  require_once __DIR__ . '/v/j.php';
});
$router->get('/j2', function () {
  require_once __DIR__ .'/core/model-f5start.php';
  $f5start = new f5start;
  $f5config = new f5config;
  require_once __DIR__ . '/v/j.php';
});
$router->get('/j3', function () {
  require_once __DIR__ .'/core/model-f5start.php';
  $f5start = new f5start;
  $f5config = new f5config;
  require_once __DIR__ . '/v/j.php';
});
// Static route: /opt - #OPTIONS
$router->get('/opt', function () {
  require_once __DIR__ .'/core/model-f5start.php'; $f5start = new f5start; $f5locale = new f5locale;
  $getGUIOPT05tab01panHeading = $f5locale->getOPT05tab01Heading(); $getGUIOPT05tab02panHeading = $f5locale->getOPT05tab02Heading();
  $getHTMLpageTITLE = $f5start->getTITLEpage('options'); $getBASEDIR = $f5start->getf5BaseDIR(); $getGUIpage1HEADER = $f5start->getHEADER1page('options');
  $getGUIpage1HEADERico = $f5start->getHEADER1pageIco('options'); $getGUIf5bName = $f5start->getf5brandPanel('index');
  require_once __DIR__ . '/v/nb-f5index.php'; 
  $getGUIOPT03tab01optStrON = $f5locale->getGUIOPT03tab01optStr(1); $getGUIOPT03tab01optStrOFF = $f5locale->getGUIOPT03tab01optStr(0);
  $getGUIOPT03tab02optStrON = $f5locale->getGUIOPT03tab02optStr(1); $getGUIOPT03tab02optStrOFF = $f5locale->getGUIOPT03tab02optStr(0);
  $getGUIOPT03tab03optStrON = $f5locale->getGUIOPT03tab03optStr(1); $getGUIOPT03tab03optStrOFF = $f5locale->getGUIOPT03tab03optStr(0);
  $getGUIOPT03tab04optStrON = $f5locale->getGUIOPT03tab04optStr(1); $getGUIOPT03tab04optStrOFF = $f5locale->getGUIOPT03tab04optStr(0);
  $getGUIOPT03tab05optStrON = $f5locale->getGUIOPT03tab05optStr(1); $getGUIOPT03tab05optStrOFF = $f5locale->getGUIOPT03tab05optStr(0);

  $getGUIOPT04tab01optStrON = $f5locale->getGUIOPT04tab01optStr(1); $getGUIOPT04tab01optStrOFF = $f5locale->getGUIOPT04tab01optStr(0);
  $getGUIOPT04tab02optStrON = $f5locale->getGUIOPT04tab02optStr(1); $getGUIOPT04tab02optStrOFF = $f5locale->getGUIOPT04tab02optStr(0);
  $getGUIOPT04tab03optStrON = $f5locale->getGUIOPT04tab03optStr(1); $getGUIOPT04tab03optStrOFF = $f5locale->getGUIOPT04tab03optStr(0);
  $getGUIOPT04tab04optStrON = $f5locale->getGUIOPT04tab04optStr(1); $getGUIOPT04tab04optStrOFF = $f5locale->getGUIOPT04tab04optStr(0);
  $getGUIOPT04tab05optStrON = $f5locale->getGUIOPT04tab05optStr(1); $getGUIOPT04tab05optStrOFF = $f5locale->getGUIOPT04tab05optStr(0);

  $getGUIOPT05tab01optStrON = $f5locale->getOPT05tab01optStr(1); $getGUIOPT05tab01optStrOFF = $f5locale->getOPT05tab01optStr(0);
  $getGUIOPT05tab02optStrON = $f5locale->getOPT05tab02optStr(1); $getGUIOPT05tab02optStrOFF = $f5locale->getOPT05tab02optStr(0);
  $getGUIOPT05tab03optStrON = $f5locale->getOPT05tab03optStr(1); $getGUIOPT05tab03optStrOFF = $f5locale->getOPT05tab03optStr(0);
  $getGUIOPT05tab04optStrON = $f5locale->getOPT05tab04optStr(1); $getGUIOPT05tab04optStrOFF = $f5locale->getOPT05tab04optStr(0);
  $getGUIOPT05tab05optStrON = $f5locale->getOPT05tab05optStr(1); $getGUIOPT05tab05optStrOFF = $f5locale->getOPT05tab05optStr(0);
  $getGUIOPT05tab06optStrON = $f5locale->getOPT05tab06optStr(1); $getGUIOPT05tab06optStrOFF = $f5locale->getOPT05tab06optStr(0);
  $getGUIOPT05tab07optStrON = $f5locale->getOPT05tab07optStr(1); $getGUIOPT05tab07optStrOFF = $f5locale->getOPT05tab07optStr(0);
  $getGUIOPT05tab08optStrON = $f5locale->getOPT05tab08optStr(1); $getGUIOPT05tab08optStrOFF = $f5locale->getOPT05tab08optStr(0);
  $getGUIOPT05tab09optStrON = $f5locale->getOPT05tab09optStr(1); $getGUIOPT05tab09optStrOFF = $f5locale->getOPT05tab09optStr(0);
  $getGUIOPT05tab10optStrON = $f5locale->getOPT05tab10optStr(1); $getGUIOPT05tab10optStrOFF = $f5locale->getOPT05tab10optStr(0);
  $getGUIOPT05tab11optStrON = $f5locale->getOPT05tab11optStr(1); $getGUIOPT05tab11optStrOFF = $f5locale->getOPT05tab11optStr(0);
  $getGUIOPT05tab12optStrON = $f5locale->getOPT05tab12optStr(1); $getGUIOPT05tab12optStrOFF = $f5locale->getOPT05tab12optStr(0);
  $getGUIOPT05tab13optStrON = $f5locale->getOPT05tab13optStr(1); $getGUIOPT05tab13optStrOFF = $f5locale->getOPT05tab13optStr(0);

  $getGUIOPT02tab01label = $f5locale->getOPT02tab01label();
  $getGUIOPT02tab02label = $f5locale->getOPT02tab02label();
  $getGUIOPT02tab03label = $f5locale->getOPT02tab03label();

  $getGUIOPT01tab01select = $f5locale->getGUIOPT01tab01select();
  $getGUIOPT01tab01select1val = $f5locale->getGUIOPT01tab01select1val();
  $getGUIOPT01tab01select2val = $f5locale->getGUIOPT01tab01select2val();
  $getGUIOPT01tab01label = $f5locale->getGUIOPT01tab01label();
  $getGUIOPT01tab02label = $f5locale->getGUIOPT01tab02label();

  $getGUIOPT02tab01select = $f5start->getGUIOPT02tab01select();
  $getGUIpage1tab01 = $f5start->getTAB1opt('options',0);
  $getGUIpage1tab02 = $f5start->getTAB2opt('options',0);
  $getGUIpage1tab03 = $f5start->getTAB3opt('options',0);
  $getGUIpage1tab04 = $f5start->getTAB4opt('options',0);
  $getGUIpage1tab05 = $f5start->getTAB5opt('options',0);
  $getGUItab1label = $f5start->getTAB1opt('options',1);
  $getGUItab2label = $f5start->getTAB2opt('options',1);
  $getGUItab3label = $f5start->getTAB3opt('options',1);
  $getGUItab4label = $f5start->getTAB4opt('options',1);
  $getGUItab5label = $f5start->getTAB5opt('options',1);
  $getGU1saveBUTTON = $f5start->getBUTTON1save('options',0);
  $getGU1saveICOBUTTON = $f5start->getBUTTON1save('options',1);
  $getf5name = $f5start->getf5name();
  $getf5ver = $f5start->getf5version();
  $getf5rdate = $f5start->getf5releaseDate();
  $getf5url01 = $f5start->getf5url01x();
  $getf5url02 = $f5start->getf5url02x();
  require_once 'v/v-f5opt.php';
}); 
// Static route: /abt - #ABOUT
$router->get('/abt', function () {
  //require_once __DIR__ . '/v/f5.navbar.php';
  require_once __DIR__ .'/core/model-f5start.php'; $f5start = new f5start;
  $getHTMLpageTITLE = $f5start->getTITLEpage('about'); 
  $getf5fullName = $f5start->getf5Fullname();
  $f5titteName = $f5start->getf5name4about();
  require_once __DIR__ . '/v/nb-f5index.php';
  require_once 'v/v-f5about.php';
});

// XXX Static route: /sht - #showTime
$router->get('/sht', function () {
  require_once __DIR__ .'/core/model-f5start.php'; $f5start = new f5start;
  $getHTMLpageTITLE = $f5start->getTITLEpage('sh0wTime');  
  require_once __DIR__ . '/core/model-f5start.php';
  require_once __DIR__ . '/v/nb-f5index.php';
  require_once 'v/v-f5sht.php';
});





// 02 Static route: /mob - mobile client DeviceType
$router->get('/mob', function () {
  require_once __DIR__ . '/core/model-f5start.php';
  require_once __DIR__ . '/v/nb-f5mob.php';
  require_once 'v/v-f5mob.php';
});
// 03 Static route: /tab - tablet client DeviceType
$router->get('/tab', function () {
  require_once __DIR__ . '/core/model-f5start.php';
  require_once __DIR__ . '/v/nb-f5tab.php';
  require_once 'v/v-f5tab.php';
});




// XXX Static route: /db - #SQLite DB WEB manager
$router->get('/db', function () {
  //require_once __ . '/core/adminer.php';
  header('Location: http://f5/core/adminer.php');
});



    

    // Static route: /add
    $router->get('/add', function () {
        require_once __DIR__ . '/v/nb-f5index.php';
        //require_once __DIR__ . '/core/model-add.php';
        //echo '<h1>f5/p</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
        require_once 'v/view-add.php';
    });
// Static route: /edit
    $router->get('/edit', function () {

  //require_once __DIR__ .'/core/model-f5start.php'; $f5start = new f5start;
  //$getHTMLpageTITLE = $f5start->getTITLEpage('index'); $getGUIpage1HEADER = $f5start->getHEADER1page('index');
  //$getGUIpage1HEADERico = $f5start->getHEADER1pageIco('index');
  //require_once __DIR__ . '/v/nb-f5index.php';
  //$f5mainLogo = '/img/f5-logo-blue-el.png';

        require_once __DIR__ . '/v/nb-f5marshal.php';
        //require_once __DIR__ . '/core/model-edit.php';
        require_once __DIR__ . '/core/model-db.php';
        //echo '<h1>f5/p</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
        require_once 'v/v-edit.php';
    });












        // Static route: /sadn - sqliteAdmin
    $router->get('/sadm', function () {
            require_once  __DIR__ . '/core/sqladm.php';
    });
    $router->get('/mt', function () {
        require_once __DIR__ . '/core/class.f5.main.php';
        require_once 'v/mt.php';
    });    
    $router->get('/s', function () {
        require_once __DIR__ . '/core/class.f5.main.php';
        require_once 'v/view-signal.php';
    });
    $router->get('/ms', function () {
        //require_once __DIR__ . '/v/f5.m.navbar.php';
        //require_once __DIR__ . '/core/model-mngmt.php';
        //echo '<h1>f5/p</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
        require_once 'v/mi_sm.php';
    });
    $router->get('/md', function () {
        require_once __DIR__ . '/core/model-mngmt.php';
        //echo '<h1>f5/p</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
        require_once 'v/md.php';
    });    




// ###############
// ###############
// ###############
//////////////////// Dynamic route: /new/att
  $router->get('/new/att', function () {
    require_once __DIR__ . '/v/f5.navbar.php';
    require_once __DIR__ . '/core/model-db.php';
    require_once 'v/view-edit.php';
  });
  $router->get('/new/event', function () {
    require_once __DIR__ . '/v/f5.navbar.php';
    require_once __DIR__ . '/core/model-db.php';
    require_once 'v/view-edit.php';
    echo 'att id ' . htmlentities($id);
  });  
  $router->get('/new/eventPlace', function () {
    require_once __DIR__ . '/v/f5.navbar.php';
    require_once __DIR__ . '/core/model-db.php';
    require_once 'v/view-edit.php';
  });  
  $router->get('/edit/att/(\d+)', function ($id) {
    require_once __DIR__ . '/v/f5.navbar.php';
    require_once __DIR__ . '/core/model-db.php';
    require_once 'v/view-edit.php';
    echo 'att id ' . htmlentities($id);
  });
  $router->get('/edit/event/(\d+)', function ($id) {
    require_once __DIR__ . '/v/f5.navbar.php';
    require_once __DIR__ . '/core/model-db.php';
    require_once 'v/view-edit.php';
    echo 'att id ' . htmlentities($id);
  });
  $router->get('/edit/eventPlace/(\d+)', function ($id) {
    require_once __DIR__ . '/v/f5.navbar.php';
    require_once __DIR__ . '/core/model-db.php';
    require_once 'v/view-edit.php';
    echo 'att id ' . htmlentities($id);
  });  

/////////////////////// Dynamic route: /edit/id
    $router->get('/edit/(\d+)', function ($id) {
        require_once __DIR__ . '/v/f5.navbar.php';
        require_once __DIR__ . '/core/model-db.php';
        require_once 'v/view-edit.php';
        echo 'att id ' . htmlentities($id);
    });
    // Static route: /j
//    $router->get('/j', function () {
//        echo '<h1>f5/j</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
//    });
    // Static route: /hello
    $router->get('/hello', function () {
        echo '<h1>f5/router</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
    });

    // Static route: /f5
    $router->get('/f5', function () {
        echo '<h1>f5/router*f5*</h1><p>Visit <code>/f5/<em>???</em></code> go2the r331</p>';
    });

    // Dynamic route: /hello/name
    $router->get('/hello/(\w+)', function ($name) {
        echo 'Hello ' . htmlentities($name);
    });

    // Dynamic route: /ohai/name/in/parts
    $router->get('/ohai/(.*)', function ($url) {
        echo 'Ohai ' . htmlentities($url);
    });




    // Dynamic route with (successive) optional subpatterns: /blog(/year(/month(/day(/slug))))
    $router->get('/blog(/\d{4}(/\d{2}(/\d{2}(/[a-z0-9_-]+)?)?)?)?', function ($year = null, $month = null, $day = null, $slug = null) {
        if (!$year) {
            echo 'Blog overview';
            return;
        }
        if (!$month) {
            echo 'Blog year overview (' . $year . ')';
            return;
        }
        if (!$day) {
            echo 'Blog month overview (' . $year . '-' . $month . ')';
            return;
        }
        if (!$slug) {
            echo 'Blog day overview (' . $year . '-' . $month . '-' . $day . ')';
            return;
        }
        echo 'Blogpost ' . htmlentities($slug) . ' detail (' . $year . '-' . $month . '-' . $day . ')';
    });

    // Subrouting
        // Subrouting
    $router->mount('/j', function () use ($router) {

        // will result in '/movies'
        $router->get('/', function () {
            echo 'judas area';
        });

        // will result in '/movies'
        $router->post('/', function () {
            echo 'add j';
        });

        // will result in '/movies/id'
        $router->get('/(\d+)', function ($id) {
            echo 'judas id ' . htmlentities($id);
        });

        // will result in '/movies/id'
        $router->put('/(\d+)', function ($id) {
            echo 'Update j id ' . htmlentities($id);
        });

    });
    $router->mount('/movies', function () use ($router) {

        // will result in '/movies'
        $router->get('/', function () {
            echo 'movies overview';
        });

        // will result in '/movies'
        $router->post('/', function () {
            echo 'add movie';
        });

        // will result in '/movies/id'
        $router->get('/(\d+)', function ($id) {
            echo 'movie id ' . htmlentities($id);
        });

        // will result in '/movies/id'
        $router->put('/(\d+)', function ($id) {
            echo 'Update movie id ' . htmlentities($id);
        });

    });

    // Thunderbirds are go!
    $router->run();

// EOF