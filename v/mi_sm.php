
<!DOCTYPE html><html class=''>
<head>
<link href="../css/bootstrap.css" rel="stylesheet">
<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/bootstrap300.min.js"></script>
  <meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" /><link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" /><link rel="canonical" href="https://codepen.io/koenigsegg1/pen/GoEvRd?limit=all&page=33&q=contact+" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>


<style class="cp-pen-styles">body {
  display: flex;
  justify-content: center;
  align-items: center;
  background: #1D90F5;
  min-height: 100vh;
  margin: 0;
  font-family: roboto;
}
body:before {
  content: '';
  background: #E8FAFF;
  height: 50%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
}

.menu {
  display: block;
  box-sizing: border-box;
  background: white;
  height: 87.75px;
  width: 87.75px;
  padding: 36px;
  border-radius: 6px;
  overflow: hidden;
  position: relative;
  box-shadow: 0 8px 22px rgba(0, 0, 0, 0.15);
  transition: 1.3s cubic-bezier(0.53, 0, 0.15, 1.3);
}
.menu.expanded {
  width: 351px;
}

span {
  padding: 9px;
  white-space: nowrap;
  visibility: visible;
  opacity: 1;
  transition: .3s;
  transform: rotateY(0deg);
}
span:nth-of-type(1) {
  transition-delay: .4s;
}
span:nth-of-type(2) {
  transition-delay: .5s;
}
span:nth-of-type(3) {
  transition-delay: .6s;
}
span:nth-of-type(4) {
  transition-delay: .7s;
}
span.hidden {
  width: 0;
  visibility: hidden;
  opacity: 0;
  transform: rotateY(90deg);
}
span.hidden:nth-of-type(1) {
  transition-delay: .3s;
}
span.hidden:nth-of-type(2) {
  transition-delay: .2s;
}
span.hidden:nth-of-type(3) {
  transition-delay: .1s;
}
span.hidden:nth-of-type(4) {
  transition-delay: 0s;
}
span a {
  color: black;
  text-decoration: none;
  font-size: 16.36364px;
  transition: .3s;
}
span a:hover {
  color: #1D90F5;
}

.container {
  position: absolute;
  top: 22.5px;
  right: calc(100% - 73.125px);
  height: 42.75px;
  width: 58.5px;
  transition: 1s;
}
.container.close {
  right: 0;
}

.toggle {
  position: relative;
  height: 42.75px;
  width: 58.5px;
  cursor: pointer;
}
.toggle:after, .toggle:before {
  content: '';
  background: #1D90F5;
  width: 36px;
  height: 4.5px;
  margin: auto;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  transition: 1.3s cubic-bezier(0.53, 0, 0.15, 1.3);
}
.toggle:before {
  top: -30%;
}
.toggle:after {
  top: 30%;
}
.toggle.close:before {
  transform: rotate(-225deg);
  top: 0;
}
.toggle.close:after {
  transform: rotate(225deg);
  top: 0;
}
</style></head><body>

<div class="menu">
  <div class="container">
    <div class="toggle"></div>
  </div><span class="hidden"><a href="#">Home </a></span><span class="hidden"><a href="#">About </a></span><span class="hidden"><a href="#">Info</a></span><span class="hidden"><a href="#">Contact</a></span>
</div>
<script >$('.toggle').on('click', function() {
  $('.menu').toggleClass('expanded');  
  $('span').toggleClass('hidden');  
  $('.container , .toggle').toggleClass('close');  
});
//# sourceURL=pen.js
</script>
</body></html>