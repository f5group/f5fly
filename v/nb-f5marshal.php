<!DOCTYPE html>
<html>
<html lang="ru">
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php $pageTitle = 'f5: Маршал - f5/fly'; echo $pageTitle; ?></title>
  <link rel="icon" href="../favicon.ico">
    <!-- f5 core CSS -->
  <link href="../css/f5main.css" rel="stylesheet">
  <!-- Bootstrap and also core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../css/hover-min.css" rel="stylesheet">
  <link href="../css/bootstrap-editable.css" rel="stylesheet">
  <link href="../css/manifest.json" rel="manifest">
  <link href="../css/pretty-checkbox.min.css" rel="stylesheet" type="text/css">
  <link href="../css/materialdesignicons.min.css" rel="stylesheet" type="text/css">  
  <link href="../css/daterangepicker.css" rel="stylesheet" type="text/css">

  <!-- 
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://свитязь.рф/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://свитязь.рф/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://свитязь.рф/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://свитязь.рф/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="https://свитязь.рф/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://свитязь.рф/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://свитязь.рф/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://свитязь.рф/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="https://свитязь.рф/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="https://свитязь.рф/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="https://свитязь.рф/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="https://свитязь.рф/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="https://свитязь.рф/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="СЦ Свитязь - Спорт, Тяжелая атлетика"/>
<meta name="msapplication-TileColor" content="#BEBEBE" />
<meta name="msapplication-TileImage" content="https://свитязь.рф/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="https://свитязь.рф/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="https://свитязь.рф/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="https://свитязь.рф/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="https://свитязь.рф/mstile-310x310.png" />
-->

<!-- <script src="../js/jquery-1.11.1.min.js"></script> -->
<script src="../js/jquery.1.12.4.min.js"></script>
<script src="../js/tippy.all.min.js"></script>
<script src="../js/chrono.js"></script>
<script src="../js/libs/moment.min.js"></script>
<script src="../js/libs/daterangepicker.js"></script>
<script>
  $(document).ready(function(){
    $(".hamburger").click(function(){ $(this).toggleClass("is-active"); });
  });
</script>

</head>

  <?php
$f5db = new PDO('sqlite:/Users/jb/wrk/f5.local/core/f5db.sl3');
$f5db -> exec("SET CHARACTER SET utf8");
$f5db->query('set names utf8');


try {    

$result = $f5db->query('SELECT value FROM f5lng WHERE id="3" LIMIT 1;');
foreach ($result as $result) { $MENUi0 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="4" LIMIT 1;');
foreach ($result as $result) { $MENUi1 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="5" LIMIT 1;');
foreach ($result as $result) { $MENUi2 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="6" LIMIT 1;');
foreach ($result as $result) { $MENUi3 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="7" LIMIT 1;');
foreach ($result as $result) { $MENUi4 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="8" LIMIT 1;');
foreach ($result as $result) { $MENUi5 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="9" LIMIT 1;');
foreach ($result as $result) { $MENUi6 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="10" LIMIT 1;');
foreach ($result as $result) { $MENUi7 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="11" LIMIT 1;');
foreach ($result as $result) { $MENUi8 = $result['value']; }
$result = $f5db->query('SELECT value FROM f5lng WHERE id="12" LIMIT 1;');
foreach ($result as $result) { $MENUi9 = $result['value']; }



} catch(PDOException $e) {
    echo $e->getMessage();
}
  ?>
<body id="mainz">
            <nav id="navbarmain" class="navbar navbar-default top01menu">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand nof5activetoprootmenu" title="<?=$MENUi0;?>" href="/"><?=$MENUi0;?>
            </a>
          </div>

    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav"> <li>
        <li class="mmnu0"><a id="p01" title="<?=$MENUi0;?>" href="/m"><b><i class="icon mdi mdi-24px mdi-google-controller"> </i><?=$MENUi1;?></b><i class="icon mdi mdi-arrow-right-drop-circle-outline"> </i></a></li>
        <li class="mmnu0"><a id="d02" title="<?=$MENUi0;?>" href="/d"><b><i class="icon mdi mdi-24px mdi-table-large"> </i><?=$MENUi2;?></b><i class="icon mdi mdi-high-definition"> </i></a></li>
        <li class="mmnu0"><a id="t03" title="<?=$MENUi0;?>" href="/t"><b><i class="icon mdi mdi-24px mdi-television"> </i><?=$MENUi3;?></b><i class="icon mdi mdi-high-definition"> </i></a></li>
        <li class="mmnu0"><a id="j04" title="<?=$MENUi0;?>" href="/j"><b><i class="icon mdi mdi-24px mdi-scale-balance"></i><?=$MENUi4;?></b><i class="icon mdi mdi-cellphone-iphone"> </i></span></a></li>
        <li class="mmnu0"><a id="s05" title="<?=$MENUi0;?>" href="/s"><b><i class="icon mdi mdi-24px mdi-bell-outline"></i><?=$MENUi5;?></b><i class="icon mdi mdi-standard-definition"></i></a></li>
      </ul>

<ul class="nav navbar-nav navbar-right">
<li class="dropdown">

<!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="icon mdi mdi-24px mdi-menu"> </i></a> -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <div class="hamburger" id="hamburgerMENU">
          <span class="line"></span>
          <span class="line"></span>
          <span class="line"></span>
        </div>
        </a>
<ul class="dropdown-menu" role="menu">
  <li class="divider"></li>
<li><a title="<?=$MENUi6;?>" href="/sht"><i class="icon mdi mdi-24px mdi-silverware-variant"></i> <?=$MENUi6;?></a></li>
<li class="divider"></li>
<li><a title="<?=$MENUi7;?>" href="/opt"> <i class="icon mdi mdi-24px mdi-settings-box"> </i> <?=$MENUi7;?></a></li>
<li class="divider"></li>
<li><a target="_blank" title="<?=$MENUi8;?>" href="/db"><i class="icon mdi mdi-24px mdi-database"> </i> <?=$MENUi8;?></a></li>
<li class="divider"></li>
<li><a title="<?=$MENUi9;?>" href="/abt"> &nbsp;&nbsp;&nbsp;<?=$MENUi0;?>&nbsp;<i class="icon mdi mdi-24px mdi-information"> </i></a></li>
<li class="divider"></li>
</ul>
</li>
</ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
<div id="mnuLine">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="toggle" class="wo0line" title="Show main menu !" href="#"><i class="icon mdi mdi-36px mdi-menu"></i></a> 
          </div>
      </nav>



