<?php 
$f5start = new f5start; 
$getf5lang = $f5start->getf5GUIlang();
$getBASEDIR = $f5start->getf5BaseDIR();
$getGUIf5bName = $f5start->getf5brandPanel('index');
$getf5tabName01 = $f5start->getPageIndexTab01();
$getf5tabName02 = $f5start->getPageIndexTab02();

$getf5name = $f5start->getf5name();
$getf5fullName = $f5start->getf5Fullname();
$getf5ver = $f5start->getf5version();
$getf5rdate = $f5start->getf5releaseDate();
$getf5url01 = $f5start->getf5url01x();
$getf5url02 = $f5start->getf5url02x();
$getf5url03 = $f5start->getf5url03x();
$getf5header01 = $f5start->getPageHeader01();

$getGUIbtn01 = $f5start->getPageIndexBtn01();
$getGUIpwd4juda_str01 = $f5start->getPageIndexJPWDstr01();
$getPWD4judas = $f5start->getPageIndexJPWD();

$getGUIpan01label = $f5start->getPageIndexPanel01label();
$getGUImodalHeader = $f5start->getGUImdHeader();
$getGUImodalSavedINFO = $f5start->getGUIsavedINFO();
$getGUIstep01 = $f5start->getGUIstep01title();
$getGUIstep02 = $f5start->getGUIstep02title();
$getGUIstep03 = $f5start->getGUIstep03title();
$getGUIstep04 = $f5start->getGUIstep04title();
$getGUIstep05 = $f5start->getGUIstep05title();

$getGUIsexLabel = $f5start->getGUIstep01label1();
$getGUIcountryLabel = $f5start->getGUIstep01label2();

$getGUIwcatLabel = $f5start->getGUIstep01label3();
$getGUIcityTeamLabel = $f5start->getGUIstep01label4();
$getGUIavaLabel = $f5start->getGUIstep01label5();
$getGUIbDayLabel = $f5start->getGUIstep01label6();

$getGUIstep01lblFname = $f5start->getGUIfName(index,0);
$getGUIstep01lblFnameDef = $f5start->getGUIfName(index,1);
$getGUIstep01lblLname = $f5start->getGUIlName(index,0);
$getGUIstep01lblLnameDef = $f5start->getGUIlName(index,1);

$getGUIsnatch1 = $f5start->getGUIstep01label7();
$getGUIcj1 = $f5start->getGUIstep01label8();
?>
  
<!-- f5-core maibody div wrapper container --> 
<div id="mainbody">
<!-- f5-core main wrapper container --> 
<div class="container">
<br>

<div class="wjumbotron">
  <h3>
  <div class="wrap2logo">
    <img class="logoimgmain" src="<?php echo $getBASEDIR; echo $f5mainLogo;?>"><span class="text-successf5"><?php
      echo $getGUIf5bName;?> </span>
  </div>
  </h3>

<!-- show flag and  ava propperty q div block --> 
<div class="ttheright">
  <div class="row">
    <div class="col-xs-12">
    </div>
  </div>
</div>


<script>
// ################################################################################################################
// #####  use JS localStorage !!!
// ################################################################################################################
if (window.localStorage && (typeof(Storage) !== "undefined")) {
  localStorage.clear();
  localStorage.setItem('ipv4addr', '10.5.5.2');
  localStorage.setItem('portnumb', '9997');
  localStorage.setItem('need2refreshMarchal', '1');
  localStorage[ 'need2refreshMarchal' ] = true;
  console.log('localStorage: cleared&setup');
} else {
  console.log('localStorage: browser does not support Web Storage...');
}


function getLStorage(param) {
  let currGender = localStorage.getItem('currGender');
  let currContry = localStorage.getItem('currContry');
  let currWCat = localStorage.getItem('currWCat');
  let currCityTeam = localStorage.getItem('currCityTeam');
  let currAva = localStorage.getItem('currAva');
  let currBDay = localStorage.getItem('currBDay');
  let currFNAme = localStorage.getItem('currFNAme');
  let currLName = localStorage.getItem('currLName');
  let currS1order = localStorage.getItem('currS1order');
  let currCJ1order = localStorage.getItem('currCJ1order');
  let getParam = param;
  if (param==='print') {
    console.log('gen1:' +currGender+'| country2:'+currContry+'| wcat3:'+currWCat+'| cTeam4:'+currCityTeam+'| ava5:'+currAva+'| BDay6:'+currBDay+'| fName7:'+currFNAme+'| LName8:'+currLName+'| S1_9:'+currS1order + '| CJ1_10:'+currCJ1order +'|||');
  }
}


function getCountry(countryVALUE) {
  let y = countryVALUE;
  let x = document.getElementById('topInfoBox_FlowAtt').innerHTML;
  $('#radioCountryAtt').fadeOut( 123, "linear", function() { });
  document.getElementById('topInfoBox_FlowAtt').innerHTML = y+'&nbsp;<i class="icon mdi mdi-18px mdi-earth"></i>&nbsp;'+x;
  let timerId = setTimeout(function tblRefresh3s() { $("#radioCountryAtt").hide(); timerId = setTimeout(tblRefresh3s, 555); }, 555);
    try { localStorage.setItem('currContry', y); } catch (e) 
      { if (e == QUOTA_EXCEEDED_ERR) { alert('localStorage QUOTA_EXCEEDED_ERROR!'); } }
}

$(document).ready(function(){
  $('#i_addons a').on('click', function (e) {
    e.preventDefault(); $(this).tab('show');
    let timerIdCfgBtn = setTimeout(function hide0cfgDiv() { 
    location.reload(true);
      timerId = setTimeout(hide0cfgDiv, 12345); 
    }, 12345);
  });


$('#inputDateRandeid').daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  minYear: 1977,
  maxYear: 2022,
  locale: {
    format: 'YYYY'
  },
    "autoApply": true,
    "drops": "up",
    "opens": "center"
});

$('#inputDateRandeid').on('apply.daterangepicker', function(ev, picker) {
  console.log(picker.startDate.format('DD.MM.YYYY'));
  let ATTbDay = picker.startDate.format('YYYY');
  let bDayAtt = picker.startDate.format('DD.MM.YYYY');
  let defaCityTeam = 'Сургут';
  let LScurrAva = localStorage.getItem('currAva');
  let LScurrCityTeam = localStorage.getItem('currCityTeam');
  try {
    localStorage.setItem('currBDay', bDayAtt);
  if (LScurrAva === null) { localStorage.setItem('currAva','f5defava0.png'); console.log('f5defava0.png *** ' + LScurrAva); }
  if (LScurrCityTeam === null) { localStorage.setItem('currCityTeam','Сургут'); console.log('Сургут *** ' + LScurrCityTeam); }
        } catch (err) 
          { 
            if (err === QUOTA_EXCEEDED_ERR) 
              { alert('localStorage QUOTA_EXCEEDED_ERROR!'); } 
          }
    
    //localStorage.setItem('currWCat', defaCityTeam);
    //localStorage.setItem('currAva', currAva);          

  let attCountry = document.getElementById('topInfoBox_AttPII').innerHTML;
  let attCountryNV = document.getElementById('topInfoBox_AttPII').innerHTML = ATTbDay;
  let x = document.getElementById('topInfoBox_FlowAtt').innerHTML;
//  console.log(attCountryNV+'_');
  document.getElementById('a03stepID').classList.toggle("active", false);
  document.getElementById('a04stepID').classList.toggle("disabled", false);
  document.getElementById('a04stepID').classList.toggle("active", true);
  document.getElementById('a04stepID').click();  
  document.getElementById('inputNames').style.display = 'block';
  document.getElementById('nameDiv').style.display = 'block';
  document.getElementById('name1a').style.display = 'block';
  document.getElementById('bdayDIV_id').style.display = 'none';
  document.getElementById('attAva').style.display = 'none';
  let labelbDay = document.getElementById('attBDAYSPAN');
  labelbDay.innerHTML = ' | <b>' + bDayAtt + '</b>';

  //document.getElementById('inputCityTeamAtt').style.display = 'none'; 
  //document.getElementById('attAva').style.display = 'none';

  // if wCat not selected, set DEFA
    let cityTeamDIV = document.getElementById('inputCityTeamAtt');    
    if (window.getComputedStyle(cityTeamDIV).display === 'block') {
    let y = 'Сургут';
    let x = document.getElementById('cityTeamAtt').innerHTML;
    cityTeamDIV.style.display = 'none';
    document.getElementById('cityTeamAtt').innerHTML = y+'&nbsp;<i class="icon mdi mdi-18px mdi-earth"></i>&nbsp;'+x;
    $('#inputCityTeamAtt').fadeOut( 123, "linear", function() { });
  }

    document.getElementById('a03stepID').classList.toggle("disabled-step", true);
    document.getElementById('a03stepID').setAttribute('disabled','disabled');
    document.getElementById('a03stepID').classList.toggle("disabled", true); 
});


document.addEventListener('keyup', function(e) {
    if (e.keyCode == 27) {
      document.getElementById('xcloseid').focus();
      document.getElementById("xcloseid").click();
      document.location.reload();
    }
});

// ################################################################################################################
// #####  trY use JS localStorage @@@
// ################################################################################################################
  //document.getElementById('sexDiv').style.display = 'none';
  //document.getElementById('wcatDiv').style.display = 'none';
  document.getElementById('radioCountryAtt').style.display = 'none';
  document.getElementById('inputDateRandeid').style.display = 'none';
  document.getElementById('bdayDIV_id').style.display = 'none';
  document.getElementById('image_uploads').style.display = 'none';

  document.getElementById('nameDiv').style.display = 'none';
  document.getElementById('idattNameF').style.display = 'none';
  document.getElementById('idattNameL').style.display = 'none';
  
  document.getElementById('a_m666_kat1a').style.display = 'none';
  document.getElementById('a_f666_kat1a').style.display  = 'none';
  document.getElementById('divLname').style.display  = 'none';

  document.getElementById('name1a').style.display = 'none';
  document.getElementById('name2a').style.display = 'none';
  
  document.getElementById('selectWcat').style.display = 'none';
  
  document.getElementById('inputNames').style.display = 'none';
  document.getElementById('att_j1p1').style.display = 'none';
  document.getElementById('divS1i').style.display = 'none';
  document.getElementById('divCJ1i').style.display = 'none';

  document.getElementById('attAva').style.display = 'none';
  document.getElementById('wcatLabel').style.display = 'none';
  document.getElementById('inputCityTeamAtt').style.display = 'none';



$('#s1i').on('save', function(e, params) {
  let nameSpanVAL = document.getElementById('namesSPAN').innerHTML 
  let s1val = params.newValue;
  try { localStorage.setItem('currS1order', s1val); } catch (exx) 
    { if (exx == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
  $('#divS1i').fadeOut( 789, "linear", function() {  console.log('s1CHanged'+ s1val); });
  document.getElementById('divCJ1i').style.display = 'block';

});
$('#cj1i').on('save', function(e, params) {
  let nameSpanVAL = document.getElementById('namesSPAN').innerHTML 
  let cj1val = params.newValue;  
  document.getElementById('namesSPAN').innerHTML 
  try { localStorage.setItem('currCJ1order', cj1val); } catch (exxb) 
    { if (exxb == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
  $('#divCJ1i').fadeOut( 789, "linear", function() {  console.log('j1CHanged'+ cj1val); });


//console.log('=======START===== new Flow & Athlete =====LOGGIN=======');
//getLStorage('print');
//console.log(s1val + cj1val );
//console.log('=======STOP===== new Flow & Athlete =====LOGGIN=======');
document.getElementById("btnModalSaveadd").click();

});


$('#name1a').on('save', function(e, params) { 
  let fNameVAL = document.getElementById('attFName').innerHTML = params.newValue;
  //document.getElementById('nameDiv').style.display = 'block';
  document.getElementById('divLname').style.display  = 'block';
  document.getElementById('name2a').style.display = 'block';
  document.getElementById('namesSPAN').innerHTML = fNameVAL;
  document.getElementById('divCJ1i').style.display = 'block';
  try { localStorage.setItem('currFNAme', fNameVAL); } catch (exex) 
    { if (exex == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
  $('#divFname').fadeOut( 789, "linear", function() {  console.log('nameFchanged'); });

});


$('#name2a').on('save', function(e, params) { 
  let lNameVAL = document.getElementById('attLName').innerHTML = params.newValue;
  let x = document.getElementById('namesSPAN').innerHTML;
  document.getElementById('namesSPAN').innerHTML = '<p class="bg-primary">' + x + ' ' + lNameVAL + '</p>';
  try { localStorage.setItem('currLName', lNameVAL); } catch (expp) 
    { if (expp == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
  $('#divLname').fadeOut( 789, "linear", function() {  console.log('nameFchanged'); });
  document.getElementById('att_j1p1').style.display = 'block';
  document.getElementById('divS1i').style.display = 'block';
  document.getElementById('divCJ1i').style.display = 'none';
 
  document.getElementById('a04stepID').classList.toggle("active", false);
  document.getElementById('a05stepID').classList.toggle("disabled", false);
  document.getElementById('a05stepID').classList.toggle("active", true);
  document.getElementById('a05stepID').click();  
    document.getElementById('a04stepID').classList.toggle("disabled-step", true);
    document.getElementById('a04stepID').setAttribute('disabled','disabled');
    document.getElementById('a04stepID').classList.toggle("disabled", true);
});

$('#a_m666_kat1a').on('save', function(e, params) {
  //document.getElementById('attWcat').innerHTML = params.newValue + '&#13199;';
  document.getElementById('selectWcat').style.display = 'none';
  document.getElementById('a_m666_kat1a').style.display = 'none';
  document.getElementById('a_f666_kat1a').style.display = 'none';
  document.getElementById('name1a').style.display = 'block';
  let x = document.getElementById('topInfoBox_FlowAtt').innerHTML;
  let y = document.getElementById('attWcat').innerHTML = '&nbsp; <kbd>'+params.newValue + '&#13199;</kbd>';
  document.getElementById('a02stepID').classList.toggle("active", false);
  document.getElementById('a03stepID').classList.toggle("disabled", false);
  document.getElementById('a03stepID').classList.toggle("active", true);
  document.getElementById('a03stepID').click();  
  document.getElementById('inputDateRandeid').style.display = 'block';
  document.getElementById('bdayDIV_id').style.display = 'block';
  document.getElementById('attAva').style.display = 'block';
  document.getElementById('topInfoBox_FlowAtt').innerHTML = '<b>' + x + '  ' + y + '</b>';
  document.getElementById('image_uploads').style.display = 'block';
  document.getElementById('wcatLabel').style.display = 'none';
  document.getElementById('inputCityTeamAtt').style.display = 'block'; 

  let countryDIVid = document.getElementById('radioCountryAtt');    
  // If the element is visible, hide it
  if (window.getComputedStyle(countryDIVid).display === 'block') {
    let y = 'RUS';
    let x = document.getElementById('topInfoBox_FlowAtt').innerHTML;
    countryDIVid.style.display = 'none';
    document.getElementById('topInfoBox_FlowAtt').innerHTML = y+'&nbsp;<i class="icon mdi mdi-18px mdi-earth"></i>&nbsp;'+x;
    $('#radioCountryAtt').fadeOut( 123, "linear", function() { });
  }
  
  try { localStorage.setItem('currWCat', params.newValue); } catch (exux) 
    { if (exux == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }

// hide myselfe 1 step
    document.getElementById('a02stepID').classList.toggle("disabled-step", true);
    document.getElementById('a02stepID').setAttribute('disabled','disabled');
    document.getElementById('a02stepID').classList.toggle("disabled", true);
});
$('#a_f666_kat1a').on('save', function(e, params) {
  document.getElementById('selectWcat').style.display = 'none';
  document.getElementById('a_m666_kat1a').style.display = 'none';
  document.getElementById('a_f666_kat1a').style.display = 'none';
  document.getElementById('name1a').style.display = 'block';
  let x = document.getElementById('topInfoBox_FlowAtt').innerHTML;
  let y = document.getElementById('attWcat').innerHTML = '&nbsp; <kbd>'+params.newValue + '&#13199;</kbd>';
  document.getElementById('a02stepID').classList.toggle("active", false);
  document.getElementById('a03stepID').classList.toggle("disabled", false);
  document.getElementById('a03stepID').classList.toggle("active", true);
  document.getElementById('a03stepID').click();
  document.getElementById('inputDateRandeid').style.display = 'block';
  document.getElementById('bdayDIV_id').style.display = 'block';
  document.getElementById('attAva').style.display = 'block';
  document.getElementById('inputCityTeamAtt').style.display = 'block';    
  //document.getElementById('topInfoBox_FlowAtt').innerHTML = '<b>' + x + '  ' + y + '</b>';
  document.getElementById('image_uploads').style.display = 'block';
  document.getElementById('wcatLabel').style.display = 'none';
  let countryDIVid = document.getElementById('radioCountryAtt');  
    if (window.getComputedStyle(countryDIVid).display === 'block') {
    let y = 'RUS';
    let x = document.getElementById('topInfoBox_FlowAtt').innerHTML;
    countryDIVid.style.display = 'none';
    document.getElementById('topInfoBox_FlowAtt').innerHTML = y+'&nbsp;<i class="icon mdi mdi-18px mdi-earth"></i>&nbsp;'+x;
    $('#radioCountryAtt').fadeOut( 123, "linear", function() { });
  }

  try { localStorage.setItem('currWCat', params.newValue); } catch (e) 
    { if (e == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
    //var date = new Date(new Date().getTime() + 60 * 1000);
    //document.cookie = "wcat=params.newValue; path=/; expires=" + date.toUTCString();

    document.getElementById('a02stepID').classList.toggle("disabled-step", true);
    document.getElementById('a02stepID').setAttribute('disabled','disabled');
    document.getElementById('a01stepID').classList.toggle("disabled", true);
});

$('#radio-img-1').on('click', function (e) {
  $(this).fadeOut( 987, "linear", function() {
  });
  let timerId = setTimeout(function tblRefresh3s() { $("#radioSex").hide(); timerId = setTimeout(tblRefresh3s, 888); }, 999);
  document.getElementById('topInfoBox_FlowAtt').innerHTML = "Мужчины / Юноши";
  let genderSexIS=1;
  $("#selectWcat,#a_m666_kat1a").show();
  document.getElementById('topInfoBox_FlowAtt').classList.toggle("hidden", false);
  document.getElementById('topInfoBox_FlowAtt').classList.toggle("show", true);
  document.getElementById('a01stepID').classList.toggle("active", false);
  document.getElementById('a02stepID').classList.toggle("disabled", false);
  document.getElementById('a02stepID').classList.toggle("active", true);
  document.getElementById('a02stepID').click();
  document.getElementById('wcatLabel').style.display = 'block';
  document.getElementById('wcatLabel').classList.toggle("show", true);
  document.getElementById('a01stepID').classList.toggle("show", false);
  document.getElementById('wcatLabel').style.display = 'block';
  document.getElementById('wcatLabel').classList.toggle("show", true);
  document.getElementById('sexLabel').style.display = 'none';
  document.getElementById('radioCountryAtt').style.display = 'block';
  try { localStorage.setItem('currGender', genderSexIS); } catch (expp) 
    { if (expp == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
var date = new Date(new Date().getTime() + 60 * 1000);
document.cookie = "sex=1; path=/; expires=" + date.toUTCString();

document.getElementById('a01stepID').classList.toggle("disabled-step", true);
document.getElementById('a01stepID').setAttribute('disabled','disabled');
document.getElementById('a01stepID').classList.toggle("disabled", true);
});  
$('#radio-img-2').on('click', function (e) {
  $(this).fadeOut( 987, "linear", function() {
  });
  let timerId = setTimeout(function tblRefresh3s() { $("#radioSex").hide(); timerId = setTimeout(tblRefresh3s, 888); }, 999);
  document.getElementById('topInfoBox_FlowAtt').innerHTML = "Женщины / Девушки";
  let genderSexIS=0;
  $("#selectWcat,#a_f666_kat1a").show();
  document.getElementById('topInfoBox_FlowAtt').classList.toggle("hidden", false);
  document.getElementById('topInfoBox_FlowAtt').classList.toggle("show", true);
  document.getElementById('a01stepID').classList.toggle("active", false);
  document.getElementById('a02stepID').classList.toggle("disabled", false);
  document.getElementById('a02stepID').classList.toggle("active", true);
  document.getElementById('a02stepID').click();
  document.getElementById('wcatLabel').style.display = 'block';
  document.getElementById('wcatLabel').classList.toggle("show", true);  
  document.getElementById('sexLabel').style.display = 'none';
  document.getElementById('radioCountryAtt').style.display = 'block';
  try { localStorage.setItem('currGender', genderSexIS); } catch (exxpp) 
    { if (exxpp == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
var date = new Date(new Date().getTime() + 60 * 1000);
document.cookie = "sex=0; path=/; expires=" + date.toUTCString();

document.getElementById('a01stepID').classList.toggle("disabled-step", true);
document.getElementById('a01stepID').setAttribute('disabled','disabled');
document.getElementById('a01stepID').classList.toggle("disabled", true);

});
//document.getElementById('a01stepID').href = '';
});
</script>


<p class="ttheright"><?=$getGUIpage1HEADER;?> <?=$getGUIpage1HEADERico;?>&nbsp;</p>
  <div class="panel panel-default">
    <div class="panel-heading text-left">
      <?=$getf5header01;?>
    <div class="hover07">
      <div id="addAthtelle" class="new_flow_2arch"><div class="startFlowbkp"> </div></div>
    </div>
    <div class="list-group" id="i_addons" role="tablist">
      <a id="x_fst" class="list-group-item list-group-item-action lnk" data-toggle="list" href="#fst" role="tab"> </a>
      <a id="za01" class="list-group-item list-group-item-action lnk" data-toggle="list" href="#ar_choz" role="tab"><?=$getf5tabName01;?></a>
      <a id="zb02" class="list-group-item list-group-item-action lnk" data-toggle="list" href="#qrj" role="tab"><?=$getf5tabName02;?></a>
      <a id="x_cls" class="list-group-item list-group-item-action lnk" data-toggle="list" href="#cls" role="tab"> </a>
    </div>

<!-- Tab panes -->
    <div class="tab-content">
    <div class="tab-pane fade" id="fst" role="tabpanel"></div>
    <div class="tab-pane fade" id="qrj" role="tabpanel">
    <div class="panel-heading text-left"><?=$getGUIpan01label;?> 
    <div class="container-fluid bg-3 text-center">    
    <div class="row">
      <div class="col-sm-4">
        <p>Judge #1</p><br>
          <div class="qrJuda01" style="width: 123px;" alt="Image"></div><br>
      </div>
      <div class="col-sm-4"> 
        <p>Chief Judge</p><br>
      <div class="qrJuda02" alt="Image"></div><br>
      </div>
      <div class="col-sm-4"> 
        <p>Judge #2</p><br>
      <div class="qrJuda03" alt="Image"></div><br>
      </div>
    </div>
    <div id="pwddiv"> 
      <h2 id="blink0passwd"><?=$getGUIpwd4juda_str01;?></h2>
      <h1 id="blink1passwd_red"><?=$getPWD4judas;?></h1>
    </div>
  </div><br><br>

<div class="hidden alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
!!! HELLO !!!
</div>

</div>
</div>

<div class="tab-pane fade" id="ar_choz" role="tabpanel">
<div class="panel-heading text-left">
<span class="plain-select">
<?php $getBKParchSelect = $f5start->listBKParchives_sel_opt_html(); ?>
</span> 
<button type="button" tabindex="9" id="btn01indexOk" class="btn ModalbuttonSave "><?=$getGUIbtn01;?></button>
</div>
</div>

<div class="tab-pane fade" id="cls" role="tabpanel"></div>
</div>
</div>
</div>



</div> 
</div><!-- /container -->

<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->

</div><!-- /mainbody -->


<!-- Modal content -->
<!-- The Modal addAthlete -->
<div id="addAthletemd" class="modal">
  <div class="modal-content">
    <div id="xcloseid" class="close">&#10006;</div>
    <div><h4 id="idonpomostadd" class="addAthlettehModalheader"><?=$getGUImodalHeader;?></h4></div>
<!-- loadind #preloaderimg content -->
<div class="loaderwrap">
<div id="preloaderimg" class="preloader_shadows"></div>
</div>
 <h2 id="addModalInformTxt" class="addAthlettehModalheader" style="display: none"><?=$getGUImodalSavedINFO;?></h2>


                <form class="form cf">
                    <div class="wizard">
<div class="container">
<!-- <p class="bg-info hidden">.. Flow Info ...</p> -->
<div id="topInfoBox_FlowAtt" class="hidden well well-sm">...</div>
<span id="topInfoBox_AttPII" class="bg-info hidden"></span>
<span id="topInfoBox_countryAtt" class="bg-info hidden"></span>
<span id="cityTeamAtt"></span>
<span id="attBDAYSPAN"></span>
<br><span id="namesSPAN"></span>

<div id="attCountryAVASPAN" class="preview text-center col-xs-2 col-sm-2">
    </div>
</div>

                        <div class="wizard-inner">
                            <!-- <div class="connecting-line"></div> -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item">
                                    <a id="a01stepID" href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?=$getGUIstep01;?>" class="nav-link active">
                                <span class="round-tab">
                                    <i class="icon mdi mdi-36px mdi-human-male-female"></i>
                                </span>
                            </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a id="a02stepID" href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?=$getGUIstep02;?>" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="icon mdi mdi-36px mdi-weight-kilogram"></i>
                                </span>
                            </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a id="a03stepID" href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?=$getGUIstep03;?>" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="icon mdi mdi-36px mdi-face"></i>
                                </span>
                            </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a id="a04stepID" href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="<?=$getGUIstep04;?>" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="icon mdi mdi-36px mdi-arrow-up-bold-box-outline"></i>
                                </span>
                            </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a id="a05stepID" href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="<?=$getGUIstep05;?>" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="icon mdi mdi-36px mdi-arrow-up-bold-circle-outline"></i>
                                </span>
                            </a>
                                </li>
                            </ul>
                        </div>




<div class="tab-content">

<div class="tab-pane active text-center" role="tabpanel" id="step1">
<div class="row" id="radioSex">
  <div class="col-xs-12 col-md-12">
    <input type="radio" id="radio-img-1" name="radio-btns-sprite">
    <label for="radio-img-1" class="male">Male</label>&nbsp;
    <input type="radio" id="radio-img-2" name="radio-btns-sprite">
    <label for="radio-img-2" class="female">Female</label>
  </div>
</div><br>
  <ul id="sexLabel" class="list-inline text-center">
    <li><p class="text-info"><?=$getGUIsexLabel;?></p></li>
  </ul>
</div>


<div class="tab-pane" role="tabpanel" id="step2">
  <div class="row toppER" id="radioCountryAtt">
<p class="text-info"><?=$getGUIcountryLabel;?></p>
<div class="col-xs-12 col-md-12 text-center">
<input type="radio" id="counry1" name="radio-btns-sprite" checked="checked" value="RUS" onchange="getCountry(this.value)">
    <label for="counry1" class="cRussia">RUS</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry2" name="radio-btns-sprite" value="KAZ" onchange="getCountry(this.value)">
    <label for="counry2" class="cKaz">KAZ</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry3" name="radio-btns-sprite" value="BEL" onchange="getCountry(this.value)">
    <label for="counry3" class="cBel">BEL</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry4" name="radio-btns-sprite" value="UKR" onchange="getCountry(this.value)">
    <label for="counry4" class="cUkr">URK</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry5" name="radio-btns-sprite" value="ARM" onchange="getCountry(this.value)">
    <label for="counry5" class="cArm">ARM</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry6" name="radio-btns-sprite" value="GRG" onchange="getCountry(this.value)">
    <label for="counry6" class="cGrg">GRG</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry7" name="radio-btns-sprite" value="UZB" onchange="getCountry(this.value)">
    <label for="counry7" class="cUzb">UZB</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="radio" id="counry8" name="radio-btns-sprite" value="TJK" onchange="getCountry(this.value)">
    <label for="counry8" class="cTjk">TJK</label>               
  </div>
</div> <br>
<div class="row" id="selectWcat">
  <div class="col-xs-10 col-md-10 sxb1">
<a href="#" tabindex="5" id="a_m666_kat1a" data-type="select" data-pk="10" data-title="" data-emptytext="50 kG" data-placeholder="34 kG">50 KG</a><br>
<a href="#" tabindex="6" id="a_f666_kat1a" data-type="select" data-pk="11" data-title="" data-emptytext="53 kG" data-placeholder="34 kG">35 KG</a>
  </div>
    <ul id="wcatLabel" class="list-inline text-center">
    <li><p class="text-info"><?=$getGUIwcatLabel;?></p></li>
  </ul>
</div>

  </div>


<div class="tab-pane" role="tabpanel" id="step3">
    <div class="row toppER" id="inputCityTeamAtt">
<p class="text-info"><?=$getGUIcityTeamLabel;?></p>
<div class="col-xs-12 col-md-12 text-center">
<!-- <input type="text" id="CityGroupATT" name="name_CityGroupATT" onchange="getCountry(this.value)"> -->
<!--Make sure the form has the autocomplete function switched off:-->
<form autocomplete="off" action="/action_page.php">
  <div class="autocomplete" style="width:345px;">
      <input id="myInput" type="text" name="myCountry" placeholder="Country">
  </div>
</form>
  </div>
</div>


<div class="row" id="attAva">
<p class="text-info"><?=$getGUIavaLabel;?></p>
  <div id="avaDIV2top" class="preview text-center col-xs-2 col-sm-2">
 <img id="defAttAvaIMG" src="<?php echo '/img/also/f5defava'.mt_rand(0, 0).'.png'; ?>">  
    </div>
<div class="upload-btn-wrapper">
  <div id="ADDAttAvaIMG" class="btnzzz"></div>
  <input size="10" tabindex="8" type="file" id="image_uploads" name="image_name_uploads" accept=".jpg, .jpeg, .png" />
</div>
  </div><br>

<div class="row" id="bdayDIV_id">
<div class="input-group">
<span class="input-group-addon" id="basic-addon5"><?=$getGUIbDayLabel;?></span>
<input id="inputDateRandeid" value="2003" type="text" size="35" class="datepicker-here form-control" >
</div><br>
  </div>

</div>




  <div class="tab-pane text-center" role="tabpanel" id="step4">
  <div class="col-xs-12 col-md-12 text-center">
    <div id="nameDiv">
    <div id="idattNameF" class="lngbox" title="fn"> <span id="attFName">.</span></div><br>
    <div id="idattNameL" class="lngbox" title="ln"> <span id="attLName">.</span></div>
    </div>
  </div>

<div class="row" id="inputNames">
  <div id="divFname" class="col-xs-6 col-md-4 sxb1"><h3><a tabindex="1" href="#" class="s1" id="name1a"><?=$getGUIstep01lblFnameDef;?></a></h3> <h4><?=$getGUIstep01lblFname;?></h4></div>
  <div id="divLname" class="col-xs-6 col-md-4 sxb1"><h3><a tabindex="2" href="#" class="s1" id="name2a"><?=$getGUIstep01lblLnameDef;?></a></h3> <h4><?=$getGUIstep01lblLname;?></h4></div>
  <div class="col-xs-6 col-md-4">&nbsp;</div>
</div>
  </div>


  <div class="tab-pane text-center" role="tabpanel" id="step5">
    <div class="row" id="att_j1p1">
    <div id="divS1i" class="col-md-2"><h3><a tabindex="7" href="#" type="number" id="s1i">59</a></h3> <h4><p class="kgUcase">&#x338f;</p><?=$getGUIsnatch1;?></h4></div>
    <div id="divCJ1i" class="col-md-2"><h3><a tabindex="8" href="#" type="number" id="cj1i">69</a></h3> <h4><p class="kgUcase">&#x338f;</p><?=$getGUIcj1;?></h4></div>
    </div>
    <button type="button" id="btnModalSaveadd" class="hidden btn btn-primary btnModalSaveadd"> SAVE ALL!</button>
  </div>

  </div>


  <div class="clearfix"></div>
  </div>
  </form>
  </div>


<div class="hidden">
  <div class="col-xs-6 col-md-4">
    <div id="sexDiv">
    <label for="attSex"></label>
    <div id="idattSex" class="lngbox" title=""> <span id="attSex">..</span></div>
    </div>  
  </div>
  <div class="col-xs-6 col-md-4">
    <div id="wcatDiv">
    <label for="attWcat"></label>
    <div id="idattWcat" class="lngbox" title=""> <span id="attWcat">.</span></div>
    </div>
  </div>
  </div>




</div>

<script>
  function setGUIlng(valueToSelect) {
    let lng3code = 'rus';
    if(valueToSelect == 1) { lng3code = 'rus'; $.get("../core/model-f5start.php?setGUIlng", { "l3c": lng3code },
      function(data) {  $("#thenow_w").text(data); console.log("response5: " + data); });
      let timerIdrus = setTimeout(function indRefresh1s() { location.reload(); timerIdrus = setTimeout(indRefresh1s, 1234); }, 1234);
      let hideBtn = document.getElementById("lngPicker").style.height = "40px";
    }
    if(valueToSelect == 2) { lng3code = 'eng'; $.get("../core/model-f5start.php?setGUIlng", { "l3c": lng3code },
      function(data) {  $("#thenow_w").text(data); console.log("response5: " + data); });
      let timerIdeng = setTimeout(function indRefresh1s() { location.reload(); timerIdeng = setTimeout(indRefresh1s, 1234); }, 1234);
      let hideBtn = document.getElementById("lngPicker").style.height = "40px";
    }
    console.log('YAP! lng3code = ' + valueToSelect);
  }
</script>

    <script src="../js/bootstrap300.min.js"></script>
    <script src="../js/bootstrap-editable.min.js"></script>
    <!-- <script src="../js/f5index.core.js"></script> -->
    <script src="../js/libs/typeahead.jquery.min.js"></script>
    <script src="../js/device.min.js"></script>


<script>
  tippy('[title]', { placement: 'bottom', animation: 'scale', inertia: true, size:'large', interactiveBorder:7,duration: 777, followCursor: true, arrow: true });
  if(device.tablet()==true) { document.location.href = "/tab"; }
  if(device.mobile()==true) { document.location.href = "/mob"; }
</script>

<script>


$(document).ready(function(){  
  $('#flagimg').css('opacity','0.2');
  document.getElementById('addModalInformTxt').style.display='none';
  //document.getElementById('outspace').style.display='none';
  //document.getElementById('wcatDiv').style.display="none";
  document.getElementById('addModalInformTxt').style.display='none';
  document.getElementById('preloaderimg').style.display='none';
  $( '#topheader .navbar-nav a' ).on( 'click', function () {
    $( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
    $( this ).parent( 'li' ).addClass( 'active' );
  });    

$(function(){
  $.fn.editable.defaults.mode='inline';
  $.fn.editable.defaults.emptytext='...';
  $.fn.editable.defaults.savenochange=true;
  $.fn.editable.defaults.autotext='auto';
  $.fn.editable.defaults.showbuttons=true;
//  $.fn.editable.defaults.inputclass ="j1p1w";wCatINPIT01
  $.fn.editable.defaults.inputclass = "wCatINPIT01";
  //$.fn.editable.defaults.autotext = 'auto'; //$.fn.editable.defaults.enablefocus = 'true'; 
  $.fn.editable.defaults.onblur = 'onblur'; 
  $('#a_m666_kat1a').editable({ 
        enablefocus: true,
        inputclass:'input-wcatlarge',
        showbuttons: false,
        source: [
{text: "Men's category", children: [{value: 34, text: "34 kG"}, {value: 38, text: "38 kG"},{value: 42, text: "42 kG"},{value: 46, text: "46 kG"},{value: 50, text: "50 kG"},{value: 56, text: "56 kG"},{value: 62, text: "62 kG"},{value: 69, text: "69 kG"},{value: 6969, text: "+69 kG"},{value: 77, text: "77 kG"},{value: 85, text: "85 kG"},{value: 94, text: "94 kG"},{value: 9494, text: "+94 kG"},{value: 105, text: "105 kG"},{value: 105105, text: "+105 kG"} ]},
           ]
    });
$('#a_f666_kat1a').editable({ 
        enablefocus: true,
        inputclass:'input-wcatlarge',
        showbuttons: false,
        source: [
{text: "Woman's category", children: [{value: 34, text: "34 kG"}, {value: 36, text: "36 kG"},{value: 40, text: "40 kG"},{value: 44, text: "44 kG"},{value: 48, text: "48 kG"},{value: 53, text: "53 kG"},{value: 58, text: "58 kG"},{value: 63, text: "63 kG"},{value: 69, text: "69 kG"},{value: 6969, text: "+69 kG"},{value: 75, text: "75 kG"},{value: 90, text: "90 kG"},{value: 9090, text: "+90 kG"}]}
           ]
    });
  $("#country1a").editable({ value: 1,source: [
    {value: 1, text: "RUS"},{value: 2, text: "KAZ"},{value: 3, text: "BEL"},{value: 4, text: "UKR"},{value: 5, text: "UZB"},{value: 6, text: "TJK"},{value: 7, text: "ARM"},{value: 8, text: "GRG"}
    ] });
  $("#name1a").editable({ url: "../core/model-db.php?get_name1=tmpChange"  });
  $("#name2a").editable({ url: "../core/model-db.php?get_name1=tmpChange"  });
  $("#s1i").editable({ 
    url: "../core/model-db.php?get_name1=tmpChange",
    enablefocus: true,
    inputclass:'input-weightQuery',
    showbuttons: true
      });
  $("#cj1i").editable({ 
    url: "../core/model-db.php?get_name1=tmpChange",
    enablefocus: true,
    inputclass:'input-weightQuery',
    showbuttons: true
          });
}); 

$(function () {
    $('.btn-radio').click(function(e) {
        $('.btn-radio').not(this).removeClass('active')
        .siblings('input').prop('checked',false)
            .siblings('.img-radio').css('opacity','0.2');
      $(this).addClass('active')
            .siblings('input').prop('checked',true)
        .siblings('.img-radio').css('opacity','0.9');
    });
});


let timerIdpwdiv = setTimeout(function hide0pwdDiv() { document.getElementById('pwddiv').style.display='none'; timerId = setTimeout(hide0pwdDiv, 7234); }, 7234);

    function uploadFile(file){
    let url = '../core/f5uploader.php';
    let xhr = new XMLHttpRequest();
    let fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            console.log(xhr.responseText); // handle response.
        }
    };
    fd.append("upload_file", file);
    xhr.send(fd);
}


let uploadfiles = document.getElementById('image_uploads');
  uploadfiles.addEventListener('change', function () {
    let files = this.files;
    var fileInput = document.getElementById('image_uploads');   
    var filename = fileInput.files[0].name;
    for(let i=0; i<files.length; i++){
      try { localStorage.setItem('currAva', filename); } catch (exxpp) 
      { if (exxpp == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }
      uploadFile(this.files[i]); // call the function to upload the file
      }
  }, false);


//let input = document.querySelector('input');
let input = document.getElementById('image_uploads');
let preview = document.querySelector('.preview');
let hideDefAVA = document.getElementById('defAttAvaIMG');
let hideAddAVA = document.getElementById('ADDAttAvaIMG');
let hideAddAVAPrevDIV = document.getElementById('avaDIV2top');
let attAvaDIV = document.getElementById('attAva');
let attBdayDIV = document.getElementById('bdayDIV_id');


//input.style.opacity = 0;

input.addEventListener('change', updateImageDisplay); function updateImageDisplay() {
  while(preview.firstChild) {
    preview.removeChild(preview.firstChild);
    hideDefAVA.style.display = 'none'; 
    hideAddAVA.style.display = 'none'; 
    hideAddAVAPrevDIV.style.display = 'none';
    hideAddAVAPrevDIV.style.display = 'none';
    attAvaDIV.style.display = 'none';
//    attBdayDIV.style.display = 'none';


    //let timerIdpwdiv = setTimeout(function hide0Div() { document.getElementById('DIVimage_uploads').style.display='none'; timerId = setTimeout(hide0Div, 2999); }, 2898);
    
  }

  let curFiles = input.files;
  if(curFiles.length === 0) {
    let para = document.createElement('p');
    para.textContent = 'No files currently selected for upload';
    preview.appendChild(para);
  } else {
    let list = document.createElement('ul');
    preview.appendChild(list);
    for(let i = 0; i < curFiles.length; i++) {
      let listItem = document.createElement('li');
      let para = document.createElement('p');
      if(validFileType(curFiles[i])) {
        //para.textContent = 'File name ' + curFiles[i].name + ', file size ' + returnFileSize(curFiles[i].size) + '.';
        
        let image = document.createElement('img');
        image.src = window.URL.createObjectURL(curFiles[i]);
        image.setAttribute("id", "inf0");

        listItem.appendChild(image);
        listItem.appendChild(para);

      } else {
        para.textContent = 'File name ' + curFiles[i].name + ': Not a valid file type.';
        listItem.appendChild(para);
      }

      list.appendChild(listItem);
    }
  }
}

let fileTypes = [
  'image/jpeg',
  'image/pjpeg',
  'image/png'
]

function validFileType(file) {
  for(let i = 0; i < fileTypes.length; i++) {
    if(file.type === fileTypes[i]) {
      return true;
    }
  }

  return false;
}

function returnFileSize(number) {
  if(number < 1024) {
    return number + 'bytes';
  } else if(number > 1024 && number < 1048576) {
    return (number/1024).toFixed(1) + 'KB';
  } else if(number > 1048576) {
    return (number/1048576).toFixed(1) + 'MB';
  }
}

    });






/*
      function sh2the_flag() { 
      $( "#counryid li" ).click( function() {
      let ccode = this.id; ccode = '../img/flags/'+ccode+'.png';
      $( "#flag_country" ).prop('src', ccode);
    //console.log(ccode);
    })
}
*/

// Get the modal
let modal = document.getElementById('addAthletemd');
// Get the button that opens the modal
let btn = document.getElementById("addAthtelle");
// Get the <span> element that closes the modal
let xspan = document.getElementById("xcloseid");
let btnSave = document.getElementById("btnModalSaveadd");
// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
    let btn = document.getElementById("addAthtelle");
    let fadediv = document.getElementById("mainbody");
    fadediv.className = "blurBg";
}
// When the user clicks on <span> (x), close the modal
xspan.onclick = function() {
    modal.style.display = "none";
    let fadediv = document.getElementById("mainbody");
    fadediv.className -= "blurBg";
    fadediv.class   -= "blurBg";
}
// btnModalClose
btnSave.onclick = function() {
  setTimeout(function () { let url='m'; location.replace(url);  }, 1234);
  let loader = document.getElementById("preloaderimg");
    loader.style.display = "block";
    let fadediv     = document.getElementById("mainz");
    fadediv.className += "blurBg";

  let currGender = localStorage.getItem('currGender');
  let currContry = localStorage.getItem('currContry'); 
  let imgFl = 1; // tempDefa
  switch (currContry) {
    case 'RUS': { imgFl = '1'; imgFlsh = 'RUS'; break; }
    case 'KAZ': { imgFl = '2'; imgFlsh = 'KAZ'; break; }
    case 'BEL': { imgFl = '3'; imgFlsh = 'BEL'; break; }
    case 'UKR': { imgFl = '4'; imgFlsh = 'UKR'; break; }
    case 'UZB': { imgFl = '5'; imgFlsh = 'UZB'; break; }
    case 'TJK': { imgFl = '6'; imgFlsh = 'TJK'; break; }
    case 'TJK': { imgFl = '7'; imgFlsh = 'ARM'; break; }
    case 'GRG': { imgFl = '8'; imgFlsh = 'GRG'; break; }
    default: { imgFl = '1'; imgFlsh = 'RUS'; break; }
}
if (currContry!=null)  currContry = imgFl;//'f5defava0.png';
if (currContry===null)  currContry = '1';

  let currWCat = localStorage.getItem('currWCat');
  let currCityTeam = localStorage.getItem('currCityTeam');
  if (currCityTeam===null) currCityTeam = 'Сургут';
  let currAva = localStorage.getItem('currAva');
  if (currAva===null)  currAva = 'f5defava0.png';
  let currBDay = localStorage.getItem('currBDay');
  let currFNAme = localStorage.getItem('currFNAme');
  let currLName = localStorage.getItem('currLName');
  let currS1order = localStorage.getItem('currS1order');
  let currCJ1order = localStorage.getItem('currCJ1order');
    let fName1val   = currFNAme;
    let fName2val   = currLName;
    let j1val       = currS1order;
    let p1val       = currCJ1order;
    let sex         = currGender;
    let avatar      = currAva; //document.getElementById("image_uploads");
    let weight_cat  = currWCat + ' kg';
    let bkpCmd      = 'fullbkp';

$.get("../core/model-db.php", { "newBackup":bkpCmd,"set_sex": sex,"set_name1": fName1val,"set_name2": fName2val,"att_bday": currBDay,"set_j1": j1val,"set_p1":p1val, "set_ava": avatar, "weight_cat": weight_cat, "att_country":currContry ,"att_cityteam":currCityTeam },
function(data) { console.log("response: " + data); });
  document.getElementById('addModalInformTxt').style.display='block';
  $('#addModalInformTxt').addClass('blink_me');
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        let fadediv = document.getElementById("mainbody");
        fadediv.className -= "blurBg";

    }
}
        function DropD(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('.dropd a');
        this.val = '';
        this.index = -1;
        this.initEvents();
      }
      DropD.prototype = {
        initEvents : function() {
          let obj = this;

          obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
          });

          obj.opts.on('click',function(){
            let opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
          });
        },
        getValue : function() {
          return this.val;
        },
        getIndex : function() {
          return this.index;
        }
      }

      $(function() {

        let dd = new DropD( $('#dd') );
      });



</script>








 <script>

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;          
              let getCityTeam = this.getElementsByTagName("input")[0].value;
              var labEEEl = document.getElementById('cityTeamAtt');
              labEEEl.innerHTML = '<b>' + getCityTeam + ' </b> ';
              $('#inputCityTeamAtt').fadeOut( 123, "linear", function() { });
              try { localStorage.setItem('currCityTeam', getCityTeam); } catch (exxpp) 
                { if (exxpp == QUOTA_EXCEEDED_ERR)  { alert('localStorage QUOTA_EXCEEDED_ERROR!'); }  }

              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
      });
}


/*An array containing all the country names in the world:*/
let countries = ['Абакан','Абдулино','Абинск','Агидель','Агрыз','Адыгейск','Азнакаево','Азов','Ак-Довурак','Аксай','Алагир','Алапаевск','Алатырь','Алдан','Алейск','Александров','Александровск','Александровск-Сахалинский','Алексеевка','Алексин','Алзамай','Альметьевск','Амурск','Анадырь','Анапа','Ангарск','Андреаполь','Анжеро-Судженск','Анива','Апатиты','Апрелевка','Апшеронск','Арамиль','Аргун','Ардатов','Ардон','Арзамас','Арзамас','Аркадак','Армавир','Арсеньев','Архангельск','Асбест','Асино','Астрахань','Аткарск','Ахтубинск','Ачинск','Аша','Бабаево','Бабушкин','Бавлы','Багратионовск','Байкальск','Баймак','Бакал','Баксан','Балабаново','Балаково','Балахна','Балахна','Балашиха','Балашов','Балей','Балтийск','Барабинск','Барнаул','Барыш','Батайск','Бежецк','Белая Калитва','Белая Холуница','Белгород','Белебей','Белинский','Белово','Белогорск','Белозерск','Белокуриха','Беломорск','Белорецк','Белореченск','Белоусово','Белоярский','Белый','Бердск','Березники','Беслан','Бийск','Бикин','Билибино','Биробиджан','Бирск','Бирюсинск','Бирюч','Благовещенск','Благовещенск','Благодарный','Бобров','Богданович','Богородицк','Богородск','Богородск','Боготол','Богучар','Бодайбо','Бокситогорск','Болгар','Бологое','Болотное','Болохово','Болхов','Большой Камень','Бор','Бор','Борзя','Борисоглебск','Боровичи','Боровск','Бородино','Братск','Бронницы','Брянск','Бугульма','Бугуруслан','Бузулук','Буинск','Буй','Буйнакск','Бутурлиновка','Валдай','Валуйки','Велиж','Великие Луки','Великий Новгород','Великий Устюг','Вельск','Верещагино','Верея','Верхнеуральск','Верхний Тагил','Верхний Уфалей','Верхняя Пышма','Верхняя Салда','Верхняя Тура','Верхотурье','Верхоянск','Весьегонск','Ветлуга','Ветлуга','Видное','Вилюйск','Вилючинск','Вихоревка','Вичуга','Владивосток','Владикавказ','Владимир','Волгоград','Волгодонск','Волгореченск','Волжск','Волжский','Вологда','Володарск','Володарск','Волоколамск','Волосово','Волхов','Волчанск','Вольск','Воркута','Воронеж','Ворсма','Ворсма','Воскресенск','Воткинск','Всеволожск','Вуктыл','Выборг','Выкса','Выкса','Высоковск','Высоцк','Вытегра','Вяземский','Вязники','Вязьма','Вятские Поляны','Гаврилов Посад','Гаврилов-Ям','Гагарин','Гаджиево','Гай','Галич','Гатчина','Гвардейск','Гдов','Геленджик','Георгиевск','Глазов','Горбатов','Горбатов','Горно-Алтайск','Горнозаводск','Горнозаводск','Горняк','Городец','Городец','Городище','Городовиковск','Гороховец','Горячий Ключ','Грайворон','Гремячинск','Грозный','Грязи','Грязовец','Губаха','Губкин','Губкинский','Гудермес','Гуково','Гулькевичи','Гурьевск','Гурьевск','Гусев','Гусь-Хрустальный','Давлеканово','Дагестанские Огни','Далматово','Дальнегорск','Дальнереченск','Данилов','Данков','Дегтярск','Дедовск','Демидов','Дербент','Десногорск','Дзержинск','Дзержинск','Дзержинский','Дивногорск','Дигора','Димитровград','Дмитров','Дмитровск','Дно','Добрянка','Долгопрудный','Долинск','Домодедово','Донецк','Донской','Дорогобуж','Дрезна','Дубна','Дубовка','Дудинка','Духовщина','Дюртюли','Дятьково','Егорьевск','Ейск','Екатеринбург','Елабуга','Елец','Елизово','Ельня','Еманжелинск','Емва','Енисейск','Ермолино','Ершов','Ессентуки','Ефремов','Железноводск','Железногорск','Железногорск','Железногорск-Илимский','Железнодорожный','Жердевка','Жиздра','Жирновск','Жуков','Жуковка','Жуковский','Завитинск','Заводоуковск','Заволжск','Заволжье','Заволжье','Задонск','Заинск','Закаменск','Западная Двина','Заполярный','Зарайск','Заречный','Заречный','Заринск','Звенигово','Звенигород','Зверево','Зеленогорск','Зеленоградск','Зеленодольск','Зеленокумск','Зерноград','Зея','Зима','Златоуст','Злынка','Змеиногорск','Знаменск','Зубцов','Зуевка','Ивангород','Иваново','Ивантеевка','Ивдель','Игарка','Ижевск','Избербаш','Изобильный','Иланский','Инза','Инсар','Инта','Ипатово','Ирбит','Иркутск','Исилькуль','Искитим','Истра','Ишим','Ишимбай','Йошкар-Ола','Кадников','Казань','Калач','Калачинск','Калач-на-Дону','Калининград','Калининск','Калтан','Калуга','Калязин','Камбарка','Каменка','Каменногорск','Каменск-Уральский','Каменск-Шахтинский','Камень-на-Оби','Камешково','Камызяк','Камышин','Камышлов','Канаш','Кандалакша','Канск','Карабаново','Карабаш','Карабулак','Карасук','Карачаевск','Карачев','Каргат','Каргополь','Карпинск','Карталы','Касимов','Касли','Каспийск','Катав-Ивановск','Катайск','Качканар','Кашин','Кашира','Кедровый','Кемерово','Кемь','Кизел','Кизилюрт','Кизляр','Кимовск','Кимры','Кингисепп','Кинель','Кинешма','Киреевск','Киренск','Киржач','Кириллов','Кириши','Киров','Киров','Кировград','Кирово-Чепецк','Кировск','Кировск','Кирс','Кирсанов','Кисловодск','Климовск','Клин','Клинцы','Княгинино','Княгинино','Ковдор','Ковров','Ковылкино','Когалым','Кодинск','Козельск','Козловка','Козьмодемьянск','Кола','Кологрив','Коломна','Колпашево','Кольчугино','Коммунар','Комсомольск','Комсомольск-на-Амуре','Конаково','Кондопога','Кондрово','Константиновск','Копейск','Кораблино','Кореновск','Коркино','Короча','Корсаков','Коряжма','Костерево','Костомукша','Кострома','Котельниково','Котельнич','Котлас','Котово','Котовск','Кохма','Красавино','Красноармейск','Красноармейск','Красновишерск','Красногорск','Краснодар','Краснозаводск','Краснознаменск','Краснознаменск','Краснокаменск','Краснокамск','Краснослободск','Краснослободск','Краснотурьинск','Красноуральск','Красноуфимск','Красноярск','Красный Кут','Красный Сулин','Красный Холм','Кропоткин','Крымск','Кстово','Кстово','Кувандык','Кувшиново','Кудымкар','Кузнецк','Куйбышев','Кулебаки','Кулебаки','Кумертау','Кунгур','Купино','Курган','Курганинск','Курильск','Курлово','Куровское','Курск','Куртамыш','Курчатов','Куса','Кушва','Кызыл','Кыштым','Кяхта','Лабинск','Лабытнанги','Лагань','Ладушкин','Лакинск','Лангепас','Лахденпохья','Лебедянь','Лениногорск','Ленинск','Ленинск-Кузнецкий','Ленск','Лермонтов','Лесной','Лесозаводск','Лесосибирск','Ливны','Липецк','Липки','Лиски','Лихославль','Лобня','Лодейное Поле','Лосино-Петровский','Луга','Луза','Лукоянов','Лукоянов','Луховицы','Лысково','Лысково','Лысьва','Лыткарино','Льгов','Любань','Люберцы','Любим','Людиново','Лянтор','Магадан','Магас','Магнитогорск','Майкоп','Майский','Макаров','Макарьев','Макушино','Малая Вишера','Малгобек','Малмыж','Малоархангельск','Малоярославец','Мамадыш','Мамоново','Мантурово','Мариинск','Мариинский Посад','Маркс','Махачкала','Мглин','Мегион','Медвежьегорск','Медногорск','Медынь','Межгорье','Междуреченск','Мезень','Меленки','Мелеуз','Менделеевск','Мензелинск','Мещовск','Миасс','Микунь','Миллерово','Минеральные Воды','Минусинск','Миньяр','Мирный','Мирный','Михайлов','Михайловка','Михайловск','Михайловск','Мичуринск','Могоча','Можайск','Можга','Моздок','Мончегорск','Морозовск','Моршанск','Мосальск','Москва','Муравленко','Мураши','Мурманск','Муром','Мценск','Мыски','Мытищи','Мышкин','Набережные Челны','Навашино','Навашино','Наволоки','Надым','Назарово','Назрань','Называевск','Нальчик','Нариманов','Наро-Фоминск','Нарткала','Нарьян-Мар','Находка','Невель','Невельск','Невинномысск','Невьянск','Нелидово','Неман','Нерехта','Нерчинск','Нерюнгри','Нестеров','Нефтегорск','Нефтекамск','Нефтекумск','Нефтеюганск','Нея','Нижневартовск','Нижнекамск','Нижнеудинск','Нижние Серги','Нижний Ломов','Нижний Новгород','Нижний Новгород','Нижний Тагил','Нижняя Салда','Нижняя Тура','Николаевск','Николаевск-на-Амуре','Никольск','Никольск','Никольское','Новая Ладога','Новая Ляля','Новоалександровск','Новоалтайск','Новоаннинский','Нововоронеж','Новодвинск','Новозыбков','Новокубанск','Новокузнецк','Новокуйбышевск','Новомичуринск','Новомосковск','Новопавловск','Новоржев','Новороссийск','Новосибирск','Новосиль','Новосокольники','Новотроицк','Новоузенск','Новоульяновск','Новоуральск','Новохоперск','Новочебоксарск','Новочеркасск','Новошахтинск','Новый Оскол','Новый Уренгой','Ногинск','Нолинск','Норильск','Ноябрьск','Нурлат','Нытва','Нюрба','Нягань','Нязепетровск','Няндома','Облучье','Обнинск','Обоянь','Обь','Одинцово','Ожерелье','Октябрьск','Октябрьский','Окуловка','Оленегорск','Олонец','Омск','Омутнинск','Онега','Опочка','Орёл','Оренбург','Орехово-Зуево','Орлов','Орск','Оса','Осинники','Осташков','Остров','Островной','Острогожск','Отрадное','Отрадный','Оха','Оханск','Павлово','Павлово','Павловск','Павловский Посад','Палласовка','Партизанск','Певек','Пенза','Первомайск','Первомайск','Первоуральск','Перевоз','Перевоз','Пересвет','Переславль-Залесский','Пермь','Пестово','Петров Вал','Петровск','Петровск-Забайкальский','Петрозаводск','Петропавловск-Камчатский','Петухово','Петушки','Печора','Печоры','Пионерский','Питкяранта','Плавск','Пласт','Поворино','Подольск','Подпорожье','Покачи','Покров','Покровск','Полевской','Полесск','Полысаево','Полярные Зори','Полярный','Поронайск','Порхов','Похвистнево','Почеп','Починок','Пошехонье','Правдинск','Приволжск','Приморск','Приморско-Ахтарск','Приозерск','Прокопьевск','Пролетарск','Протвино','Прохладный','Псков','Пудож','Пустошка','Пучеж','Пушкино','Пущино','Пыталово','Пыть-Ях','Пятигорск','Радужный','Радужный','Райчихинск','Раменское','Рассказово','Ревда','Реж','Реутов','Ржев','Родники','Рославль','Россошь','Ростов-на-Дону','Рошаль','Ртищево','Рубцовск','Рудня','Руза','Рузаевка','Рыбинск','Рыбное','Рыльск','Ряжск','Рязань','Салават','Салаир','Салехард','Сальск','Самара','Санкт-Петербург','Саранск','Сарапул','Саратов','Саров','Саров','Сасово','Сатка','Сафоново','Саяногорск','Саянск','Светлогорск','Светлоград','Светлый','Светогорск','Свирск','Свободный','Себеж','Северобайкальск','Северодвинск','Северо-Курильск','Североморск','Североуральск','Северск','Севск','Сегежа','Сельцо','Семикаракорск','Семилуки','Сенгилей','Серафимович','Сергач','Сергач','Сергиев Посад','Сердобск','Серов','Серпухов','Сертолово','Сибай','Сим','Сковородино','Скопин','Славгород','Славск','Славянск-на-Кубани','Сланцы','Слободской','Слюдянка','Смоленск','Снежинск','Снежногорск','Собинка','Советск','Советск','Советск','Советская Гавань','Советский','Сокол','Солигалич','Соликамск','Солнечногорск','Сольвычегодск','Соль-Илецк','Сольцы','Сорочинск','Сорск','Сортавала','Сосенский','Сосновка','Сосновоборск','Сосновый Бор','Сосногорск','Сочи','Спас-Деменск','Спас-Клепики','Спасск','Спасск-Дальний','Спасск-Рязанский','Среднеколымск','Среднеуральск','Сретенск','Ставрополь','Старая Русса','Старица','Стародуб','Старый Оскол','Стерлитамак','Стрежевой','Строитель','Струнино','Ступино','Суворов','Суджа','Судогда','Суздаль','Суоярви','Сураж','Сургут','Суровикино','Сурск','Сусуман','Сухиничи','Сухой Лог','Сходня','Сызрань','Сыктывкар','Сысерть','Сычевка','Сясьстрой','Тавда','Таганрог','Тайга','Тайшет','Талдом','Талица','Тамбов','Тара','Таруса','Татарск','Таштагол','Тверь','Теберда','Тейково','Темников','Темрюк','Терек','Тетюши','Тимашевск','Тихвин','Тихорецк','Тобольск','Тогучин','Тольятти','Томари','Томмот','Томск','Топки','Торжок','Торопец','Тосно','Тотьма','Трехгорный','Троицк','Троицк','Трубчевск','Туапсе','Туймазы','Тула','Тулун','Туран','Туринск','Тутаев','Тында','Тырныауз','Тюкалинск','Тюмень','Уварово','Углегорск','Углич','Удачный','Удомля','Ужур','Узловая','Улан-Удэ','Ульяновск','Унеча','Урай','Урень','Урень','Уржум','Урус-Мартан','Урюпинск','Усинск','Усмань','Усолье','Усолье-Сибирское','Уссурийск','Усть-Джегута','Усть-Илимск','Усть-Катав','Усть-Кут','Усть-Лабинск','Устюжна','Уфа','Ухта','Учалы','Уяр','Фатеж','Фокино','Фокино','Фролово','Фрязино','Фурманов','Хабаровск','Хадыженск','Ханты-Мансийск','Харабали','Харовск','Хасавюрт','Хвалынск','Хилок','Химки','Холм','Холмск','Хотьково','Цивильск','Цимлянск','Чадан','Чайковский','Чапаевск','Чаплыгин','Чебаркуль','Чебоксары','Чегем','Чекалин','Челябинск','Чердынь','Черемхово','Черепаново','Череповец','Черкесск','Черноголовка','Черногорск','Чернушка','Черняховск','Чехов','Чистополь','Чита','Чкаловск','Чкаловск','Чудово','Чулым','Чусовой','Чухлома','Шагонар','Шадринск','Шали','Шарыпово','Шарья','Шатура','Шахты','Шахунья','Шахунья','Шацк','Шебекино','Шелехов','Шенкурск','Шилка','Шимановск','Шиханы','Шлиссельбург','Шумерля','Шумиха','Шуя','Щербинка','Щигры','Щучье','Электрогорск','Электросталь','Электроугли','Элиста','Энгельс','Эртиль','Юбилейный','Югорск','Южа','Южно-Сахалинск','Южно-Сухокумск','Южноуральск','Юрга','Юрьевец','Юрьев-Польский','Юрюзань','Юхнов','Ядрин','Якутск','Ялуторовск','Янаул','Яранск','Яровое','Ярославль','Ярцево','Ясногорск','Ясный','Яхрома'];


/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);
</script>



<?php require_once('v/f-f5index.php'); ?>
</body>
</html>