
<title><?php echo $getHTMLpageTITLE?></title>

<!-- f5-core maibody div wrapper container --> 
<div id="about_mainbody">
<!-- f5-core main wrapper container -->

<div class="container">
  <div class="row">
    <div class="col-12 text-center">
    <div class="box">
      <div class="box-icon">
        <img class="logof5about" src="/img/f5-logo-blue-el.png">
      </div>
      <small id="f5BLACKfullname"><kbd><?=$getf5fullName;?></kbd></small>
    <div class="info">
<h3 class="text-center f5font text-successf5""><?=$f5titteName;?></h3>
  
<div class="alert alert-light" role="alert">What is f5/fly system?!!</div>
<p class="text-primary">f5/fly - it is SPORT Scoreboard & Judicial system. Simply is web site, with full auto sport Events&Judding system.</p>

<p>The birth of the idea of f5/fly and the first version of the system was started in 2012!<br>
We, the f5/fly system called Scoreboard or autoservice. According to the rules of our sport, f5/fly is an electronic judging light alarm system of the referee.</p>

<p>Today, f5 / fly is a system created thanks to many living creatures! At the origins of the same stood
<br>&nbsp;&nbsp;&nbsp; &#4055; <i>Sergey A. Pakko</i> <a href="mailto:sap@f5fly.ru"><i class="icon mdi mdi-18px mdi-email-outline"></i></a>
<br>&nbsp;&nbsp;&nbsp; &#4056; <i>Alexander M. Prostyakov</i> <a href="mailto:amp@f5fly.ru"><i class="icon mdi mdi-18px mdi-email-outline"></i></a>
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#9788; and many other people and living being</p>

<address>
  <strong>f5fly.ru</strong><br>
  628672 Parkovaya Street 10 <br>
  Langepas city, Russia<br>
  <abbr title="Phone">phone:</abbr> +7 (929) 249-1823
</address>

<address>
  <strong>for any questions and suggestions</strong><br>
  <a href="mailto:hello@f5fly.ru">hello@f5fly.ru</a><br>
  <a href="https://f5fly.ru">https://f5fly.ru</a>
</address>



<p>
 <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
 System Info: <i class="icon mdi mdi-18px mdi-arrow-down-bold"></i>
  </button>
</p>
<div class="collapse" id="collapseExample">
  <div class="card card-body">
 
 <?php echo '<h5 class="text-center">system info:</h5>';
  echo '<table cellpadding="3">' ; 
  echo '<tr><td>'.$_SERVER[PHP_SELF].'</td>';
  echo '<tr><td>'.$_SERVER[SERVER_ADDR].'</td>'; echo '<tr><td>'.$_SERVER[SERVER_NAME].'</td>';
  echo '<tr><td>'.$_SERVER[SERVER_PROTOCOL].'</td>';
  echo '<tr><td>'.$_SERVER[DOCUMENT_ROOT].'</td>';
  echo '<tr><td>'.$_SERVER[SCRIPT_NAME].'</td>';
  echo '<tr><td>'.$_SERVER[HTTP_HOST].'</td>';
  echo '<tr><td>'.$_SERVER[REMOTE_ADDR].'</td>';
  echo '<tr><td>'.$_SERVER[REQUEST_URI].'</td>';
  echo '<tr><td>SSL:'.$_SERVER[HTTPS].'</td>';
  echo '<tr><td>PORT:'.$_SERVER[SERVER_PORT].'</td>';
  echo '</table><br>'; ?>


<h5 class="text-center">What we used in the preparation of <b>f5/fly</b></h5>
<div class="containerTech">
  <div class="col-sm"><i class="icon mdi mdi-36px mdi-language-html5"> </i><?='HyperText Markup Language';?></div>
  </div>
  <div class="containerTech">
  <div class="col-sm">&nbsp;</div>
  <div class="col-sm"><i class="icon mdi mdi-36px mdi-language-php"> </i><?='Hypertext Preprocessor';?></div>
</div>  
<div class="containerTech">
  <div class="col-sm"><i class="icon mdi mdi-36px mdi-language-css3"> </i><?='Style sheet language';?></div>
</div>
  <div class="containerTech">
  <div class="col-sm"><i class="icon mdi mdi-36px mdi-language-javascript"> </i><?='High-level, interpreted programming language';?></div>
</div>
<div class="containerTech">
  <div class="col-sm"><i class="icon mdi mdi-36px mdi-power-socket"> </i><?='websocket ( RFC 6455 )';?></div>
</div>

<br><h5 class="text-center">Have a nice day!</h5>

  </div>

</div>
<br><br>

<!-- <a href="" class="btn">ok</a> -->
</div>



</div>
</div>
</div>
</div>
<br><br>
</div>




<script> 
       $(document).ready(function(){
      let i = 5;
      i++;

});


function SelectMinutes2start(valueToSelect)
{    
    let timerbox_ = document.getElementById('mmin00');
    timerbox_.innerHTML = valueToSelect;

}

$(function(){
  $(".popup").hide();
  $(".openpop").click(function (e) {
    e.preventDefault();
    $("iframe").attr("src", $(this).attr('href'));
    $(".links").fadeOut('slow');
    $(".popup").fadeIn('slow');
  });
  $(".close").click(function () {
    $(this).parent().fadeOut("slow");
    $(".links").fadeIn("slow");
  });
  $.fn.editable.defaults.mode = 'inline';     
  $.fn.editable.defaults.autotext = 'auto';
  $.fn.editable.defaults.enablefocus = 'true';
  $.fn.editable.defaults.onblur = 'onblur';
  //$.fn.editable.defaults.defaultValue = 'ВВЕДИ ДАННЫЕ';
  //$.fn.editable.defaults.emptytext = 'ПУСТО';
     $('#a_666_kat1a').editable({
        value: 48,  
        enablefocus: true, 
        source: [
{text: "MAN", children: [{value: 34, text: "34 kG"}, {value: 38, text: "38 kG"},{value: 42, text: "42 kG"},{value: 46, text: "46 kG"},{value: 50, text: "50 kG"},{value: 56, text: "56 kG"},{value: 62, text: "62 kG"},{value: 69, text: "69 kG"},{value: 6969, text: "+69 kG"},{value: 77, text: "77 kG"},{value: 85, text: "85 kG"},{value: 94, text: "94 kG"},{value: 9494, text: "+94 kG"},{value: 105, text: "105 kG"},{value: 105105, text: "+105 kG"} ]},
{text: "WOMAN", children: [{value: 34, text: "34 kG"}, {value: 36, text: "36 kG"},{value: 40, text: "40 kG"},{value: 44, text: "44 kG"},{value: 48, text: "48 kG"},{value: 53, text: "53 kG"},{value: 58, text: "58 kG"},{value: 63, text: "63 kG"},{value: 69, text: "69 kG"},{value: 6969, text: "+69 kG"},{value: 75, text: "75 kG"},{value: 90, text: "90 kG"},{value: 9090, text: "+90 kG"}]}
           ]
    }); 
});



$(function(){
    $('#country1a').editable({
        value: 1, 
        enablefocus: true,    
        source: [
              {value: 1, text: 'RUS'},
              {value: 2, text: 'KAZ'},
              {value: 3, text: 'BEL'},
              {value: 4, text: 'UKR'},
              {value: 6, text: 'UZB'},
              {value: 7, text: 'TJK'}
           ], pk: 1, url: '../core/model-db.php?set_sex' });
    });
$(function(){ $('#p1i,#j1i').editable({ enablefocus: true, pk: 1, url: 'core/model-db.php?', 

    success: function(){ $('#tax_rate').trigger('click'); }

 }); });


$(function(){ $('#name1a,#name2a').editable({ enablefocus: true, pk: 1, url: '../core/model-db.php?' }); });
$(function(){ $('#sex').editable({ enablefocus: true, defaultValue : '1', type: 'select', title: 'Select gender', placement: 'right',  value: 1,
     source: [ {value: 1, text: 'муж'}, {value: 0, text: 'жен'} ], pk: 1, url: '../core/model-db.php?set_sex' }); });

    
</script>

    <script src="../js/bootstrap300.min.js"></script>
    <script src="../js/bootstrap-editable.min.js"></script>
    <script src="../js/device.min.js"></script>

<script type="text/javascript">
      if(device.mobile()==true || device.tablet()==true || device.android()==true || device.iphone()==true || device.ipad()==true || device.androidTablet()==true) { document.location.href = "/m"; }
    </script>



</body>
</html>