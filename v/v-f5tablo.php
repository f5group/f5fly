<!DOCTYPE html>
<html>
<head>
  <meta charset='UTF-8' />
<meta http-equiv="Cache-Control" content="no-cache">
<title>f5.fly Главное Табло - Сарики</title>
<meta http-equiv="Cache-Control" content="no-cache">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../css/f5tablo.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/f5.barbell.css">

        <link rel="stylesheet" type="text/css" href="../css/css3rd/demo.css" />
        <link rel="stylesheet" type="text/css" href="../css/css3rd/style2.css" />

  <script src="../js/jquery-1.11.1.min.js"></script>
  <script src="../js/fancywebsocket.js"></script>
  <script src="../js/chrono.js"></script>
  <script src="../js/f5core.j.js"></script>

  <script type="text/javascript" src="../js/libs/modernizr.custom.js"></script>

</head>
<body>


<script>
var Server;
var jcmd, j1x, j2x,j3x;
let hideshTimeSCREEN = document.getElementById('shTimeSCREENobj');
//hideshTimeSCREEN.hide();
function loadPage(howManyMin){
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
}else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
  if(xmlhttp.readyState==4 && xmlhttp.status==200){
    document.getElementById("shTimeSCREENobj").innerHTML=xmlhttp.responseText;
  }
}
if (howManyMin==30) { xmlhttp.open("POST","/v/v-shTime/shtime30.html",true); }
//xmlhttp.open("POST","/v/v-shTime/index2.html",true);
xmlhttp.send();
}
//////////////////////////////// refresh ebery 25 min - not close sockets!
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////setTimeout("location.reload()", 300000);
/////////////////////////////////////////////////////////////////////////////////////////

///////////////// PlaySND func //////////////////////////////////////////////////////
function sndNo() {
  document.getElementById('stop_player').play(); 
  $.get("../core/model-db.php", { "is_get": "0" }, function(data) { console.log("response: " + data); });
  send('tblRefresh');
  let timerIdno = setTimeout(function tblRefresh3sno() { location.reload(); timerIdno = setTimeout(tblRefresh3sno, 2345); }, 2345);
}
function snd30sec() {
  document.getElementById('30sec_player').play();
}
function snd10sec() {
  document.getElementById('10sec_player').play();
}
function sndYes() {
  document.getElementById('start_player').play(); 
  $.get("../core/model-db.php", { "is_get": "1" }, function(data) { console.log("response: " + data); });
  send('tblRefresh');
  let timerIdyes = setTimeout(function tblRefresh3syes() { location.reload(); timerIdyes = setTimeout(tblRefresh3syes, 3456); }, 3456);
}

function log( text ) {
  $log = $('#log');
  $log.append(($log.val()?"\n":'')+text);

  if(text.indexOf('tblRefresh') + 1) {
    let timerId = setTimeout(function tblRefresh3s() { location.reload(); timerId = setTimeout(tblRefresh3s, 3456); }, 3456);
  }
  if(text.indexOf('st0p') + 1) {
    timer.stop(); 
  }
  if(text.indexOf('stAr1') + 1) {
    $('.hvr-buzz').addClass('bolders');
    let patternLocation = text.search(/_/);
    let timenow_ = text.substring(patternLocation+1,text.length-1);
    timer.mode(0); timer.reset(timenow_); timer.start(1000);
  }
  if(text.indexOf('start120') + 1) {
    $('.hvr-buzz').addClass('bolders');
    timer.mode(0); timer.reset(120); timer.start(1000);
  }
  if(text.indexOf('sh0wTime30tablo') + 1) {
    let shwTimeDIVscreen = document.getElementById('divf5tablo');
    let hideJUDdiv = document.getElementById('w3r_id_container');
    let shTimeSCREEN = document.getElementById('shTimeSCREENobj');
    //let shTBTN = document.getElementById('sh0wTimeBTN');
    //document.getElementById("sh0wTimeBTN").checked = true;
    console.log('START_showTime!!!');
    if (divf5tablo.style.display !== "none") {
      divf5tablo.style.display = "none";
      hideJUDdiv.style.display = "none";

      //document.body.style.background = "#f3f3f3 url('img_tree.png') no-repeat right top";
      document.body.style.background = "#f3f3f3 url('../img/bg/f5mBg.png') repeat";
      let timerIdre01 = setTimeout(function reNewBG() { document.body.style.background = "#f3f3f3 url('../img/bg/f5blue-bg.png') repeat"; timerId = setTimeout(reNewBG, 1234); }, 1234);
      loadPage(30);
    } else {
    divf5tablo.style.display = "block";
    hideJUDdiv.style.display = "block";
    }    
  }
  if(text.indexOf('sh0wTime_STOP') + 1) {
    let shwTimeDIVscreen = document.getElementById('divf5tablo');
    let hideJUDdiv = document.getElementById('w3r_id_container');
    let shTimeSCREEN = document.getElementById('shTimeSCREENobj');
    //let shTBTN = document.getElementById('sh0wTimeBTN');
    //document.getElementById("sh0wTimeBTN").checked = true;
    console.log('PAUSE_showTime!!!');
    let timerIdre01 = setTimeout(function reNewBG() { location.reload(); timerId = setTimeout(reNewBG, 1234); }, 1234);
    if (divf5tablo.style.display !== "none") {
      divf5tablo.style.display = "none";
      hideJUDdiv.style.display = "none";
      //document.body.style.background = "#f3f3f3 url('../img/bg/f5mBg.png') repeat";
    let timerIdre01 = setTimeout(function reNewBG() { location.reload(); timerId = setTimeout(reNewBG, 1234); }, 1234);
      //loadPage(30);
    } else {
    divf5tablo.style.display = "block";
    hideJUDdiv.style.display = "block";
    }    
  }
if(text.indexOf('setTimer60') + 1) {
  console.log('set TIMER 60 sec');
  timer.reset(60);
}
if(text.indexOf('setTimer120') + 1) {
  console.log('set TIMER 120 sec');
  timer.reset(120);
}
if(text.indexOf('start1minTimer') + 1) {
  console.log('1minSTART');
  setTime = timer.getTime();
  timer.mode(0); timer.reset(setTime); timer.start(1000);
}
if(text.indexOf('start2minTimer') + 1) {
  console.log('2minSTART');
  setTime = timer.getTime();
  timer.mode(0); timer.reset(setTime); timer.start(1000);  
}
if(text.indexOf('pause1minTimer') + 1) {
  console.log('STOP');
  timer.stop();   
}

// j3 @ 8,0,1
// j3
  if(text.indexOf('j3') + 1) {
    document.getElementById("j3d").classList.remove("jxdefa");
    document.getElementById("j3d").classList.add("jxno");
    let idnowc = document.getElementsByClassName("name2").text;
    let node2 = document.getElementById('name2');
    let node1 = document.getElementById('name1');
    htmlContent2 = node2.innerHTML;
    textContent2 = node2.textContent;
    htmlContent1 = node1.innerHTML;
    textContent1 = node1.textContent;
    FnSn = textContent2 + textContent1;
    $.get("../core/model-db.php?getFname",  { id: idnowc  }, onAjaxSuccess ); function onAjaxSuccess(data) { console.log(data); }
  }
  if(text.indexOf('j38') + 1) {
    document.getElementById("j3d").classList.remove("jxno");
    document.getElementById("j3d").classList.remove("jxyes");
    document.getElementById("j3d").classList.add("jxdefa");
//jcmd = text.slice(-4);
//j3x = jcmd.substring(3, -1);
//console.log(j3x);
  }
  if(text.indexOf('j30') + 1) {
        document.getElementById("j3d").classList.remove("jxdefa");
        document.getElementById("j3d").classList.remove("jxyes");
        document.getElementById("j3d").classList.add("jxno");
          jcmd = text.slice(-4);
          j3x = jcmd.substring(3, -1);
          //
if((j1x=='j11') && (j2x=='j21') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j30')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); }
if((j1x=='j10') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j31')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }

            console.log(j3x);
            console.log(j2x);
            console.log(j1x);
                      }             
    if(text.indexOf('j31') + 1) {
        document.getElementById("j3d").classList.remove("jxdefa");
        document.getElementById("j3d").classList.remove("jxno");
        document.getElementById("j3d").classList.add("jxyes");
          jcmd = text.slice(-4);
          j3x = jcmd.substring(3, -1);
            console.log(j3x);
            console.log(j2x);
            console.log(j1x); 
            ///
if((j1x=='j11') && (j2x=='j21') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j30')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); }
if((j1x=='j10') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j31')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }


///  
            //console.log(jcmd.substring(3, -1)); 
            //console.log(text.indexOf('j31')+1); 


        

                      }                             
// j2 @ 8,0,1
// j2       
    if(text.indexOf('j28') + 1) {
              document.getElementById("j2d").classList.remove("jxno");
              document.getElementById("j2d").classList.remove("jxyes");
              document.getElementById("j2d").classList.add("jxdefa");
                      }
    if(text.indexOf('j20') + 1) {
        document.getElementById("j2d").classList.remove("jxdefa");
        document.getElementById("j2d").classList.remove("jxyes");
        document.getElementById("j2d").classList.add("jxno");
          jcmd = text.slice(-4);
          j2x = jcmd.substring(3, -1);
          if((j1x=='j11') && (j2x=='j21') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j30')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); }
if((j1x=='j10') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j31')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }

            console.log(j2x);
            console.log(j3x);
            console.log(j1x);
                      }             
    if(text.indexOf('j21') + 1) {
        document.getElementById("j2d").classList.remove("jxdefa");
        document.getElementById("j2d").classList.remove("jxno");
        document.getElementById("j2d").classList.add("jxyes");
          jcmd = text.slice(-4);
          j2x = jcmd.substring(3, -1);
///
if((j1x=='j11') && (j2x=='j21') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); }
if((j1x=='j10') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j31')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
///            
            console.log(j2x);
            console.log(j3x);
            console.log(j1x);         
                      } 

// j1 @ 8,0,1
// j1       
    if(text.indexOf('j18') + 1) {
              document.getElementById("j1d").classList.remove("jxno");
              document.getElementById("j1d").classList.remove("jxyes");
              document.getElementById("j1d").classList.add("jxdefa");
                      }
    if(text.indexOf('j10') + 1) {
        document.getElementById("j1d").classList.remove("jxdefa");
        document.getElementById("j1d").classList.remove("jxyes");
        document.getElementById("j1d").classList.add("jxno");
          jcmd = text.slice(-4);
          j1x = jcmd.substring(3, -1);
if((j1x=='j11') && (j2x=='j21') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j30')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); }
if((j1x=='j10') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j31')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }

            console.log(j1x);
            console.log(j2x);
            console.log(j3x);
                      }             
    if(text.indexOf('j11') + 1) {
        document.getElementById("j1d").classList.remove("jxdefa");
        document.getElementById("j1d").classList.remove("jxno");
        document.getElementById("j1d").classList.add("jxyes");
          jcmd = text.slice(-4);
          j1x = jcmd.substring(3, -1);

console.log('Y' + '1=' + j1x + j2x + j3x);
if((j1x=='j11') && (j2x=='j21') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }
if((j1x=='j11') && (j2x=='j20') && (j3x=='j30')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); }
if((j1x=='j10') && (j2x=='j20') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j20') && (j3x=='j31')) { $('#j1d').addClass("blinkNow"); $('#j2d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j30')) { $('#j1d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" ); sndNo(); } 
if((j1x=='j10') && (j2x=='j21') && (j3x=='j31')) { $('#j2d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#j3d').addClass("blinkNow"); $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" ); sndYes(); }


console.log(j1x);
console.log(j2x);
console.log(j3x);
}
}

function send( text ) {
  Server.send( 'message', text );
}
$(document).ready(function() {
  log('Connecting...');
  <?php 
      //$jsonStr        = file_get_contents("core/f5.config.json");
      //$jsonDEC        = json_decode($jsonStr);
      //$str_ws         = $jsonDEC->{'host'};
      //$str_ws         = 'ws://'.$str_ws.':'.$jsonDEC->{'port'};
      // ++ 16 may 2018@14:22:33
$get_ipv4 = R::getCell('select value from f5opt where name="f5core_srv_ipv4addr";');
$get_port = R::getCell('select value from f5opt where name="f5core_srv_port";');
$coreSocketStr = 'ws://'.$get_ipv4.':'.$get_port;

      //$coreSocketStr='ws://192.168.0.254:9122';
      ?>
      Server = new FancyWebSocket('<?php echo $coreSocketStr; ?>');

      //Server = new FancyWebSocket('ws://10.5.5.5:9122');

      $('#message').keypress(function(e) {
        if ( e.keyCode == 13 && this.value ) {
          log( 'You: ' + this.value );
          send( this.value );

          $(this).val('');
        }
      });

      //Let the user know we're connected
      Server.bind('open', function() {
        log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        log( "Disconnected." );
      });

      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        log( payload );
      });

      Server.connect();
    });
  </script>




  <div id='bodyws' style="display: none">
    <textarea style="display: none" id='log' name='log' readonly='readonly'></textarea><br/>
    <input type='text' id='message' name='message' autofocus/>
    <input type='text' id='j1' name='message' />
    <input type='text' id='j2' name='message' />
    <input type='text' id='j3' name='message' /> ***
    <input type='text' id='final' name='message' />
  </div>

<div id="joobka" style="display: none" >
        <div class="control">
            <button onClick="timer.start(1000)">Start</button> 
            <button onClick="timer.stop()">Stop</button> 
            <button onClick="timer.reset(60)">Reset</button> 
            <button onClick="timer.mode(1)">Count up</button> 
            <button onClick="timer.mode(0)">Count down</button>
        </div>
</div>
  
<div id="therul" style="display: none">
  <input id="j11" type="button" value="j1yes" onclick="judasVotes(this.id)">
  <input id="j18" type="button" value="j1def" onclick="judasVotes(this.id)">
  <input id="j10" type="button" value="j1not" onclick="judasVotes(this.id)"> |
  <input id="j21" type="button" value="j2yes" onclick="judasVotes(this.id)">
  <input id="j28" type="button" value="j2def" onclick="judasVotes(this.id)">
  <input id="j20" type="button" value="j2not" onclick="judasVotes(this.id)"> |
  <input id="j31" type="button" value="j3yes" onclick="judasVotes(this.id)">
  <input id="j38" type="button" value="j3def" onclick="judasVotes(this.id)">
  <input id="j30" type="button" value="j3not" onclick="judasVotes(this.id)">
  <p id="addModalInformTxt"></p><p id="delModalInformTxt"></p><p id="chModalInformTxt"></p>
  <p id="preloaderimg"></p><p id="preloaderdel"></p><p id="preloaderch"></p>
  <p id="outspace"></p><p id="image_uploads"></p>
  


</div>



<div id="ifnobox" style="display: none"></div>
<div id="ifyesbox" style="display: none"></div>
<div id="if30secbox" style="display: none"></div>
<div id="if10secbox" style="display: none"></div>


<div id='divf5tablo' class="zi5">
  <div class="w3-container">
<!-- The container is a centered 960px -->
<div class="container">
  <span class="attNumber"><?php $list = new f5tablo; echo '#'. $list->getIdAtNow0p(); ?></span>
  <?php echo $list->showTblAva(); ?>
  <div id="name2" class="twelve columns name2">
    <p><?php 
    $list = new f5tablo; 
    $list->showTblSecondname('y');
    ?></p>
  </div>
  <div class="row">
  <div class="five columns rank">&nbsp;</div>
    <div id="trynumb" class="five columns trynumber"> . . .
      <script>
        $.get("../core/model-db.php?getTryNum", ongeAjaxSuccess ); 
        function ongeAjaxSuccess(data1) { let msg = document.getElementById("trynumb").innerHTML = data1 + ' <small> att</small>'; }
      </script>
    </div>
    <div class="one columns timer">
      <div id="joobka">
      <!-- <div class="timerbox"> -->
        <div class="timerbox">
        
        <span class="minute">00</span>:<span class="second">60</span>
      </div>
      </div>
    </div>
  </div>
<!-- Sweet nested columns cleared with a clearfix class -->
  <div class="twelve columns clearfix">
<!-- In nested columns give the first column a class of alpha and the second a class of omega -->
  <div class="one columns alpha"></div>
<!-- !MAX LENTH = 15 symbols-->
  <div id ="name1" class="eleven columns omega name1"> <p> <?php $list = new f5tablo; $list->showTblFirstname('y'); ?> </p></div>
  </div>
<!-- Sweet nested columns cleared by wrapping a .row -->
  <div class="five columns">
    <div class="row">
      <div class="three columns alpha"></div>
      <div class="one omega weightkg"><?php
        $list = new f5tablo; 
        $weight_on_barbell = $list->showTblWeightNow('y');
        //$sex_on_pomost = $list->showSexAtNow();
        echo "<small><sub>kg</sub></small>";
      ?>
      </div>
    </div>
  </div>
  <div class="pikcha"><?php 
    $list = new f5tablo; 
    $weight_on_barbell = $list->getTblWeightNow('y');
    $sex_on_pomost = $list->getSexAtNow();
  ?>
  </div>
  <div class="barbel_model"><?php
    $b_model = new theBarbell();
    $b_model->getInform($weight_on_barbell,$sex_on_pomost);
    //$b_model->getInform(69,0);
  ?>
  </div>
<!-- Can push over by columns -->
  <div class="five columns offset-by-one"></div>
</div>
  </div>
</div>

<div id="w3r_id_container" class="w3r_container">
  <div id="j1d" class="w3-third jxdefa">
    <h2>#1</h2>

  </div>

  <div id="j2d" class="w3-third jxdefa">
    <h2>#2</h2>

  </div>

  <div id="j3d" class="w3-third jxdefa">
    <h2>#3</h2>
    
  </div>
</div>


<audio id="30sec_player" src="../snd/30sec.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="10sec_player" src="../snd/10sec.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="stop_player" src="../snd/no.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="start_player" src="../snd/yes.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndStart1" src="../snd/btnclick.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndStart2" src="../snd/btnclick.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndStop" src="../snd/btnclick.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndAdd" src="../snd/jchik.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndDel" src="../snd/jchik.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndSkip" src="../snd/30sec.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>

<div id="shTimeSCREENobj"></div>
<!-- <object id="shTimeSCREENobj" data="http://www.web-source.net" width="100%" height="100%">
    <embed src="http://www.web-source.net" width="600" height="400"> </embed>
    Error: Embedded data could not be displayed.
</object> -->


</body>
</html>