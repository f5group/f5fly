

<!DOCTYPE html>
<html >

<head>
  <meta charset="UTF-8">
  <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
    <title>f5.func - АвтоСарики -  gogo музыкальная минутка</title>
  
  
      <script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
  
      <style>
      html,
body {
  height: 60%;
  margin: 0;
  padding: 0;
  width: 60%;
}

.modal {
  background: dodgerblue;
  height: 1px;
  overflow: hidden;
  position: fixed;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  -webkit-transition: width 0.5s ease 0.5s,  height 0.5s ease;
  transition: width 0.5s ease 0.5s,  height 0.5s ease;
  width: 0;
}

.content {
  color: transparent;
  font-family: 'Consolas', arial, sans-serif;
  font-size: 2em;
  position: absolute;
  top: 50%;
  text-align: center;
  -webkit-transform: translate3d(0, -50%, 0);
          transform: translate3d(0, -50%, 0);
  -webkit-transition: color 0.5s ease;
  transition: color 0.5s ease;
  width: 100%;
}

label {
  color: dodgerblue;
  cursor: pointer;
  font-family: 'Consolas', arial, sans-serif;
  font-size: 2em;
  position: fixed;
  left: 50%;
  top: 50%;
  text-transform: uppercase;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  -webkit-transition: color 0.5s ease 0.5s;
  transition: color 0.5s ease 0.5s;
}

input {
  cursor: pointer;
  height: 0;
  opacity: 0;
  width: 0;
}
input:focus {
  outline: none;
}

input:checked {
  height: 40px;
  opacity: 1;
  position: fixed;
  right: 20px;
  top: 20px;
  z-index: 1;
  -webkit-appearance: none;
  width: 40px;
}
input:checked::after, input:checked:before {
  border-top: 1px solid #FFF;
  content: '';
  display: block;
  position: absolute;
  top: 50%;
  -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
  width: 100%;
}
input:checked::after {
  -webkit-transform: rotate(-45deg);
          transform: rotate(-45deg);
}

input:checked + label {
  color: #FFF;
  -webkit-transition: color 0.5s ease;
  transition: color 0.5s ease;
}

input:checked ~ .modal {
  height: 100%;
  width: 100%;
  -webkit-transition: width 0.5s ease,  height 0.5s ease 0.5s;
  transition: width 0.5s ease,  height 0.5s ease 0.5s;
}
input:checked ~ .modal .content {
  color: #FFF;
  -webkit-transition: color 0.5s ease 0.5s;
  transition: color 0.5s ease 0.5s;
}

body {
  margin: 0;
  background: #000; 
}
video { 
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    transform: translateX(-50%) translateY(-50%);
 background: url('../img/ava0000000.png') no-repeat;
  background-size: cover;
  transition: 1s opacity;
}
.stopfade { 
   opacity: .5;
}

#polina { 
  font-family: Agenda-Light, Agenda Light, Agenda, Arial Narrow, sans-serif;
  font-weight:100; 
  background: rgba(0,0,0,0.3);
  color: white;
  padding: 2rem;
  width: 33%;
  margin:2rem;
  float: right;
  font-size: 1.2rem;
}
h1 {
  font-size: 3rem;
  text-transform: uppercase;
  margin-top: 0;
  letter-spacing: .3rem;
}
#polina button { 
  display: block;
  width: 80%;
  padding: .4rem;
  border: none; 
  margin: 1rem auto; 
  font-size: 1.3rem;
  background: rgba(255,255,255,0.23);
  color: #fff;
  border-radius: 3px; 
  cursor: pointer;
  transition: .3s background;
}
#polina button:hover { 
   background: rgba(0,0,0,0.5);
}

a {
  display: inline-block;
  color: #fff;
  text-decoration: none;
  background:rgba(0,0,0,0.5);
  padding: .5rem;
  transition: .6s background; 
}
a:hover{
  background:rgba(0,0,0,0.9);
}
@media screen and (max-width: 500px) { 
  div{width:70%;} 
}
@media screen and (max-device-width: 800px) {
  html { background: url(../img/ava0000000.png) #000 no-repeat center center fixed; }
  #bgvid { display: none; }
}

    </style>


<script type="text/javascript">
          $(document).ready(function(){
            // refresh every
            var pauseButton = document.querySelector("#polina button");
            pauseButton.click;
  timerId = setInterval(refresh_thath, 13000);
  function refresh_thath() {
  //$.get("core/_calc_sum_weight.php", { });
          location.reload();

  //$('#ttaabbllee').bootstrapTable('refresh', { url: 'core/2rul_tbl.php' });
                                  }
});

</script>

<script type="text/javascript">
  var vid = document.getElementById("bgvid");
var pauseButton = document.querySelector("#polina button");

if (window.matchMedia('(prefers-reduced-motion)').matches) {
//    vid.removeAttribute("autoplay");
    vid.pause();
    pauseButton.innerHTML = "Paused";
}

function vidFade() {
  vid.classList.add("stopfade");
}

vid.addEventListener('ended', function()
{
// only functional if "loop" is removed 
vid.pause();
// to capture IE10
vidFade();
}); 


pauseButton.addEventListener("click", function() {
  vid.classList.toggle("stopfade");
  if (vid.paused) {
    vid.play();
    pauseButton.innerHTML = "Pause";
  } else {
    vid.pause();
    pauseButton.innerHTML = "Paused";
  }
});

</script>
  
  
  
</head>

<body translate="no" >

  <input id='button' type='checkbox'>
<label for='button'>f5 gogo time!</label>
<div class='modal'>


  <video poster="../img/ava0000000.png" id="bgvid" playsinline autoplay muted loop>
  <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
<!-- <source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm"> -->
<source src="../snd/vid0.mp4" type="video/mp4">
</video>
<div id="polina">
<h1>f5.func</h1>
<p>wwww.свитязь.рф</p>
<p><a href="http://thenewcode.com/777/Create-Fullscreen-HTML5-Page-Background-Video">joobaka rec</a>
<p>Мы сделем из Вас настоящего мужика, даже если вы баба!)</p>
</div>


</div>
  
  
  
  
  

</body>
</html>