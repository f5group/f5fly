<script>
function setGUIlng(valueToSelect) {
  let lng3code = 'rus';
  if(valueToSelect == 1) { lng3code = 'rus'; $.get("../core/model-f5start.php?setGUIlng", { "l3c": lng3code },
    function(data) {  $("#thenow_w").text(data); console.log("response5: " + data); });
    let timerIdrus = setTimeout(function indRefresh1s() { location.reload(); timerIdrus = setTimeout(indRefresh1s, 1234); }, 1234);
  }
  if(valueToSelect == 2) { lng3code = 'eng'; $.get("../core/model-f5start.php?setGUIlng", { "l3c": lng3code },
    function(data) {  $("#thenow_w").text(data); console.log("response5: " + data); });
    let timerIdeng = setTimeout(function indRefresh1s() { location.reload(); timerIdeng = setTimeout(indRefresh1s, 1234); }, 1234);
  }
  console.log('YAP! lng3code = ' + valueToSelect);
}
</script>
<!-- f5-core maibody div wrapper container --> 
<div id="mainbody">
<!-- f5-core main wrapper container --> 
<div class="container">
<br>

<div class="wjumbotron">
  <h3>
  <div class="wrap2logo">
    <img class="logoimgmain" src="<?php echo $getBASEDIR;?>/img/f5-logo-blue-el.png"><span class="text-successf5"><?php echo $getGUIf5bName;?> </span>
  </div>
  </h3>

<!-- show flag and  ava propperty q div block --> 
<div class="ttheright">
  <div class="row">
    <div class="col-xs-12">
    </div>
  </div>
</div>


<p class="ttheright"> <?=$getGUIpage1HEADER;?> <?=$getGUIpage1HEADERico;?>&nbsp;</p>
<div>
  <div class="panel with-nav-tabs panel-default">
    <div class="panel-heading text-left"> <? $getGUIpage1panelHEADING;?>
      <label> <span class="label-stacked"></span> </label>
    </div>
    <div class="panel-heading">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab5def" data-toggle="tab"><?=$getGUIpage1tab01;?> </a></li>
      <li><a href="#tab1def" data-toggle="tab"><?=$getGUIpage1tab02;?></a></li>
      <li><a href="#tab4def" data-toggle="tab"><?=$getGUIpage1tab05;?></a></li>
      <li><a href="#tab2def" data-toggle="tab"><?=$getGUIpage1tab03;?></a></li>
      <li><a href="#tab3def" data-toggle="tab"><?=$getGUIpage1tab04;?></a></li>
      
    </ul>
    </div>

<div class="panel-body">
<div class="tab-content">

<div class="tab-pane fade in active" id="tab5def">                         
<div class="panel-heading text-left">
<?=$getGUItab1label;?>
<span class="plain-select">
<select id="guilanguage" class="inp"  name="language" onchange="setGUIlng(this.value);">
<option value="0" selected disabled hidden><?=$getGUIOPT01tab01select;?></option>
<option value="1"><?=$getGUIOPT01tab01select1val;?></option>
<option value="2"><?=$getGUIOPT01tab01select2val;?></option>
</select>
</span>
<?php $getGUIlng = $f5start->getf5GUIlang(3); ?>
<div class="lngbox" title="f5 GUI Language"> <span id="guilang"><?=$getGUIlng?></span></div>
</div>
<br>
<?php $getIPv4 = $f5start->getCore_ipv4(3); ?>
<fieldset class="form-group inline-label">
<input type="text" class="form-control" id="f5ipv4serev" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" placeholder="<?=$getIPv4?>" onkeyup="validateIPadr(this.value);" onchange="setSERVERipAddress(this.value);">
<label for="f5ipv4serev"><?=$getGUIOPT01tab01label;?></label>
</fieldset>

<div id="divIPcaption" class="alert alert-success"><?=$getGUIOPT01tab02label;?><strong> <?=$getIPv4?> </strong> </div>

<div class="col-md-4 text-center"><hr>
<button id="saveBTNgeneral" type="button" onclick="console.log('save01 is OK!');" data-toggle="modal" href="#ignismyModal" class="btn btn-primary btn-sm dashbrdBtnxx">
 <?=$getGU1saveBUTTON;?> <?=$getGU1saveICOBUTTON;?> </button><br><br>
</div><br><br>  
</div>




<div class="tab-pane fade" id="tab1def">
<div class="panel-heading text-left">
<?=$getGUItab2label;?>
<span id="selEvent0" class="plain-select">
  <label for="selEvent0"><?=$getGUIOPT02tab01label;?></label>
<?php echo $getThisVal = @$f5start->listAllEventsNames_sel_opt_html(15); ?>
</span> <br>
</div>&nbsp;&nbsp;&nbsp;
<div class="input-group">
<span class="input-group-addon" id="basic-addon3"><?=$getGUIOPT02tab02label;?></span>
<input id="inputDateRandeid" type="text" size="67" class="datepicker-here form-control" onkeyup="selectEventsDatesRange(this.value);" onblur="selectEventsDatesRange(this.value);" onclick="selectEventsDatesRange(this.value);" ontoggle="selectEventsDatesRange(this.value);" >
</div><br>
<div class="input-group">
<span class="input-group-addon" id="basic-addon3"><?=$getGUIOPT02tab03label;?></span>
<div class="autocomplete">
  <input id="inputEventPlaceid" type="text" size="67" placeholder="where" onkeyup="selectEventsPlace(this.value);" onblur="selectEventsPlace(this.value);" onclick="selectEventsPlace(this.value);" ontoggle="selectEventsPlace(this.value);"> 
</div>
</div><br>
<div class="text-center">
<div class="eventnamebox" title="f5 EVENT Name"> <span id="f5event_name"><? 
@$getThisVal = @$f5start->getEVENTOptValue(15); ?></span></div><br>
<div class="eventnamebox" title="f5 EVENT Name"> <span id="f5event_dates">.</span></div><br>
<div class="eventnamebox" title="f5 EVENT Name"> <span id="f5event_place">..</span></div>
</div>
<div class="col-md-4 text-center"><hr>
<button id="saveBTNevent" type="button" onclick="testFunc();" data-toggle="modal" href="#ignismyModal" class="btn btn-primary btn-sm dashbrdBtnxx">
 <?=$getGU1saveBUTTON;?> <?=$getGU1saveICOBUTTON;?> </button><br><br>  
</div>
</div>




<div class="tab-pane fade" id="tab2def">                       
  <div class="panel-heading text-left">
    <?=$getGUItab3label;?>
      <div class="pretty p-icon p-toggle p-plain">
      <input id="jcfgATTAttID" onchange="setTcfg_sATTAtt();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-numeric-3-box-multiple-outline"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT04tab01optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT04tab01optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>    
      <div class="pretty p-icon p-toggle p-plain">
      <input id="tcfgNUMAttID" onchange="setTcfg_sNUMAtt();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-numeric"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT04tab02optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT04tab02optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>  
      <div class="pretty p-icon p-toggle p-plain">
      <input id="tcfgAVAFLAGAttID" onchange="setTcfg_sAvaFlagAtt();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-account-circle"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT04tab03optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT04tab03optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div class="pretty p-icon p-toggle p-plain">
      <input id="jcfgAVAorFLAGAttID" onchange="setTcfg_Ava_or_Flag();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-account-box-outline"></i>
        <label class="text-success">&nbsp;&nbsp;<?=$getGUIOPT04tab04optStrON;?>&nbsp; <span class="label label-success">ФОТО</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-flag-outline"></i>
        <label class="text-primary">&nbsp;&nbsp;<?=$getGUIOPT04tab04optStrOFF;?>&nbsp; <span class="label label-primary">ФЛАГ</span></label>
      </div>
      </div><br><br> 
      <div class="pretty p-icon p-toggle p-plain">
      <input id="jcfgBModelID" onchange="setTcfg_sBModel();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-dumbbell"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT04tab05optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT04tab05optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>  
</div>
<div class="col-md-4 text-center"><hr>
<button id="saveBTNtablo" type="button" onclick="console.log('save01 is OK!');" data-toggle="modal" href="#ignismyModal" class="btn btn-primary btn-sm dashbrdBtnxx">
 <?=$getGU1saveBUTTON;?> <?=$getGU1saveICOBUTTON;?> </button><br><br>  
</div>
</div>



<div class="tab-pane fade" id="tab3def">
  <div class="panel-heading text-left">
    <?=$getGUItab4label;?>
    <!-- groupbox panel-->
    <div id='topPanel01' class = "panel panel-default"><div class ="panel-heading"><?=$getGUIOPT05tab01panHeading;?></div>
    <div class = "panel-body">
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgTOPPANELID" onchange="setDcfg_sTOPanel();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-format-vertical-align-top"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab01optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab01optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div id="dcfgEVNAMEIDDIV" class="pretty p-icon p-toggle p-plain">
      <input id="dcfgEVNAMEID" onchange="setDcfg_sEVName();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-information-outline"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab02optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab02optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div id="dcfgEVPACEIDDIV" class="pretty p-icon p-toggle p-plain">
      <input id="dcfgEVPACEID" onchange="setDcfg_sEVPlace();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-home-circle"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab03optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab03optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div id="dcfgSEXWCATFLOWIDDIV" class="pretty p-icon p-toggle p-plain">
      <input id="dcfgSEXWCATFLOWID" onchange="setDcfg_sSEXWCFlow();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-human-male-female"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab04optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab04optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>            
      <div id="dcfgLOGOIDDIV" class="pretty p-icon p-toggle p-plain">
      <input id="dcfgLOGOID" onchange="setDcfg_sLOGO();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-image-area"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab05optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab05optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>      
    </div>
</div>   
 <div class = "panel panel-default">
  <div class ="panel-heading"><?=$getGUIOPT05tab02panHeading;?></div>
   <ul class = "list-group">
      <li class = "list-group-item">
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgNUMAttID" onchange="setDcfg_sNUMAtt();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-numeric"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab06optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab06optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgAVAFLAGAttID" onchange="setDcfg_sAvaFlagAtt();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-account-circle"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab07optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab07optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div id='topPanel02' class="pretty p-icon p-toggle p-plain">
      <input id="dcfgAVAorFLAGAttID" onchange="setDcfg_Ava_or_Flag();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-account-box-outline"></i>
        <label class="text-success">&nbsp;&nbsp;<?=$getGUIOPT05tab08optStrON;?>&nbsp; <span class="label label-success">ФОТО</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-flag-outline"></i>
        <label class="text-primaty">&nbsp;&nbsp;<?=$getGUIOPT05tab08optStrOFF;?>&nbsp; <span class="label label-primary">ФЛАГ</span></label>
      </div>
      </div><br><br>      
      </li>
      <li class = "list-group-item">
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgRATINGSID" onchange="setDcfg_sRatings();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-trophy"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab09optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab09optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgRATINGS3rankID" onchange="setDcfg_sRatings3rank();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-trophy-variant-outline"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab13optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab13optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br>      
      </li>
      <li class = "list-group-item">
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgOPID" onchange="setDcfg_sOPatt_highlight();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-arrow-right-drop-circle-outline"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab10optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab10optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgNOPID" onchange="setDcfg_sNOPatt_highlight();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-skip-forward"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab11optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab11optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br>
      </li> <!-- Z5woJy7JV2 admin_f5flyru_db admin_f5fly_usr -->
      <li class = "list-group-item">
      <div class="pretty p-icon p-toggle p-plain">
      <input id="dcfgMOTDID" onchange="setDcfg_sMOTD();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-library-books"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT05tab12optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT05tab12optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>        
      </li>   
   </ul>
  </div>



<div class="panel panel-success">
  <div class="panel-heading">Изменить задний фон (картинку) Табло Рейтингов - Дубль</div>
  <div class="panel-body">
  <div class="fupload_add_div" title="Нажмите на меня, для обзора файла">
<input size="1" tabindex="8" type="file" id="image_uploads" name="image_uploadsname" accept=".jpg, .jpeg, .png" />
</div>
  <div class="preview">
    <img src="<?php echo '/img/flags/'.mt_rand(1, 4).'.png'; ?>"> 
    <div id="theinfo">
  </div>
  </div>
  </div>
  <div class="panel-footer"><a href="#">сбросить на картинку по умолчанию<i class="icon mdi mdi-24px mdi-reload"></i></a></div>
</div>


</div>
<div class="col-md-4 text-center"><hr>
<button id="saveBTNdbl" type="button" onclick="console.log('save01 is OK!');" data-toggle="modal" href="#ignismyModal" class="btn btn-primary btn-sm dashbrdBtnxx">
 <?=$getGU1saveBUTTON;?> <?=$getGU1saveICOBUTTON;?> </button><br><br>   
</div>
</div>




<div class="tab-pane fade" id="tab4def">                 
  <div class="panel-heading text-left">
    <?=$getGUItab5label;?>
  <div class="pretty p-icon p-toggle p-plain">
    <input id="jcfgShowTopMenuID" onchange="setJcfg_sTOPMENU();" type="checkbox" />
    <div class="state p-on"><i class="icon mdi mdi-24px mdi-border-top"></i>
      <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT03tab05optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
    </div>
    <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
      <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT03tab05optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
    </div>
  </div><br><br>    
  <div class="pretty p-icon p-toggle p-plain">
    <input id="jcfgShowATTAttID" onchange="setJcfg_sATTAtt();" type="checkbox" />
    <div class="state p-on"><i class="icon mdi mdi-24px mdi-numeric-3-box-multiple-outline"></i>
      <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT03tab01optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
    </div>
    <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
      <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT03tab01optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
    </div>
  </div><br><br>
  <div class="pretty p-icon p-toggle p-plain">
  <input id="jcfgShowNUMAttID" onchange="setJcfg_sNUMAtt();" type="checkbox" />
  <div class="state p-on"><i class="icon mdi mdi-24px mdi-numeric"></i>
    <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT03tab02optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
  </div>
  <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
    <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT03tab02optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
  </div>
  </div><br><br>  
  <div class="pretty p-icon p-toggle p-plain">
    <input id="jcfgShowNameAttID" onchange="setJcfg_sNameAtt();" type="checkbox" />
  <div class="state p-on"><i class="icon mdi mdi-24px mdi-account"></i>
    <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT03tab03optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
  </div>
  <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
    <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT03tab03optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
  </div>
  </div><br><br>    
  <div class="pretty p-icon p-toggle p-plain">
  <input id="jcfgShowWeightID" onchange="setJcfg_sWeight();" type="checkbox" />
  <div class="state p-on"><i class="icon mdi mdi-24px mdi-weight-kilogram"></i>
    <label class="text-info">&nbsp;&nbsp;<?=$getGUIOPT03tab04optStrON;?>&nbsp; <span class="label label-success">ON</span></label>
  </div>
  <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
    <label class="text-muted">&nbsp;&nbsp;<?=$getGUIOPT03tab04optStrOFF;?>&nbsp; <span class="label label-default">OFF</span></label>
  </div>
  </div><br><br>
  </div>
<div class="col-md-4 text-center"><hr>
<button id="saveBTNjud" type="button" onclick="console.log('save01 is OK!');" data-toggle="modal" href="#ignismyModal" class="btn btn-primary btn-sm dashbrdBtnxx">
 <?=$getGU1saveBUTTON;?> <?=$getGU1saveICOBUTTON;?> </button><br><br>
</div>
</div>


</div>
<br>

</div>
</div>
</div>
</div>
</div>



<!--
<div class="panel panel-default">
<div class="panel-heading text-left">
<h4><sup>#информация &nbsp;</sup></h4>
<div>
<div class='alsof5url'>
  <a class='s5f alsof5urla' href='#' target='_blank'> <span class='glyphicon glyphicon-globe'>Условия использования</span> </a>
  <a class='s5f alsof5urla' href='#' target='_blank'> <span class='glyphicon glyphicon-globe'>Пользовательское соглашение</span> </a>
</div>
<div class='alsof5url'>
  <a class='s5f alsof5urla' href='#' target='_blank'> <span class='glyphicon glyphicon-globe'>Помощь / поддержка</span> </a><br>
  <a class='s5f alsof5urla' href='#' target='_blank'> <span class='glyphicon glyphicon-globe'>Политика конфиденциальности</span> </a> 
</div>
<div class='alsof5url'>
  <a class='s5f alsof5urla' href='#' target='_blank'> <span class='glyphicon glyphicon-globe'>Часто задаваемые вопросы</span> </a>
</div>
<div class='alsof5url'>
  <a class='s5f alsof5urla' href='#' target='_blank'> <span class='glyphicon glyphicon-globe'>Правила - Тяжелая Атлетика (2017)</span> </a>
</div>

</div>
<div class="links">
  <a class="openpop" href="http://f5/md">dbl</a> <a class="openpop" href="http://f5/mt">tablo</a>
</div>

<br><br>
<div id="totalAthleteBtm" class="mainurl hvr-wobble-skew">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-globe"></span> <a href="f5s.su" class="s1">f5s.su</a> <span class="glyphicon glyphicon-info-sign"></span> <a href="#" class="s5f">f5sport.ru</a>&nbsp;&nbsp;&nbsp;</div>
</div>
<div class="mainurl">
<a href="#" class="s9">пульт</a> &#10044; <a href="#" class="s9">дубль</a> &#10044; <a href="#" class="s9">табло</a> &#10044; <a href="#" class="s9">судьи</a> &#10044; <a href="#" class="s9">сигнал</a> 
</div>
</div>
-->

    </div> <!-- /container -->
 
</div><!-- /mainbody -->
</div>


<script>
    $(function(){
        window.setTimeout(function(){
            $('#my-alert').alert('close');
        },3456);
    });
</script>




<!-- <script src="../js/datepicker.min.js"></script> -->
<script>
function setJcfg_check() {
  let getOpt_ = document.getElementById('jcfgShowIdAtt');
  let getOpt_val_ = document.getElementById('jcfgShowIdAtt').value;
  let newVal ='on';
  switch (getOpt_val_) { 
    case 'on': newVal ='off'; break;
    case 'off': newVal ='on'; break;
    default: newVal ='off';
  }  
  console.log(newVal);

  document.body.onload = addElement;
  var my_div = newDiv = null;
  function addElement() {
    let newDiv = document.createElement("div");
    let BFnewDiv = document.getElementById("gfdgfdg");
        newDiv.innerHTML = "hgfhgfhgfhgfhfghgfh";
        newDiv.class = "alert alert-success alert-dismissible";
        newDiv.role = "alert";
    my_div = document.getElementById("my-alert");
    document.body.insertBefore(BFnewDiv, my_div);
  }
}    
function getViewEventCfg() {
  let xcfgCMD1 = 'get/15';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD1 }, function(data15) { console.log("jcfgCMD:" + data15); 
    if (data15) { document.getElementById('f5event_name').innerHTML = data15; } });
  let xcfgCMD2 = 'get/16';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD2 }, function(data16) { console.log("jcfgCMD:" + data16); 
    if (data16) { document.getElementById('f5event_dates').innerHTML = data16; } });
  let xcfgCMD3 = 'get/17';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD3 }, function(data17) { console.log("jcfgCMD:" + data17); 
    if (data17) { document.getElementById('f5event_place').innerHTML = data17; } });      
}

function getViewJudgeCfg() {
  let xcfgCMD1 = 'get/19';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD1 }, function(data19) { console.log("jcfgCMD:" + data19); 
    if (data19==1) { document.getElementById('jcfgShowNUMAttID').click(); } });
  let xcfgCMD2 = 'get/20';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD2 }, function(data20) { console.log("jcfgCMD:" + data20); 
    if (data20==1) { document.getElementById('jcfgShowNameAttID').click(); } });
  let xcfgCMD3 = 'get/21';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD3 }, function(data21) { console.log("jcfgCMD:" + data21); 
    if (data21==1) { document.getElementById('jcfgShowATTAttID').click(); } });
  let xcfgCMD4 = 'get/22';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD4 }, function(data22) { console.log("jcfgCMD:" + data22); 
    if (data22==1) { document.getElementById('jcfgShowWeightID').click(); } });
let xcfgCMD5 = 'get/51';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD5 }, function(data51) { console.log("jcfgCMD:" + data51); 
    if (data51==1) { document.getElementById('jcfgShowTopMenuID').click(); } });  
}
function getViewTabloCfg() {
  let xcfgCMD1 = 'get/44';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD1 }, function(data44) { console.log("TcfgCMD:" + data44); 
    if (data44==1) { document.getElementById('tcfgNUMAttID').click(); } });
  let xcfgCMD2 = 'get/45';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD2 }, function(data45) { console.log("TcfgCMD:" + data45); 
    if (data45==1) { document.getElementById('tcfgAVAFLAGAttID').click(); } });
  let xcfgCMD3 = 'get/46';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD3 }, function(data46) { console.log("TcfgCMD:" + data46); 
    if (data46==1) { document.getElementById('jcfgAVAorFLAGAttID').click(); } });
  let xcfgCMD4 = 'get/47';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD4 }, function(data47) { console.log("TcfgCMD:" + data47); 
    if (data47==1) { document.getElementById('jcfgATTAttID').click(); } });
  let xcfgCMD5 = 'get/48';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD5 }, function(data48) { console.log("TcfgCMD:" + data48); 
    if (data48==1) { document.getElementById('jcfgBModelID').click(); } });  
}
function getViewDoubleCfg() {
  let xcfgCMD1 = 'get/31';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD1 }, function(data31) { console.log("TcfgCMD:" + data31); 
    if (data31==1) { document.getElementById('dcfgTOPPANELID').click();
    document.getElementById('dcfgEVNAMEID').disabled = false; document.getElementById('dcfgEVPACEID').disabled = false;
    document.getElementById('dcfgSEXWCATFLOWID').disabled = false; document.getElementById('dcfgLOGOID').disabled = false; 
    document.getElementById('dcfgEVNAMEIDDIV').style.display = "inline"; document.getElementById('dcfgEVPACEIDDIV').style.display = "inline";
    document.getElementById('dcfgSEXWCATFLOWIDDIV').style.display = "inline"; document.getElementById('dcfgLOGOIDDIV').style.display = "inline";
    document.getElementById('topPanel01').style.height = '100%';
    }
    if (data31==0) { 
  document.getElementById('dcfgEVNAMEID').disabled = true; document.getElementById('dcfgEVPACEID').disabled = true;
  document.getElementById('dcfgSEXWCATFLOWID').disabled = true; document.getElementById('dcfgLOGOID').disabled = true;
  document.getElementById('dcfgEVNAMEIDDIV').style.display = "none"; document.getElementById('dcfgEVPACEIDDIV').style.display = "none";
  document.getElementById('dcfgSEXWCATFLOWIDDIV').style.display = "none"; document.getElementById('dcfgLOGOIDDIV').style.display = "none";
  document.getElementById('topPanel01').style.height = '91px';
    }
    });
  let xcfgCMD2 = 'get/32';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD2 }, function(data32) { console.log("TcfgCMD:" + data32); 
    if (data32==1) { document.getElementById('dcfgEVNAMEID').click(); } });
  let xcfgCMD3 = 'get/33';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD3 }, function(data33) { console.log("TcfgCMD:" + data33); 
    if (data33==1) { document.getElementById('dcfgEVPACEID').click(); } });
  let xcfgCMD4 = 'get/34';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD4 }, function(data34) { console.log("TcfgCMD:" + data34); 
    if (data34==1) { document.getElementById('dcfgSEXWCATFLOWID').click(); } });
  let xcfgCMD5 = 'get/35';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD5 }, function(data35) { console.log("TcfgCMD:" + data35); 
    if (data35==1) { document.getElementById('dcfgLOGOID').click(); } });
  let xcfgCMD6 = 'get/36';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD6 }, function(data36) { console.log("TcfgCMD:" + data36); 
    if (data36==1) { document.getElementById('dcfgNUMAttID').click(); } });  
  let xcfgCMD7 = 'get/37';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD7 }, function(data37) { console.log("TcfgCMD:" + data37); 
    if (data37==1) { document.getElementById('dcfgAVAFLAGAttID').click(); 
      document.getElementById('dcfgAVAorFLAGAttID').disabled = false;
      document.getElementById('topPanel02').style.display = "inline";
      document.getElementById('topPanel02').style.height = '100%';
    }
    if (data37==0) {
      document.getElementById('dcfgAVAorFLAGAttID').disabled = true;
      document.getElementById('topPanel02').style.display = "none";
      document.getElementById('topPanel02').style.height = '123px';
    }     
  });  
  let xcfgCMD8 = 'get/38';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD8 }, function(data38) { console.log("TcfgCMD:" + data38); 
    if (data38==1) { document.getElementById('dcfgAVAorFLAGAttID').click(); } });  
  let xcfgCMD9 = 'get/39';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD9 }, function(data39) { console.log("TcfgCMD:" + data39); 
    if (data39==1) { document.getElementById('dcfgRATINGSID').click(); } });  
  let xcfgCMD10 = 'get/41';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD10 }, function(data41) { console.log("TcfgCMD:" + data41); 
    if (data41==1) { document.getElementById('dcfgOPID').click(); } });
  let xcfgCMD11 = 'get/42';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD11 }, function(data42) { console.log("TcfgCMD:" + data42); 
    if (data42==1) { document.getElementById('dcfgNOPID').click(); } });
let xcfgCMD12 = 'get/43';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD12 }, function(data43) { console.log("TcfgCMD:" + data43); 
    if (data43==1) { document.getElementById('dcfgMOTDID').click(); } });  
}


$('#inputDateRandeid').daterangepicker({
  locale: {
    format: 'DD.MM.YYYY'
  },
    "autoApply": true,
    "opens": "center"
}, function(start, end, label) {
  let dateRanges = start.format('DD.MM.YYYY') +"-"+ end.format('DD.MM.YYYY');
  //console.log('New date range selected: ' + start.format('DD.MM.YYYY') + ' to ' + end.format('DD.MM.YYYY') + ' (predefined range: ' + label + ')');
  console.log(dateRanges);
  $.get("../core/model-f5start.php?setEventDateRange", { "dateRange": dateRanges }, function(data) { console.log("response: " + data); });
});


function validateIPadr(inputText) {
    let str = inputText; 
    let res = str.match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);
    if (!res) { document.getElementById("divIPcaption").innerHTML = "Input Valid ipv4 address!" } else {
      document.getElementById("divIPcaption").innerHTML = 'New IP ADDRESS is:' +res[0];
      console.log(res);
      }
  }
function setSERVERipAddress(valueToEdit) {    
  let guiip_ = document.getElementById('divIPcaption');
  guiip_.innerHTML = valueToEdit;
  guiip_.value = valueToEdit;
  $.get("../core/model-f5start.php?setf5IPADDRESS", { "ipv4addr": guiip_.value }, function(data) { console.log("response: " + data); });
}
function selectf5GUIlanguage(valueToSelect) {    
  let guilang_ = document.getElementById('guilang');
  let lng3code ='rus';
  guilang_.value = valueToSelect;
  if (valueToSelect==1) { lng3code ='rus'; }
  if (valueToSelect==2) { lng3code ='eng'; }
  guilang_.innerHTML = lng3code;
  $.get("../core/model-db.php?setGUILang", { "guilang": guilang_.value }, function(data) { console.log("response: " + data); document.location.reload(); });
}

$(function(){
    $(".popup").hide();
    $(".openpop").click(function (e) {
    e.preventDefault();
    $("iframe").attr("src", $(this).attr('href'));
    $(".links").fadeOut('slow');
    $(".popup").fadeIn('slow');
    });

    $(".close").click(function () {
        $(this).parent().fadeOut("slow");
        $(".links").fadeIn("slow");
    });

});

function selectEventsName(valueToCh){
  let eventname = document.getElementById('f5event_name');
  eventname.innerHTML = valueToCh;
  $.get("../core/model-f5start.php?setEvent", { "eventId": valueToCh }, function(data) { console.log("response: " + data); eventname.innerHTML = data; });
}
function selectEventsDatesRange(valueToSel){
  //let guidaterange_ = document.getElementById('inputDateRandeid');
  //guidaterange_.innerHTML = valueToSel;
  //guidaterange_.value = valueToSel;
  let xcfgCMD = 'get/16';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD }, function(data16) { console.log("jcfgCMD:" + data16); 
  if (data16) { document.getElementById('inputDateRandeid').innerHTML = data16; document.getElementById('inputDateRandeid').value = data16; } });  
}
function selectEventsPlace(valueToSel){
  let guidaterange_ = document.getElementById('inputEventPlaceid');
  $.get("../core/model-f5start.php?setEventPlace", { "placeId": valueToSel },
  function(data) { 
    //console.log("response: " + data); 
    guidaterange_.innerHTML = data;
    guidaterange_.value = data;
  });
  let xcfgCMD3 = 'get/17';
  $.get("../core/model-f5start.php?getF5OPTIONS", { f5cfgCMD:xcfgCMD3 }, function(data17) { console.log("jcfgCMD:" + data17); 
  if (data17) { document.getElementById('f5event_place').innerHTML = data17; } });
}

///////////////////////////////////
//  DOUBLE cfg //
///////////////////////////////////
function setDcfg_sTOPanel() {
  let getOpt_ = document.getElementById('dcfgTOPPANELID');
  let getOpt_val_ = document.getElementById('dcfgTOPPANELID').value;
  if (getOpt_val_ == "on") {
    document.getElementById('dcfgEVNAMEID').disabled = false; document.getElementById('dcfgEVPACEID').disabled = false;
    document.getElementById('dcfgSEXWCATFLOWID').disabled = false; document.getElementById('dcfgLOGOID').disabled = false; 
    document.getElementById('dcfgEVNAMEIDDIV').style.display = "inline"; document.getElementById('dcfgEVPACEIDDIV').style.display = "inline";
    document.getElementById('dcfgSEXWCATFLOWIDDIV').style.display = "inline"; document.getElementById('dcfgLOGOIDDIV').style.display = "inline";
    document.getElementById('topPanel01').style.height = '100%';
    getOpt_.value = "off"; 
  } else { 
  getOpt_.value = "on"; 
  document.getElementById('dcfgEVNAMEID').disabled = true; document.getElementById('dcfgEVPACEID').disabled = true;
  document.getElementById('dcfgSEXWCATFLOWID').disabled = true; document.getElementById('dcfgLOGOID').disabled = true;
  document.getElementById('dcfgEVNAMEIDDIV').style.display = "none"; document.getElementById('dcfgEVPACEIDDIV').style.display = "none";
  document.getElementById('dcfgSEXWCATFLOWIDDIV').style.display = "none"; document.getElementById('dcfgLOGOIDDIV').style.display = "none";
  document.getElementById('topPanel01').style.height = '91px';
  }
  let jcfgCMDx = getOpt_val_+'/31';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sEVName() {
  let getOpt_ = document.getElementById('dcfgEVNAMEID');
  let getOpt_val_ = document.getElementById('dcfgEVNAMEID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/32';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sEVPlace() {
  let getOpt_ = document.getElementById('dcfgEVPACEID');
  let getOpt_val_ = document.getElementById('dcfgEVPACEID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/33';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sSEXWCFlow() {
  let getOpt_ = document.getElementById('dcfgSEXWCATFLOWID');
  let getOpt_val_ = document.getElementById('dcfgSEXWCATFLOWID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/34';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sLOGO() {
  let getOpt_ = document.getElementById('dcfgLOGOID');
  let getOpt_val_ = document.getElementById('dcfgLOGOID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/35';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sNUMAtt() {
  let getOpt_ = document.getElementById('dcfgNUMAttID');
  let getOpt_val_ = document.getElementById('dcfgNUMAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/36';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sAvaFlagAtt() {
  let getOpt_ = document.getElementById('dcfgAVAFLAGAttID');
  let getOpt_val_ = document.getElementById('dcfgAVAFLAGAttID').value;
  if (getOpt_val_ == "on") { 
    document.getElementById('dcfgAVAorFLAGAttID').disabled = false;
    document.getElementById('topPanel02').style.display = "inline";
    document.getElementById('topPanel02').style.height = '100%';
    getOpt_.value = "off"; } else {
      document.getElementById('dcfgAVAorFLAGAttID').disabled = true;
      document.getElementById('topPanel02').style.display = "none";
      document.getElementById('topPanel02').style.height = '123px';
      getOpt_.value = "on";
    }
  let jcfgCMDx = getOpt_val_+'/37';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_Ava_or_Flag() {
  let getOpt_ = document.getElementById('dcfgAVAorFLAGAttID');
  let getOpt_val_ = document.getElementById('dcfgAVAorFLAGAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/38';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sRatings() {
  let getOpt_ = document.getElementById('dcfgRATINGSID');
  let getOpt_val_ = document.getElementById('dcfgRATINGSID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/39';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sRatings3rank() {
  let getOpt_ = document.getElementById('dcfgRATINGS3rankID');
  let getOpt_val_ = document.getElementById('dcfgRATINGS3rankID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/50';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sOPatt_highlight() {
  let getOpt_ = document.getElementById('dcfgOPID');
  let getOpt_val_ = document.getElementById('dcfgOPID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/41';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sNOPatt_highlight() {
  let getOpt_ = document.getElementById('dcfgNOPID');
  let getOpt_val_ = document.getElementById('dcfgNOPID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/42';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setDcfg_sMOTD() {
  let getOpt_ = document.getElementById('dcfgMOTDID');
  let getOpt_val_ = document.getElementById('dcfgMOTDID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/43';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
///////////////////////////////////
//  TABLO cfg //
///////////////////////////////////
function setTcfg_sAvaFlagAtt() {
  let getOpt_ = document.getElementById('tcfgAVAFLAGAttID');
  let getOpt_val_ = document.getElementById('tcfgAVAFLAGAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/45';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setTcfg_Ava_or_Flag() {
  let getOpt_ = document.getElementById('jcfgAVAorFLAGAttID');
  let getOpt_val_ = document.getElementById('jcfgAVAorFLAGAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/46';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}   
function setTcfg_sBModel() {
  let getOpt_ = document.getElementById('jcfgBModelID');
  let getOpt_val_ = document.getElementById('jcfgBModelID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/48';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setTcfg_sNUMAtt() {
  let getOpt_ = document.getElementById('tcfgNUMAttID');
  let getOpt_val_ = document.getElementById('tcfgNUMAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/44';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setTcfg_sATTAtt() {
  let getOpt_ = document.getElementById('jcfgATTAttID');
  let getOpt_val_ = document.getElementById('jcfgATTAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMDx = getOpt_val_+'/47';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMDx },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    });
}
function setJcfg_sTOPMENU() {
  let getOpt_ = document.getElementById('jcfgShowTopMenuID');
  let getOpt_val_ = document.getElementById('jcfgShowTopMenuID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMD4 = getOpt_val_+'/51';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMD4 },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    });
}
function setJcfg_sNUMAtt() {
  let getOpt_ = document.getElementById('jcfgShowNUMAttID');
  let getOpt_val_ = document.getElementById('jcfgShowNUMAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMD1 = getOpt_val_+'/19';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMD1 },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    }); 
}
function setJcfg_sNameAtt() {
  let getOpt_ = document.getElementById('jcfgShowNameAttID');
  let getOpt_val_ = document.getElementById('jcfgShowNameAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMD2 = getOpt_val_+'/20';
  $.get("../core/model-f5start.php?setF5OPTIONS", { "f5cfgCMD": jcfgCMD2 },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    });
}
function setJcfg_sATTAtt() {
  let getOpt_ = document.getElementById('jcfgShowATTAttID');
  let getOpt_val_ = document.getElementById('jcfgShowATTAttID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMD3 = getOpt_val_+'/21';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMD3 },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    });
}
function setJcfg_sWeight() {
  let getOpt_ = document.getElementById('jcfgShowWeightID');
  let getOpt_val_ = document.getElementById('jcfgShowWeightID').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMD4 = getOpt_val_+'/22';
  $.get("../core/model-f5start.php?setF5OPTIONS", { f5cfgCMD:jcfgCMD4 },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    });
}


getViewEventCfg();
getViewJudgeCfg();
getViewTabloCfg();
getViewDoubleCfg();

</script>



<script>
    function testFunc() {
    console.log('SAVEING OK!');
  }
$(document).ready(function() {
function autocomplete(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
       closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
       a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
      });
}

/*
var countries = [];
var datax = countries;
let xcfgCMD = 'ALL';
$.get("../core/model-f5start.php?getJSON_EVPlaces", { f5cfgCMD:xcfgCMD }, 
  function(datax) { console.log("datax:" + datax);
  countries = datax;
  console.log('### ' + countries);
  autocomplete(document.getElementById('inputEventPlaceid'),countries);
});
*/

let countries = ['Абакан','Абдулино','Абинск','Агидель','Агрыз','Адыгейск','Азнакаево','Азов','Ак-Довурак','Аксай','Алагир','Алапаевск','Алатырь','Алдан','Алейск','Александров','Александровск','Александровск-Сахалинский','Алексеевка','Алексин','Алзамай','Альметьевск','Амурск','Анадырь','Анапа','Ангарск','Андреаполь','Анжеро-Судженск','Анива','Апатиты','Апрелевка','Апшеронск','Арамиль','Аргун','Ардатов','Ардон','Арзамас','Арзамас','Аркадак','Армавир','Арсеньев','Архангельск','Асбест','Асино','Астрахань','Аткарск','Ахтубинск','Ачинск','Аша','Бабаево','Бабушкин','Бавлы','Багратионовск','Байкальск','Баймак','Бакал','Баксан','Балабаново','Балаково','Балахна','Балахна','Балашиха','Балашов','Балей','Балтийск','Барабинск','Барнаул','Барыш','Батайск','Бежецк','Белая Калитва','Белая Холуница','Белгород','Белебей','Белинский','Белово','Белогорск','Белозерск','Белокуриха','Беломорск','Белорецк','Белореченск','Белоусово','Белоярский','Белый','Бердск','Березники','Беслан','Бийск','Бикин','Билибино','Биробиджан','Бирск','Бирюсинск','Бирюч','Благовещенск','Благовещенск','Благодарный','Бобров','Богданович','Богородицк','Богородск','Богородск','Боготол','Богучар','Бодайбо','Бокситогорск','Болгар','Бологое','Болотное','Болохово','Болхов','Большой Камень','Бор','Бор','Борзя','Борисоглебск','Боровичи','Боровск','Бородино','Братск','Бронницы','Брянск','Бугульма','Бугуруслан','Бузулук','Буинск','Буй','Буйнакск','Бутурлиновка','Валдай','Валуйки','Велиж','Великие Луки','Великий Новгород','Великий Устюг','Вельск','Верещагино','Верея','Верхнеуральск','Верхний Тагил','Верхний Уфалей','Верхняя Пышма','Верхняя Салда','Верхняя Тура','Верхотурье','Верхоянск','Весьегонск','Ветлуга','Ветлуга','Видное','Вилюйск','Вилючинск','Вихоревка','Вичуга','Владивосток','Владикавказ','Владимир','Волгоград','Волгодонск','Волгореченск','Волжск','Волжский','Вологда','Володарск','Володарск','Волоколамск','Волосово','Волхов','Волчанск','Вольск','Воркута','Воронеж','Ворсма','Ворсма','Воскресенск','Воткинск','Всеволожск','Вуктыл','Выборг','Выкса','Выкса','Высоковск','Высоцк','Вытегра','Вяземский','Вязники','Вязьма','Вятские Поляны','Гаврилов Посад','Гаврилов-Ям','Гагарин','Гаджиево','Гай','Галич','Гатчина','Гвардейск','Гдов','Геленджик','Георгиевск','Глазов','Горбатов','Горбатов','Горно-Алтайск','Горнозаводск','Горнозаводск','Горняк','Городец','Городец','Городище','Городовиковск','Гороховец','Горячий Ключ','Грайворон','Гремячинск','Грозный','Грязи','Грязовец','Губаха','Губкин','Губкинский','Гудермес','Гуково','Гулькевичи','Гурьевск','Гурьевск','Гусев','Гусь-Хрустальный','Давлеканово','Дагестанские Огни','Далматово','Дальнегорск','Дальнереченск','Данилов','Данков','Дегтярск','Дедовск','Демидов','Дербент','Десногорск','Дзержинск','Дзержинск','Дзержинский','Дивногорск','Дигора','Димитровград','Дмитров','Дмитровск','Дно','Добрянка','Долгопрудный','Долинск','Домодедово','Донецк','Донской','Дорогобуж','Дрезна','Дубна','Дубовка','Дудинка','Духовщина','Дюртюли','Дятьково','Егорьевск','Ейск','Екатеринбург','Елабуга','Елец','Елизово','Ельня','Еманжелинск','Емва','Енисейск','Ермолино','Ершов','Ессентуки','Ефремов','Железноводск','Железногорск','Железногорск','Железногорск-Илимский','Железнодорожный','Жердевка','Жиздра','Жирновск','Жуков','Жуковка','Жуковский','Завитинск','Заводоуковск','Заволжск','Заволжье','Заволжье','Задонск','Заинск','Закаменск','Западная Двина','Заполярный','Зарайск','Заречный','Заречный','Заринск','Звенигово','Звенигород','Зверево','Зеленогорск','Зеленоградск','Зеленодольск','Зеленокумск','Зерноград','Зея','Зима','Златоуст','Злынка','Змеиногорск','Знаменск','Зубцов','Зуевка','Ивангород','Иваново','Ивантеевка','Ивдель','Игарка','Ижевск','Избербаш','Изобильный','Иланский','Инза','Инсар','Инта','Ипатово','Ирбит','Иркутск','Исилькуль','Искитим','Истра','Ишим','Ишимбай','Йошкар-Ола','Кадников','Казань','Калач','Калачинск','Калач-на-Дону','Калининград','Калининск','Калтан','Калуга','Калязин','Камбарка','Каменка','Каменногорск','Каменск-Уральский','Каменск-Шахтинский','Камень-на-Оби','Камешково','Камызяк','Камышин','Камышлов','Канаш','Кандалакша','Канск','Карабаново','Карабаш','Карабулак','Карасук','Карачаевск','Карачев','Каргат','Каргополь','Карпинск','Карталы','Касимов','Касли','Каспийск','Катав-Ивановск','Катайск','Качканар','Кашин','Кашира','Кедровый','Кемерово','Кемь','Кизел','Кизилюрт','Кизляр','Кимовск','Кимры','Кингисепп','Кинель','Кинешма','Киреевск','Киренск','Киржач','Кириллов','Кириши','Киров','Киров','Кировград','Кирово-Чепецк','Кировск','Кировск','Кирс','Кирсанов','Кисловодск','Климовск','Клин','Клинцы','Княгинино','Княгинино','Ковдор','Ковров','Ковылкино','Когалым','Кодинск','Козельск','Козловка','Козьмодемьянск','Кола','Кологрив','Коломна','Колпашево','Кольчугино','Коммунар','Комсомольск','Комсомольск-на-Амуре','Конаково','Кондопога','Кондрово','Константиновск','Копейск','Кораблино','Кореновск','Коркино','Короча','Корсаков','Коряжма','Костерево','Костомукша','Кострома','Котельниково','Котельнич','Котлас','Котово','Котовск','Кохма','Красавино','Красноармейск','Красноармейск','Красновишерск','Красногорск','Краснодар','Краснозаводск','Краснознаменск','Краснознаменск','Краснокаменск','Краснокамск','Краснослободск','Краснослободск','Краснотурьинск','Красноуральск','Красноуфимск','Красноярск','Красный Кут','Красный Сулин','Красный Холм','Кропоткин','Крымск','Кстово','Кстово','Кувандык','Кувшиново','Кудымкар','Кузнецк','Куйбышев','Кулебаки','Кулебаки','Кумертау','Кунгур','Купино','Курган','Курганинск','Курильск','Курлово','Куровское','Курск','Куртамыш','Курчатов','Куса','Кушва','Кызыл','Кыштым','Кяхта','Лабинск','Лабытнанги','Лагань','Ладушкин','Лакинск','Лангепас','Лахденпохья','Лебедянь','Лениногорск','Ленинск','Ленинск-Кузнецкий','Ленск','Лермонтов','Лесной','Лесозаводск','Лесосибирск','Ливны','Липецк','Липки','Лиски','Лихославль','Лобня','Лодейное Поле','Лосино-Петровский','Луга','Луза','Лукоянов','Лукоянов','Луховицы','Лысково','Лысково','Лысьва','Лыткарино','Льгов','Любань','Люберцы','Любим','Людиново','Лянтор','Магадан','Магас','Магнитогорск','Майкоп','Майский','Макаров','Макарьев','Макушино','Малая Вишера','Малгобек','Малмыж','Малоархангельск','Малоярославец','Мамадыш','Мамоново','Мантурово','Мариинск','Мариинский Посад','Маркс','Махачкала','Мглин','Мегион','Медвежьегорск','Медногорск','Медынь','Межгорье','Междуреченск','Мезень','Меленки','Мелеуз','Менделеевск','Мензелинск','Мещовск','Миасс','Микунь','Миллерово','Минеральные Воды','Минусинск','Миньяр','Мирный','Мирный','Михайлов','Михайловка','Михайловск','Михайловск','Мичуринск','Могоча','Можайск','Можга','Моздок','Мончегорск','Морозовск','Моршанск','Мосальск','Москва','Муравленко','Мураши','Мурманск','Муром','Мценск','Мыски','Мытищи','Мышкин','Набережные Челны','Навашино','Навашино','Наволоки','Надым','Назарово','Назрань','Называевск','Нальчик','Нариманов','Наро-Фоминск','Нарткала','Нарьян-Мар','Находка','Невель','Невельск','Невинномысск','Невьянск','Нелидово','Неман','Нерехта','Нерчинск','Нерюнгри','Нестеров','Нефтегорск','Нефтекамск','Нефтекумск','Нефтеюганск','Нея','Нижневартовск','Нижнекамск','Нижнеудинск','Нижние Серги','Нижний Ломов','Нижний Новгород','Нижний Новгород','Нижний Тагил','Нижняя Салда','Нижняя Тура','Николаевск','Николаевск-на-Амуре','Никольск','Никольск','Никольское','Новая Ладога','Новая Ляля','Новоалександровск','Новоалтайск','Новоаннинский','Нововоронеж','Новодвинск','Новозыбков','Новокубанск','Новокузнецк','Новокуйбышевск','Новомичуринск','Новомосковск','Новопавловск','Новоржев','Новороссийск','Новосибирск','Новосиль','Новосокольники','Новотроицк','Новоузенск','Новоульяновск','Новоуральск','Новохоперск','Новочебоксарск','Новочеркасск','Новошахтинск','Новый Оскол','Новый Уренгой','Ногинск','Нолинск','Норильск','Ноябрьск','Нурлат','Нытва','Нюрба','Нягань','Нязепетровск','Няндома','Облучье','Обнинск','Обоянь','Обь','Одинцово','Ожерелье','Октябрьск','Октябрьский','Окуловка','Оленегорск','Олонец','Омск','Омутнинск','Онега','Опочка','Орёл','Оренбург','Орехово-Зуево','Орлов','Орск','Оса','Осинники','Осташков','Остров','Островной','Острогожск','Отрадное','Отрадный','Оха','Оханск','Павлово','Павлово','Павловск','Павловский Посад','Палласовка','Партизанск','Певек','Пенза','Первомайск','Первомайск','Первоуральск','Перевоз','Перевоз','Пересвет','Переславль-Залесский','Пермь','Пестово','Петров Вал','Петровск','Петровск-Забайкальский','Петрозаводск','Петропавловск-Камчатский','Петухово','Петушки','Печора','Печоры','Пионерский','Питкяранта','Плавск','Пласт','Поворино','Подольск','Подпорожье','Покачи','Покров','Покровск','Полевской','Полесск','Полысаево','Полярные Зори','Полярный','Поронайск','Порхов','Похвистнево','Почеп','Починок','Пошехонье','Правдинск','Приволжск','Приморск','Приморско-Ахтарск','Приозерск','Прокопьевск','Пролетарск','Протвино','Прохладный','Псков','Пудож','Пустошка','Пучеж','Пушкино','Пущино','Пыталово','Пыть-Ях','Пятигорск','Радужный','Радужный','Райчихинск','Раменское','Рассказово','Ревда','Реж','Реутов','Ржев','Родники','Рославль','Россошь','Ростов-на-Дону','Рошаль','Ртищево','Рубцовск','Рудня','Руза','Рузаевка','Рыбинск','Рыбное','Рыльск','Ряжск','Рязань','Салават','Салаир','Салехард','Сальск','Самара','Санкт-Петербург','Саранск','Сарапул','Саратов','Саров','Саров','Сасово','Сатка','Сафоново','Саяногорск','Саянск','Светлогорск','Светлоград','Светлый','Светогорск','Свирск','Свободный','Себеж','Северобайкальск','Северодвинск','Северо-Курильск','Североморск','Североуральск','Северск','Севск','Сегежа','Сельцо','Семикаракорск','Семилуки','Сенгилей','Серафимович','Сергач','Сергач','Сергиев Посад','Сердобск','Серов','Серпухов','Сертолово','Сибай','Сим','Сковородино','Скопин','Славгород','Славск','Славянск-на-Кубани','Сланцы','Слободской','Слюдянка','Смоленск','Снежинск','Снежногорск','Собинка','Советск','Советск','Советск','Советская Гавань','Советский','Сокол','Солигалич','Соликамск','Солнечногорск','Сольвычегодск','Соль-Илецк','Сольцы','Сорочинск','Сорск','Сортавала','Сосенский','Сосновка','Сосновоборск','Сосновый Бор','Сосногорск','Сочи','Спас-Деменск','Спас-Клепики','Спасск','Спасск-Дальний','Спасск-Рязанский','Среднеколымск','Среднеуральск','Сретенск','Ставрополь','Старая Русса','Старица','Стародуб','Старый Оскол','Стерлитамак','Стрежевой','Строитель','Струнино','Ступино','Суворов','Суджа','Судогда','Суздаль','Суоярви','Сураж','Сургут','Суровикино','Сурск','Сусуман','Сухиничи','Сухой Лог','Сходня','Сызрань','Сыктывкар','Сысерть','Сычевка','Сясьстрой','Тавда','Таганрог','Тайга','Тайшет','Талдом','Талица','Тамбов','Тара','Таруса','Татарск','Таштагол','Тверь','Теберда','Тейково','Темников','Темрюк','Терек','Тетюши','Тимашевск','Тихвин','Тихорецк','Тобольск','Тогучин','Тольятти','Томари','Томмот','Томск','Топки','Торжок','Торопец','Тосно','Тотьма','Трехгорный','Троицк','Троицк','Трубчевск','Туапсе','Туймазы','Тула','Тулун','Туран','Туринск','Тутаев','Тында','Тырныауз','Тюкалинск','Тюмень','Уварово','Углегорск','Углич','Удачный','Удомля','Ужур','Узловая','Улан-Удэ','Ульяновск','Унеча','Урай','Урень','Урень','Уржум','Урус-Мартан','Урюпинск','Усинск','Усмань','Усолье','Усолье-Сибирское','Уссурийск','Усть-Джегута','Усть-Илимск','Усть-Катав','Усть-Кут','Усть-Лабинск','Устюжна','Уфа','Ухта','Учалы','Уяр','Фатеж','Фокино','Фокино','Фролово','Фрязино','Фурманов','Хабаровск','Хадыженск','Ханты-Мансийск','Харабали','Харовск','Хасавюрт','Хвалынск','Хилок','Химки','Холм','Холмск','Хотьково','Цивильск','Цимлянск','Чадан','Чайковский','Чапаевск','Чаплыгин','Чебаркуль','Чебоксары','Чегем','Чекалин','Челябинск','Чердынь','Черемхово','Черепаново','Череповец','Черкесск','Черноголовка','Черногорск','Чернушка','Черняховск','Чехов','Чистополь','Чита','Чкаловск','Чкаловск','Чудово','Чулым','Чусовой','Чухлома','Шагонар','Шадринск','Шали','Шарыпово','Шарья','Шатура','Шахты','Шахунья','Шахунья','Шацк','Шебекино','Шелехов','Шенкурск','Шилка','Шимановск','Шиханы','Шлиссельбург','Шумерля','Шумиха','Шуя','Щербинка','Щигры','Щучье','Электрогорск','Электросталь','Электроугли','Элиста','Энгельс','Эртиль','Юбилейный','Югорск','Южа','Южно-Сахалинск','Южно-Сухокумск','Южноуральск','Юрга','Юрьевец','Юрьев-Польский','Юрюзань','Юхнов','Ядрин','Якутск','Ялуторовск','Янаул','Яранск','Яровое','Ярославль','Ярцево','Ясногорск','Ясный','Яхрома'];

  autocomplete(document.getElementById('inputEventPlaceid'), countries);
});

</script>



<script src="../js/bootstrap300.min.js"></script>

<?php require_once('v/f-f5min.php'); ?>

</body>
</html>