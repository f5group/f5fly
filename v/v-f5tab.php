<?php 
$f5start = new f5start; 
$getf5lang = $f5start->getf5GUIlang();
$getBASEDIR = $f5start->getf5BaseDIR();
$getMotdPage = $f5start->getMOTDpage_tab();
$getf5url01 = $f5start->getf5url01x();
$getf5url02 = $f5start->getf5url02x();
$getf5url03 = $f5start->getf5url03x();
$guiTAB_BTN01 = $f5start->getf5tabBtn01str();
$guiTAB_BTN02 = $f5start->getf5tabBtn02str();
$guiTAB_BTN03 = $f5start->getf5tabBtn03str();
$guiTAB_BTN04 = $f5start->getf5tabBtn04str();
$guiTAB_BTN05 = $f5start->getf5tabBtn05str();
?>



<!-- f5-core maibody div wrapper container --> 
<div id="mainbody">
<!-- f5-core main wrapper container --> 
<div class="container">
<br><br>
      <div class="wjumbotron">

<h3>
<div class="wrap2logo"><img class="logoimgmain" src="<?php echo $getBASEDIR; ?>/img/f5-logo-blue-el.png" height="77"><span class="text-successf5">[ f5 <sub>/ fly </sub> ] </span>
</div>
</h3>

<!-- show flag and  ava propperty q div block --> 
<div class="ttheright">
  <div class="row">
    <div class="col-xs-12">
    </div>
  </div>
  </div>

    <p class="ttheright"><?php echo $getMotdPage; ?></p>

<div class="panel panel-default">
<div class="panel-heading text-left"><br>
<!-- <h4><sub>#tablet&nbsp;</sub><sup><span class="label label-warning">ver</span></sup></h4> -->
<div class="flexbox-container">
<button id="f5mobBtn01id" class="f5mobBtn01" data-loading-text="<i class='fa fa-spinner fa-spin'></i> OK!"><span><?=$guiTAB_BTN01;?> </span></button>
<button id="f5mobBtn02id" class="f5mobBtn02" data-loading-text="<i class='fa fa-spinner fa-spin'></i> OK!"><span><?=$guiTAB_BTN02;?> </span></button>
<button id="f5mobBtn03id" class="f5mobBtn03" data-loading-text="<i class='fa fa-spinner fa-spin'></i> OK!"><span><?=$guiTAB_BTN03;?> </span></button>
<button id="f5mobBtn04id" class="f5mobBtn04" data-loading-text="<i class='fa fa-spinner fa-spin'></i> OK!"><span><?=$guiTAB_BTN04;?> </span></button>
<button id="f5mobBtn05id" class="f5mobBtn05" data-loading-text="<i class='fa fa-spinner fa-spin'></i> OK!"><span><?=$guiTAB_BTN05;?> </span></button>
</div>
</div>

</div>
    </div> <!-- /wjumbotron -->


</div><!-- /container -->
</div><!-- /mainbody -->
<script>
$(document).ready(function(){ 
  $('#f5mobBtn01id').click(function(){
    $('#f5mobBtn01id').button('loading');
    setTimeout(function(){ $('#f5mobBtn01id').button('reset'); window.location.replace('/p'); }, 1234); 
  });
  $('#f5mobBtn02id').click(function(){
    $('#f5mobBtn02id').button('loading');
    setTimeout(function(){ $('#f5mobBtn02id').button('reset'); window.location.replace('/d'); }, 1234); 
  });
  $('#f5mobBtn03id').click(function(){
    $('#f5mobBtn03id').button('loading');
    setTimeout(function(){ $('#f5mobBtn03id').button('reset'); window.location.replace('/t'); }, 1234); 
  });
  $('#f5mobBtn04id').click(function(){
    $('#f5mobBtn04id').button('loading');
    setTimeout(function(){ $('#f5mobBtn04id').button('reset'); window.location.replace('/j'); }, 1234); 
  });
  $('#f5mobBtn05id').click(function(){
    $('#f5mobBtn05id').button('loading');
    setTimeout(function(){ $('#f5mobBtn05id').button('reset'); window.location.replace('/s'); }, 1234); 
  });        
});
</script>
<script>
  tippy('[title]', { placement: 'bottom', animation: 'scale', inertia: true, size:'large', interactiveBorder:7,duration: 777, followCursor: true, arrow: true });
</script>

    <script src="../js/bootstrap300.min.js"></script>

<?php require_once('v/f5footer-min.php'); ?>
</body>
</html>