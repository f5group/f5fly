<?php
$f5start = new f5start; 
$getf5lang = $f5start->getf5GUIlang();
$getBASEDIR = $f5start->getf5BaseDIR();
$getHTMLTitlePage = $f5start->getTITLEpage_tab();
$getf5name = $f5start->getf5name();
$getf5ver = $f5start->getf5version();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo @$getHTMLTitlePage; ?></title>
  <link rel="icon" href="../img/favicon.ico">
    <!-- f5 core CSS -->
  <!-- <link href="../css/f5index.css" rel="stylesheet"> -->
  <link href="../css/f5mobile.css" rel="stylesheet">
  <!-- Bootstrap and also core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
  <link href="../css/bootstrap-editable.css" rel="stylesheet" type="text/css">
  <!-- <link href="../css/hover-min.css" rel="stylesheet"> -->
  <link href="../css/font-awesome.min.css" rel="stylesheet">  
  <meta name=”robots” content=””>

<meta name=”robots” content=”noindex,noarchive,nofollow,noodp,oydir” >
<meta http-equiv="expires" content="Thu, 13 May 2027 00:00:00 GMT"/>
<meta http-equiv="pragma" content="no-cache" />
<link rel="apple-touch-icon" sizes="180x180" href="img/touch/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/touch/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/touch/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<link rel="mask-icon" href="img/touch/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta name="f5-autosport" content="f5sys">
<meta name="msapplication-TileColor" content="#FF0000">
<meta name="msapplication-TileImage" content="tile-background.png">
<meta name="msapplication-badge" content="frequency=60;polling-uri=http://host/badge.xml">
<meta name="msapplication-notification" content="frequency=30;polling-uri=http://host/live.xml">
<!-- for ios 7 style, multi-resolution icon of 152x152 -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
<link rel="apple-touch-icon" href="img/touch/icon-152.png">
<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
<meta name="mobile-web-app-capable" content="yes">
<link rel="shortcut icon" sizes="196x196" href="img/touch/icon-196.png">


<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/libs/moment.min.js"></script>
<script src="../js/tippy.all.min.js"></script>

  </head>
<body id="mainz">
            <nav id="navbarmain" class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand f5activetoprootmenu" title="f5 главная / старт" href="/">[ f5/fly ]
            </a>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav"> <li>
              <li class="mmnu0"><a id="p01" title="Пуль управления!" href="p"><b>Пульт</b> <span class="glyphicon glyphicon-queen"></span></a></li>
              <li class="mmnu0"><a id="d02" title="модуль - Дубль" href="d"><b>Дубль</b> <span class="glyphicon glyphicon-list-alt"></span></a></li>
              <li class="mmnu0"><a id="t03" title="модуль - Табло" href="t"><b>Табло</b> <span class="glyphicon glyphicon-hd-video"></span></a></li>
              <li class="mmnu0"><a id="j04" title="модуль - Судейство" href="j"><b>Судьи</b> <span class="glyphicon glyphicon-phone"></span></a></li>
              <li class="mmnu0"><a id="s05" title="модуль - Сигнал для Атлета на помосте" href="s"><b>Сигнал</b> <span class="glyphicon glyphicon-bullhorn"></span></a></li>
            </ul>

<ul class="nav navbar-nav navbar-right">
<li class="dropdown">
<a href="#" class="dropdown-toggle glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" role="button" aria-expanded="false"></a>
<ul class="dropdown-menu" role="menu">
<li><a id="j" title="перерыв - режим Табло #sh0wTime" href="sht"><span class="glyphicon glyphicon-music" aria-hidden="true"> Перерыв </span></a></li>
<li class="divider"></li>
<li><a id="tblrefresh" title="#Сбросить Табло" href="#"><span class="glyphicon glyphicon-refresh" aria-hidden="true"> Сброс табло</span></a></li> 
<li class="divider"></li>
<li><a target="_blank" title="#Важные настройки" href="opt"> <span class="glyphicon glyphicon-cog" aria-hidden="true"> #настройки </span></a></li>
<li class="divider"></li>
<li><a target="_blank" title="#dataBase" href="../core/phpliteadmin.php"> <span class="glyphicon glyphicon-hdd" aria-hidden="true"> #db</span></a></li>
<li class="divider"></li>
<li><a target="_blank" title="#about f5/fly" href="../abt"> &nbsp;&nbsp;&nbsp; #ab0ut f5&nbsp;<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a></li>
</ul>
</li>
</ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>