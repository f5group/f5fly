<?php 
//$f5start = new f5start; 
//$getf5lang = $f5start->getf5GUIlang();
//$getBASEDIR = $f5start->getf5BaseDIR();
//$getHTMLpageTITLE = $f5start->getTITLEpage('index');
//$getGUIpage1HEADER = $f5start->getHEADER1page('index');
?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
    <title><?php echo @$getHTMLpageTITLE; ?></title>
  <link rel="icon" href="../img/favicon.ico">
    <!-- f5 core CSS -->
  <link href="../css/f5index.css" rel="stylesheet">
  <!-- Bootstrap and also core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../css/daterangepicker.css" rel="stylesheet" type="text/css">
  <link href="../css/pretty-checkbox.min.css" rel="stylesheet" type="text/css">
  <link href="../css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
  <link href="../css/bootstrap-editable.css" rel="stylesheet" type="text/css">
  <!-- <link href="../css/hover-min.css" rel="stylesheet"> -->
  <meta name=”robots” content=””>

<meta name=”robots” content=”noindex,noarchive,nofollow,noodp,oydir” >
<meta http-equiv="expires" content="Thu, 13 May 2027 00:00:00 GMT"/>
<meta http-equiv="pragma" content="no-cache" />
<link rel="apple-touch-icon" sizes="180x180" href="img/touch/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/touch/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/touch/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<link rel="mask-icon" href="img/touch/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta name="f5-autosport" content="f5sys">
<meta name="msapplication-TileColor" content="#FF0000">
<meta name="msapplication-TileImage" content="tile-background.png">
<meta name="msapplication-badge" content="frequency=60;polling-uri=http://host/badge.xml">
<meta name="msapplication-notification" content="frequency=30;polling-uri=http://host/live.xml">
<!-- for ios 7 style, multi-resolution icon of 152x152 -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
<link rel="apple-touch-icon" href="img/touch/icon-152.png">
<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
<meta name="mobile-web-app-capable" content="yes">
<link rel="shortcut icon" sizes="196x196" href="img/touch/icon-196.png">



<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/libs/moment.min.js"></script>
<script src="../js/libs/daterangepicker.js"></script>
<script src="../js/tippy.all.min.js"></script>
<script>
  $(document).ready(function(){
    $(".hamburger").click(function(){ $(this).toggleClass("is-active"); });
  });
</script>
  </head>
<body id="mainz">
            <nav id="navbarmain" class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand f5activetoprootmenu" title="<?=$f5start->getTMENUi0('index',1);?>" href="/"><?=$f5start->getTMENUi0('index',0);?>
            </a>
          </div>

    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav"> <li>
        <li class="mmnu0"><a id="p01" title="<?=$f5start->getTMENUi1('index',1);?>" href="/m"><b><i class="icon mdi mdi-24px mdi-google-controller"> </i><?=$f5start->getTMENUi1('index',0);?></b><i class="icon mdi mdi-arrow-right-drop-circle-outline"> </i></a></li>
        <li class="mmnu0"><a id="d02" title="<?=$f5start->getTMENUi2('index',1);?>" href="/d"><b><i class="icon mdi mdi-24px mdi-table-large"> </i><?=$f5start->getTMENUi2('index',0);?></b><i class="icon mdi mdi-high-definition"> </i></a></li>
        <li class="mmnu0"><a id="t03" title="<?=$f5start->getTMENUi3('index',1);?>" href="/t"><b><i class="icon mdi mdi-24px mdi-television"> </i><?=$f5start->getTMENUi3('index',0);?></b><i class="icon mdi mdi-high-definition"> </i></a></li>
        <li class="mmnu0"><a id="j04" title="<?=$f5start->getTMENUi4('index',1);?>" href="/j"><b><i class="icon mdi mdi-24px mdi-scale-balance"></i><?=$f5start->getTMENUi4('index',0);?></b><i class="icon mdi mdi-cellphone-iphone"> </i></span></a></li>
        <li class="mmnu0"><a id="s05" title="<?=$f5start->getTMENUi5('index',1);?>" href="/s"><b><i class="icon mdi mdi-24px mdi-bell-outline"></i><?=$f5start->getTMENUi5('index',0);?></b><i class="icon mdi mdi-standard-definition"></i></a></li>
      </ul>

<ul class="nav navbar-nav navbar-right">
<li class="dropdown">

<!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="icon mdi mdi-24px mdi-menu"> </i></a> -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <div class="hamburger" id="hamburgerMENU">
          <span class="line"></span>
          <span class="line"></span>
          <span class="line"></span>
        </div>
        </a>
<ul class="dropdown-menu" role="menu">
  <li class="divider"></li>
<li><a title="<?=$f5start->getTMENUi6('index',1);?>" href="/sht"><i class="icon mdi mdi-24px mdi-silverware-variant"></i> <?=$f5start->getTMENUi6('index',0);?></a></li>
<li class="divider"></li>
<li><a title="<?=$f5start->getTMENUi7('index',1);?>" href="/opt"> <i class="icon mdi mdi-24px mdi-settings-box"> </i> <?=$f5start->getTMENUi7('index',0);?></a></li>
<li class="divider"></li>
<li><a target="_blank" title="<?=$f5start->getTMENUi8('index',1);?>" href="/db"><i class="icon mdi mdi-24px mdi-database"> </i> <?=$f5start->getTMENUi8('index',0);?></a></li>
<li class="divider"></li>
<li><a title="<?=$f5start->getTMENUi9('index',1);?>" href="/abt"> &nbsp;&nbsp;&nbsp; <?=$f5start->getTMENUi9('index',0);?>&nbsp;<i class="icon mdi mdi-24px mdi-information"> </i></a></li>
<li class="divider"></li>
</ul>
</li>
</ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>