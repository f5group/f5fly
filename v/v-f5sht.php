<?php 

$f5start = new f5start; 
$getf5lang = $f5start->getf5GUIlang();
$getBASEDIR = $f5start->getf5BaseDIR();
$getGUIpage1HEADER = $f5start->getHEADER1page('sh0wTime');
$getGUIpage1HEADERico = $f5start->getHEADER1pageIco('sh0wTime');
$getGUIpage1panelHEADING = $f5start->getHEADING1panel('sh0wTime');
$getGUIpage2panelHEADING = $f5start->getHEADING2panel('sh0wTime');
$getGUIpage2panelTITLE = $f5start->getTITLE2panel('sh0wTime');

$getGUI_page2panelBUTTON1 = $f5start->get1BUTTON2panel('sh0wTime');
$getGUI_page2panelBUTTON2 = $f5start->get2BUTTON2panel('sh0wTime');

$getGUIpage1LABEL = $f5start->getLABEL1page('sh0wTime');
$getGUIpage1valOPTION = $f5start->getOPT11val1page('sh0wTime');
$getGUIpage2valOPTION = $f5start->getOPT11val2page('sh0wTime');
$getGUIpage3valOPTION = $f5start->getOPT11val3page('sh0wTime');
$getGUIpage4valOPTION = $f5start->getOPT11val4page('sh0wTime');
$getGUIpage5valOPTION = $f5start->getOPT11val5page('sh0wTime');

$getGUIpage1opt1CHECKBOX = $f5start->getCHEKBOX1opt1page('sh0wTime');
$getGUIpage1opt2CHECKBOX = $f5start->getCHEKBOX1opt2page('sh0wTime');
$getGUIpage2opt1CHECKBOX = $f5start->getCHEKBOX2opt1page('sh0wTime');
$getGUIpage2opt2CHECKBOX = $f5start->getCHEKBOX2opt2page('sh0wTime');

$getf5name = $f5start->getf5name();
$getf5ver = $f5start->getf5version();
$getf5rdate = $f5start->getf5releaseDate();
$getf5url01 = $f5start->getf5url01x();
$getf5url02 = $f5start->getf5url02x();
$getf5url03 = $f5start->getf5url03x();
$getf5header01 = $f5start->getPageIndexSht01();

$getGUIbtn01 = $f5start->getPageIndexBtn01();
$getGUIpwd4juda_str01 = $f5start->getPageIndexJPWDstr01();
$getPWD4judas = $f5start->getPageIndexJPWD();



$getf5shTime_minutes = $f5start->getf5st_min();
$getf5shTime_onTablo = $f5start->getf5st_onTablo();
$getf5shTime_onDouble = $f5start->getf5st_onDouble();
$getf5shTime_isRUN = $f5start->getf5st_isRun();

$core_ipv4Str = $f5start->getCore_ipv4();
$core_port = $f5start->getCore_port();
$coreSocketStr = "ws://".$core_ipv4Str.':'.$core_port;
if (!$coreSocketStr) { var_dump('ERROR --> !getCore_socketStr() '); }

?>

<link href="../css/css3rd/tinyplayer.css" rel="stylesheet" media="all" type="text/css">
<script src="../js/fancywebsocket.js"></script>
<script src="../js/chrono.js"></script>
<script src="../js/libs/tinyplayer.js"></script>

<!-- f5-core maibody div wrapper container --> 
<div id="mainbody">
<!-- f5-core main wrapper container --> 
<div class="container">
<br><br>
      <div class="wjumbotron">

<h3>
<div class="wrap2logo"><img class="logoimgmain" src="<?php echo $getBASEDIR; ?>/img/f5-logo-blue-el.png"><span class="text-successf5">[ f5 <sub>/ fly </sub> ] </span>
</div>
</h3>



<!-- show flag and  ava propperty q div block --> 
<div class="ttheright">
  <div class="row">
    <div class="col-xs-12">
    </div>
  </div>
  </div>

<script>
$(document).ready(function() {
  let hideThis = document.getElementById('screenPanel_hiderBtn'); // hide tablo/double GUI screen Panel
    hideThis.click();
  let btnStart_ST = document.getElementById('playBtn01');
  let btnStart_SToff = document.getElementById('playBtn01off');
  let getOpt_ = document.getElementById('sh0wTimeONtablo');
  let getOpt_val_ = document.getElementById('sh0wTimeONtablo').value;
  let getOpt2_ = document.getElementById('sh0wTimeONdouble');
  let getOpt2_val_ = document.getElementById('sh0wTimeONdouble').value;
$.get("../core/model-f5start.php?getSTisONtablo", { "shTimeONtablo": 1 },
  function(data) {
    console.log("shTimeONtablo:" + data);
    if(data==0) { getOpt_.value='off'; } else { console.log('?_ '+ getOpt_val_ + '_' + getOpt2_val_); getOpt_.value='on'; getOpt_.checked = true; }
});
$.get("../core/model-f5start.php?getSTisONdouble", { "shTimeONdouble": 1 },
  function(data) {
    console.log("shTimeONdouble:" + data);
    if(data==0) { getOpt2_.value='off'; } else { console.log('?_ '+ getOpt_val_ + '_' + getOpt2_val_); getOpt2_.value='on'; getOpt2_.checked = true; }
});


if(getOpt_val_ == 'on') {
  console.log('?_ '+ getOpt_val_ + '_' + getOpt2_val_); 
  btnStart_ST.classList.toggle("disabled");
  btnStart_ST.style.visibility = 'none';
  btnStart_ST.style.visibility = 'hidden';
}

  Server = new FancyWebSocket('<?=$coreSocketStr;?>');
    $('#message').keypress(function(e) {
    if ( e.keyCode == 13 && this.value ) {
      log( 'You: ' + this.value );
      send( this.value );
      $(this).val('');
    }
    });
//Let the user know we're connected
    Server.bind('open', function() {
      log( "Connected." );
    });
//OH NOES! Disconnection occurred.
    Server.bind('close', function( data ) {
      log( "Disconnected." );
    });
//Log any messages sent from server
    Server.bind('message', function( payload ) {
      log( payload );
    });
    Server.connect();

function send( text ) {
  Server.send( 'message', text );
}
function log( text ) {
  $log = $('#log');
  $log.append(($log.val()?"\n":'')+text);
  //if(text.indexOf('pult_rfsh') + 1) { let timerId = setTimeout(function tblRefresh3s() { location.reload(); timerId = setTimeout(tblRefresh3s, 1234); }, 1234); }    
}

});


function showERRmsg(inputTXT) {
  console.log('error # 098756xff: BUTTON IS DISABLED!');
}

  var shT_t = 0;
  var shT_d = 0;
function start_sh0wTime(howManyMin) {
  let getOpt_val_ = document.getElementById('sh0wTimeONtablo').value; 
  let getOpt2_val_ = document.getElementById('sh0wTimeONdouble').value;  
  function send( text ) {
  Server.send( 'message', text );
}
  if (getOpt_val_ == "on") { let timerIdSh0wTime = setTimeout(function startShTime() { send('sh0wTime30tablo'); }, 1234); }
  if (getOpt2_val_ == "on") { let timerIdSh0wTime = setTimeout(function startShTime() { send('double_sh0wTime30'); }, 1234); }
    console.log(getOpt_val_ + '&'+getOpt2_val_ + ' t: ' + howManyMin + ' : start30' );
}
function pause_sh0wTime(howManyMin) {
  //let getOpt_val_ = document.getElementById('sh0wTimeONdouble').value;
  function send( text ) {
  Server.send( 'message', text );
}
  //if (getOpt_val_ == "on") { let timerIdSh0wTime = setTimeout(function startShTime() { send('sh0wTime_STOP'); }, 1234); }
  let timerIdSh0wTime = setTimeout(function startShTime() { send('sh0wTime_STOP'); }, 1234);
  console.log(getOpt_val_ + ' : STOP');
}
function sh0wTime_tablo() {
  let getOpt_ = document.getElementById('sh0wTimeONtablo');
  let getOpt2_ = document.getElementById('sh0wTimeONdouble');
  let getOpt_val_ = document.getElementById('sh0wTimeONtablo').value;
  let getOpt2_val_ = document.getElementById('sh0wTimeONdouble').value;
  let btnStart_ST = document.getElementById('playBtn01');
  let btnStart_SToff = document.getElementById('playBtn01off');
  if (getOpt_.value == 'on') { getOpt_.value = 'off'; } else { getOpt_.value = 'on'; }
//    console.log('?_ '+ getOpt_.value + '_%' + getOpt2_.value);
    console.log('ov_'+ getOpt_val_ + ' ov2='+ getOpt2_val_);
    if (getOpt_.value=='on') { 
      btnStart_SToff.style.visibility = 'hidden';
      btnStart_ST.classList.toggle("disabled");
      btnStart_ST.style.visibility = 'visible';
      $.get("../core/model-f5start.php?setSTisONtablo", { "isCMD": 1 }, function(data) { console.log("shTimeONtablo:" + data); });
      console.log('1-ON /tbl_');
    }
    if(getOpt_.value=='on' && getOpt2_.value=='on'){
      btnStart_SToff.style.visibility = 'hidden';
      btnStart_ST.classList.remove("disabled");
      btnStart_ST.style.visibility = 'visible';
      console.log('2ON DOUBLE');
    }
    if(getOpt_.value=='on' && getOpt2_.value=='off'){
      btnStart_SToff.style.visibility = 'hidden';
      btnStart_ST.style.visibility = 'visible';
      btnStart_ST.classList.remove("disabled");
      console.log('ON OFF1');
    }
    if(getOpt_.value=='off' && getOpt2_.value=='off'){
    getOpt_.disabled = false;
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'visible';
    btnStart_SToff.classList.add('disabled');
    btnStart_ST.style.visibility = 'hidden';
    $.get("../core/model-f5start.php?setSTisONtablo", { "isCMD": 0 }, function(data) { console.log("shTimeONtablo:" + data); });
    $.get("../core/model-f5start.php?setSTisONdouble", { "isCMD": 0 }, function(data) { console.log("shTimeONtablo:" + data); });
    console.log('OFF1 & OFF2');
    }    
}
function sh0wTime_double() {
  let getOpt_ = document.getElementById('sh0wTimeONtablo');
  let getOpt2_ = document.getElementById('sh0wTimeONdouble');
  let getOpt_val_ = document.getElementById('sh0wTimeONtablo').value;
  let getOpt2_val_ = document.getElementById('sh0wTimeONdouble').value;
  let btnStart_ST = document.getElementById('playBtn01');
  let btnST_STOP = document.getElementById('pauseBtn01');
  let btnStart_SToff = document.getElementById('playBtn01off');
  if (getOpt2_.value == 'on') { getOpt2_.value = 'off'; } else { getOpt2_.value = 'on'; }
  console.log('?_ '+ getOpt_val_ + '_' + getOpt2_val_);
  if (getOpt2_.value=='on') { 
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'hidden';
    btnStart_ST.classList.remove("disabled");
    btnStart_ST.style.visibility = 'visible';
    $.get("../core/model-f5start.php?setSTisONdouble", { "isCMD": 1 }, function(data) { console.log("shTimeONdouble:" + data); });
    console.log('onnnnnnD '); 
  }
  if(getOpt2_.value=='off') {
    getOpt_.disabled = false;
    btnStart_SToff.style.visibility = 'visible';
    btnStart_ST.classList.toggle("disabled");
    btnStart_ST.style.visibility = 'hidden';
    console.log('aaaaaafdsfdsfds2');
  if(getOpt2_.value=='on') { 
    getOpt_.disabled = false;  
    btnStart_SToff.style.visibility = 'hidden';
    btnStart_ST.style.visibility = 'visible';
    console.log('dsad__ ');
  }
  }
  if(getOpt_.value=='off'  && getOpt2_.value=='on'){
    getOpt_.disabled = false;
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'hidden';
    btnStart_ST.classList.remove("disabled");
    btnStart_ST.style.visibility = 'visible';
    console.log('1-OFF, 2-ON /dBl_');
  }
  if(getOpt2_.value=='off'  && getOpt_.value=='on'){
    getOpt_.disabled = false;
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'hidden';
    btnStart_ST.style.visibility = 'visible';
    btnStart_ST.classList.toggle("disabled")  
    console.log('1-OFF, 2-ON /dBl*');
  }  
  if(getOpt_.value=='on' && getOpt2_.value=='on'){
    getOpt_.disabled = false;
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'hidden';
    btnStart_ST.classList.remove('disabled');
    btnStart_ST.style.visibility = 'visible';
    console.log('1-ON & 2-ON /dBl');
  }
  if(getOpt_.value=='on' && getOpt2_.value=='off'){
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'hidden';
    btnStart_ST.classList.remove('disabled');
    btnStart_ST.style.visibility = 'visible';
    console.log('1-ON, 2-OFF /dBl');
  }  
  if(getOpt_.value=='off' && getOpt2_.value=='off'){
    getOpt_.disabled = false;
    getOpt2_.disabled = false;
    btnStart_SToff.style.visibility = 'visible';
    btnStart_SToff.classList.add("disabled");
    btnStart_ST.style.visibility = 'hidden';
    $.get("../core/model-f5start.php?setSTisONtablo", { "isCMD": 0 }, function(data) { console.log("shTimeONdouble:" + data); });
    $.get("../core/model-f5start.php?setSTisONdouble", { "isCMD": 0 }, function(data) { console.log("shTimeONfouble:" + data); });    
    console.log('1-OFF & 2-OFF /dBl');
  }  
}
function sh0WWW() {
  let getOpt_val_ = document.getElementById('sh0wTimeONtablo').value;
  let getOpt2_val_ = document.getElementById('sh0wTimeONdouble').value;
    console.log('ov_'+ getOpt_val_ + '  ov2_' + getOpt2_val_);
}
function YAXZZZZZZZZZZZZ() {
  let getOpt_ = document.getElementById('jcfgShowNameAtt');
  let getOpt_val_ = document.getElementById('jcfgShowNameAtt').value;
  if (getOpt_val_ == "on") { getOpt_.value = "off"; } else { getOpt_.value = "on"; }
  let jcfgCMD2 = getOpt_val_+'/20';
  $.get("../core/model-config.php?setView_JudgeCfg", { "jcfgCMD": jcfgCMD2 },
    function(data) {
      console.log("jcfgCMD:" + data); //document.location.reload(); 
    });
}

function STARTsh0wTimeCountdown( id0Felement ) {
  let chkbox_shONtablo = document.getElementById('sh0wTimeONtablo');
  let chkbox_shONdouble = document.getElementById('sh0wTimeONdouble');
  let timerboxS_ = document.getElementById('ssec00');
  let timerboxM_ = document.getElementById('mmin00');
  if(timerboxS_.innerHTML=='00' && timerboxM_.innerHTML=='10') { timer.reset(600); console.log(timerboxS_.innerHTML+':'+timerboxS_.innerHTML+'!!!'); }
  if(timerboxS_.innerHTML=='00' && timerboxM_.innerHTML=='15') { timer.reset(900); console.log(timerboxS_.innerHTML+':'+timerboxS_.innerHTML+'!!!'); }
  if(timerboxS_.innerHTML=='00' && timerboxM_.innerHTML=='20') { timer.reset(1200); console.log(timerboxS_.innerHTML+':'+timerboxS_.innerHTML+'!!!'); }
  if(timerboxS_.innerHTML=='00' && timerboxM_.innerHTML=='25') { timer.reset(1500); console.log(timerboxS_.innerHTML+':'+timerboxS_.innerHTML+'!!!'); }
  if(timerboxS_.innerHTML=='00' && timerboxM_.innerHTML=='30') { timer.reset(1800); console.log(timerboxS_.innerHTML+':'+timerboxS_.innerHTML+'!!!'); }
  console.log(id0Felement + ' val: ' + timer.getTime());
  if(id0Felement=='pauseBtn01'){
    chkbox_shONtablo.disabled = false;
    chkbox_shONdouble.disabled = false;
    pause_sh0wTime();
    timer.stop();
  }
  if(id0Felement=='playBtn01'){
    let setTime = timerboxM_.innerHTML*60;
    if (timerboxS_!=0) {
      setTime = timer.getTime(); start_sh0wTime(timerboxM_.innerHTML);
      chkbox_shONtablo.disabled = true;
      chkbox_shONdouble.disabled = true;
    }
    if (timerboxS_==0) { alert('222'); setTime = timerboxM_.innerHTML*60; }
    console.log('t1me: ' + setTime +'='+ timer.getTime());
    timer.mode(0); timer.reset(setTime); timer.start(1000);
  }
}
$(document).on('click', '.panel-heading li.clickable', function(e){
    var $this = $(this);
  if(!$this.hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideUp();
    $this.addClass('panel-collapsed');
    $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
  } else {
    $this.parents('.panel').find('.panel-body').slideDown();
    $this.removeClass('panel-collapsed');
    $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
  }
})

</script>



<script>
  /* Tiny HTML5 Music Player by Themistokle Benetatos */
  let origin = window.location.origin; //origin: "http://website.com", //host: "website.com",
    //hostname: "website.com", //href: "http://website.com/", //origin: "http://website.com",
  TrackList = 
    [
      {
        url:origin+'/snd/sh0wTime/track01.mp3',
        title:'ATL - Daemons',
        year:'2017'
      },
      {
        url:origin+'/snd/sh0wTime/track02.mp3',
        title:'Right of Stupidity',
        year:'2004'
      }
    ];
  tinyplayer(TrackList, true, true);
</script>

<script>
$(document).ready(function() {
  let playBtnIcon = $('#playBtn01');
  let pauseBtnIcon = $('#pauseBtn01');
  pauseBtnIcon.hide()
  playBtnIcon.click(function() {
    playBtnIcon.hide();
    pauseBtnIcon.show();
    pauseBtnIcon.toggleClass('active');
    return false;
  });
  pauseBtnIcon.click(function() {
    pauseBtnIcon.hide();
    playBtnIcon.show();
    pauseBtnIcon.toggleClass('active');
    return false;
  });  
});

function SelectMinutes2start(valueToSelect) { 
  timer.stop(); 
  let playBtnIcon = $('#playBtn01');
  let pauseBtnIcon = $('#pauseBtn01');  
  let timerbox_ = document.getElementById('mmin00');
  timerbox_.innerHTML = valueToSelect;
  timer.reset(valueToSelect*60);
  playBtnIcon.show();
  pauseBtnIcon.hide();
}


</script>

<p class="ttheright"> <?=$getGUIpage1HEADER;?> <?=$getGUIpage1HEADERico;?> </p>
      <div class="panel panel-default">
       <div class="panel-heading text-left">
          <? $getGUIpage1panelHEADING;?>
          <div class="hover07">
          <label> <span class="label-stacked"><?=$getGUIpage1LABEL;?></span> 
            <span class="plain-select">
                <select id="shtminutes" class="inp" name="min2sht" onchange="SelectMinutes2start(this.value);">
                <option value="10"><?=$getGUIpage1valOPTION;?></option>
                <option value="15"><?=$getGUIpage2valOPTION;?></option>
                <option value="20"><?=$getGUIpage3valOPTION;?></option>
                <option value="25"><?=$getGUIpage4valOPTION;?></option>
                <option value="30"><?=$getGUIpage5valOPTION;?></option>
                </select>
            </span> 
          </label>
          <div class="timerbox tool" title="хронограф" > <span id="mmin00" class="minute">05</span>:<span id="ssec00" class="second">00</span> </div>
          </div><br>


      <div class="pretty p-icon p-toggle p-plain">
      <input id="sh0wTimeONtablo" onchange="sh0wTime_tablo();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-monitor"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIpage1opt2CHECKBOX;?> <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIpage1opt2CHECKBOX;?> <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>
      <div class="pretty p-icon p-toggle p-plain">
      <input id="sh0wTimeONdouble" onchange="sh0wTime_double();" type="checkbox" />
      <div class="state p-on"><i class="icon mdi mdi-24px mdi-monitor-multiple"></i>
        <label class="text-info">&nbsp;&nbsp;<?=$getGUIpage2opt2CHECKBOX;?> <span class="label label-success">ON</span></label>
      </div>
      <div class="state p-off"><i class="icon mdi mdi-24px mdi-eye-off"></i>
        <label class="text-muted">&nbsp;&nbsp;<?=$getGUIpage2opt2CHECKBOX;?> <span class="label label-default">OFF</span></label>
      </div>
      </div><br><br>      

<?=$getGUIpage2panelHEADING;?>
<a id="playBtn01" href="#" class="btnf5pplay-circle" onclick="STARTsh0wTimeCountdown(this.id);"><i class="glyphicon glyphicon-play"></i></a>
<a id="pauseBtn01" href="#" class="btnf5pplay-circle" onclick="STARTsh0wTimeCountdown(this.id);"><i class="glyphicon glyphicon-stop"></i></a>
<a id="playBtn01off" href="#" class="btnf5pplay-circle disabled" onclick="showERRmsg(this.id);"><i class="glyphicon glyphicon-play"></i></a>
<br><br>

<!-- Tabs -->
<div class="wrapperGUIScreen">
  <div class="row">
    <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title"><?=$getGUIpage2panelTITLE;?></h3><span class="pull-right">
      <ul class="nav panel-tabsx">
      <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon mdi mdi-television"> </i><?=$getGUI_page2panelBUTTON1;?><i class="icon mdi mdi-high-definition"> </i></a></li>
      <li><a href="#tab2" data-toggle="tab"><?=$getGUI_page2panelBUTTON2;?> <i class="icon mdi mdi-table-large"> </i></a></li>
      <li id="screenPanel_hiderBtn" class="clickable"><a href="#"><i class="glyphicon glyphicon-chevron-up"></i></a></li>
      </ul></span>
    </div>
    <div class="panel-body">
      <div class="tab-content">
      <div class="tab-pane active" id="tab1">  
        <div id="tabloWrap2iframeID" class="tabloWrap2iframe" style="width: 1200; height:1200;">
          <iframe src="http://f5/t" style="transform-origin: 0 0; transform: scale(0.5);" sandbox="allow-forms allow-same-origin allow-scripts allow-top-navigation" width="1200" height="1200"><p>browser not support iframes</p></iframe>
        </div>
      </div>
      <div class="tab-pane" id="tab2">
        <div id="doubleWrap2iframeID" class="doubleWrap2iframe">
          <iframe src="http://f5/d" sandbox="allow-forms allow-same-origin allow-scripts allow-top-navigation" width="600" height="600"><p>browser not support iframes</p></iframe>
        </div>
      </div>
      </div>
    </div>
    </div>
  </div>
</div>










<div class="bs-example">
  <div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">f5fly plaYer</h3>
    </div>    
    <div class="panel-body"> 
  <div class="wrapper-player">
    <div id="all_tracks"></div>
  </div>

    </div>

  </div>
</div>  


</div>
</div>



</div> 
</div><!-- /container -->

<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->
<!-- ############################################################################################################################# -->

</div><!-- /mainbody -->


<script src="../js/bootstrap300.min.js"></script>
<script src="../js/device.min.js"></script>

<script>
  tippy('[title]', { placement: 'bottom', animation: 'scale', inertia: true, size:'large', interactiveBorder:7,duration: 777, followCursor: true, arrow: true });
  //if(device.tablet()==true) { document.location.href = "/tab"; }
  //if(device.mobile()==true) { document.location.href = "/mob"; }
  $(document).ready(function() {
  let timerboxM_ = document.getElementById('mmin00');
  let timerboxS_ = document.getElementById('ssec00');
  timerboxM_.innerHTML = '10';
  timerboxS_.innerHTML = '00';
});
</script>

<?php require_once('v/f5footer-min.php'); ?>
</body>
</html>