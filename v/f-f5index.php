<script>
	let hsDIV = document.getElementById('f5BLACKfullname');
	//hsDIV.style.visibility = 'hidden';
	function hideBTITLE() {
		let hsDIV = document.getElementById('f5BLACKfullname');
		hsDIV.style.visibility = 'hidden';
	}
	function showBTITLE() {
		let hsDIV = document.getElementById('f5BLACKfullname');		
		hsDIV.style.visibility = 'visible';
	}
</script>
<div class="container">
	<small id="f5BLACKfullname"><kbd><?=$getf5fullName;?></kbd></small><br>
<div id="ind_footer01" class="panel panel-default">

<!-- INFO BLOCK & LICENSE/KEY BLOCK-->
<div class="row minLine">
  <div class="col-md-6 minLine"><div class="panel-heading text-left minLine">
  	<div class="qrWiFinet text-left" onmouseenter="hideBTITLE();" onmouseleave="showBTITLE();" style="width: 123px;" alt="QR Code for f5/fly WI-Fi NET"></div>
  </div>
</div>
</div>

<div class="panel-heading text-right">
<sup>release date: <?=$getf5rdate;?> &nbsp;</sup>
<a href='/abt'><button class="btn btn-default" type="button"><?=$getf5name;?> &nbsp; <span class="label label-primary">ver <?=$getf5ver;?></span></button></a>
<!-- CHECK THE LICENSE/KEY BLOCK  <h5>LICENCE IS - <span class="label label-danger">OUT</span></h5> -->
<h5><span class="label label-success">OK</span> - LICENCE</h5>
<div id="totalAthleteBtm" class="mainurl hvr-wobble-skew">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<span class="glyphicon glyphicon-globe"></span> <a href="<?=$getf5url01;?>" class="s5fg"><?=$getf5url01;?></a>
<span class="glyphicon glyphicon-info-sign"></span> <a href="<?=$getf5url02;?>" class="s5fg"><?=$getf5url02;?></a>&nbsp;&nbsp;&nbsp;
</div>
</div>
<div class="mainurl">
<a href="#" class="sf5"><span class="glyphicon glyphicon-phone"></span> mobi</a> &#10044; <a href="#" class="sf5"> options</a> &#10044; 
<a href="#" class="sf5">db <span class="glyphicon glyphicon-list-alt"></span></a>
</div>
</div>

<div class="barbelLine text-center"  alt="f5/fly systems"></div>

<ul id="lngPicker" class="languagepicker roundborders text-center">
<?php
	if ($getf5lang=='rus') {
 		echo '<a href="#rus" onclick="setGUIlng(1);"><li><img src="../img/flags/rus-fl-sq32.png"/>Русский</li></a>';
    echo '<a href="#eng" onclick="setGUIlng(2);"><li><img src="../img/flags/usa-fl-sq32.png"/>English</li></a>';
 	}
 	if ($getf5lang=='eng') {
		echo '<a href="#eng" onclick="setGUIlng(2);"><li><img src="../img/flags/usa-fl-sq32.png"/>English</li></a>'; 	
 		echo '<a href="#rus" onclick="setGUIlng(1);"><li><img src="../img/flags/rus-fl-sq32.png"/>Русский</li></a>';
 	}
?>
 </ul>
 </div>