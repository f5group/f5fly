
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo @$getHTMLTitlePage; ?></title>
  <link rel="icon" href="../img/favicon.ico">
    <!-- f5 core CSS -->
  <!-- <link href="../css/f5index.css" rel="stylesheet"> -->
  <link href="../css/f5mobile.css" rel="stylesheet">
  <!-- Bootstrap and also core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
  <link href="../css/bootstrap-editable.css" rel="stylesheet" type="text/css">
  <!-- <link href="../css/hover-min.css" rel="stylesheet"> -->
  <link href="../css/font-awesome.min.css" rel="stylesheet">  
  <meta name=”robots” content=””>

<meta name=”robots” content=”noindex,noarchive,nofollow,noodp,oydir” >
<meta http-equiv="expires" content="Thu, 13 May 2027 00:00:00 GMT"/>
<meta http-equiv="pragma" content="no-cache" />
<link rel="apple-touch-icon" sizes="180x180" href="img/touch/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/touch/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/touch/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<link rel="mask-icon" href="img/touch/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta name="f5-autosport" content="f5sys">
<meta name="msapplication-TileColor" content="#FF0000">
<meta name="msapplication-TileImage" content="tile-background.png">
<meta name="msapplication-badge" content="frequency=60;polling-uri=http://host/badge.xml">
<meta name="msapplication-notification" content="frequency=30;polling-uri=http://host/live.xml">
<!-- for ios 7 style, multi-resolution icon of 152x152 -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
<link rel="apple-touch-icon" href="img/touch/icon-152.png">
<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
<meta name="mobile-web-app-capable" content="yes">
<link rel="shortcut icon" sizes="196x196" href="img/touch/icon-196.png">


<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/tippy.all.min.js"></script>

  </head>
<body id="mainz">
            <nav id="navbarmain" class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand f5activetoprootmenu" title="f5 главная / старт" href="/">[ f5/fly ]
            </a>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav"> <li>
              

              <li class="mmnu0"><a id="j1" title="j1" href="j1"><b>jud1</b> <i class="icon mdi mdi-18px mdi-arrow-left-bold-box-outline"> </i></a></li>
              <li class="mmnu0"><a id="j2" title="j2" href="j2"><b>jud2</b> <i class="icon mdi mdi-18px mdi-image-filter-center-focus-weak"> </i></a></li>
              <li class="mmnu0"><a id="j3" title="j3" href="j3"><b>jud3</b> <i class="icon mdi mdi-18px mdi-arrow-right-bold-box-outline"> </i></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>