<!DOCTYPE html>
<html>
<head>
  <meta charset='UTF-8' />
<meta http-equiv="Cache-Control" content="no-cache">
<title>f5.fly Главное Табло - Сарики</title>
<meta http-equiv="Cache-Control" content="no-cache">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../css/tablo.m.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">

  <script src="../js/jquery-1.11.1.min.js"></script>
  <script src="../js/fancywebsocket.js"></script>
  <script src="../js/chrono.js"></script>
  <script src="../js/f5core.j.js"></script>

</head>
<body>


  <div id='bodyws' style="display: none">
    <textarea style="display: none" id='log' name='log' readonly='readonly'></textarea><br/>
    <input type='text' id='message' name='message' autofocus/>
    <input type='text' id='j1' name='message' />
    <input type='text' id='j2' name='message' />
    <input type='text' id='j3' name='message' /> ***
    <input type='text' id='final' name='message' />
  </div>

<div id="joobka" style="display: none" >
        <div class="control">
            <button onClick="timer.start(1000)">Start</button> 
            <button onClick="timer.stop()">Stop</button> 
            <button onClick="timer.reset(60)">Reset</button> 
            <button onClick="timer.mode(1)">Count up</button> 
            <button onClick="timer.mode(0)">Count down</button>
        </div>
</div>
  
<div id="therul" style="display: none">
  <input id="j11" type="button" value="j1yes" onclick="judasVotes(this.id)">
  <input id="j18" type="button" value="j1def" onclick="judasVotes(this.id)">
  <input id="j10" type="button" value="j1not" onclick="judasVotes(this.id)"> |
  <input id="j21" type="button" value="j2yes" onclick="judasVotes(this.id)">
  <input id="j28" type="button" value="j2def" onclick="judasVotes(this.id)">
  <input id="j20" type="button" value="j2not" onclick="judasVotes(this.id)"> |
  <input id="j31" type="button" value="j3yes" onclick="judasVotes(this.id)">
  <input id="j38" type="button" value="j3def" onclick="judasVotes(this.id)">
  <input id="j30" type="button" value="j3not" onclick="judasVotes(this.id)">
</div>







<div class="zi5">

  <div class="w3-container">
<!-- The container is a centered 960px -->
<div class="container">
    <?php 
$list = new f5tablo; 
echo $list->showTblAva();
?>

  <!-- Give column value in word form (one, two..., twelve) -->
  <div id="name2" class="twelve columns name2">
<?php 
$list = new f5tablo; 
$list->showTblFirstname('y');
?>

  
  </div>
    <div class="row">
    <div class="five columns rank">&nbsp;
    

    </div>
    <div id="trynumb" class="five columns trynumber"> . . .
      <script>
      $.get("../core/model-db.php?getTryNum", ongeAjaxSuccess );
      function ongeAjaxSuccess(data1) { var msg = document.getElementById("trynumb").innerHTML = data1; }
      </script>
    

</div>
    <div class="one columns timer">
      <div id="joobka">
<div class="timerbox">
            <span class="minute">43</span>:<span class="second">21</span>
        </div>
</div>
</div>
    
    </div>

  <!-- Sweet nested columns cleared with a clearfix class -->
  <div class="twelve columns clearfix">
    <!-- In nested columns give the first column a class of alpha
    and the second a class of omega -->
    <div class="one columns alpha"></div>
    <!-- !MAX LENTH = 15 symbols-->
    <div id ="name1" class="eleven columns omega name1">
      <p>
      <?php 
$list = new f5tablo; 
$list->showTblSecondname('y');
?>
  </p></div>
  </div>
  <!-- Sweet nested columns cleared by wrapping a .row -->
  <div class="five columns">
    <div class="row">
      <div class="three columns alpha"></div>
      <div class="one columns omega weight">
              <?php 
$list = new f5tablo; 
$list->showTblWeightNow('y');


?>
      </div>
    </div>
  </div>

  <!-- Can push over by columns -->
  <div class="five columns offset-by-one"></div>

</div>
</div>
</div>

<div id="ifnobox" style="display: none"></div>
<div id="ifyesbox" style="display: none"></div>
<div id="if30secbox" style="display: none"></div>
<div id="if10secbox" style="display: none"></div>

<div id="w3r_id_container" class="w3r_container">
  <div id="j1d" class="w3-third jxdefa">
    <h2>#1</h2>

  </div>

  <div id="j2d" class="w3-third jxdefa">
    <h2>#2</h2>

  </div>

  <div id="j3d" class="w3-third jxdefa">
    <h2>#3</h2>
    
  </div>
</div>

<script type="text/javascript">
  ///////////////// PlaySND func //////////////////////////////////////////////////////
function sndNo() {
                  document.getElementById('stop_player').play(); 
                $('#ifnobox').addClass("ifnobox"); $('#ifnobox').css( "display", "block" );
                  setTimeout("location.reload()", 9000);
                      }
function snd30sec() {
            document.getElementById('30sec_player').play();
            $('#if30secbox').addClass("if30secbox"); $('#if30secbox').css( "display", "block" );
                }
function snd10sec() {
            document.getElementById('10sec_player').play();
            $('#if10secbox').addClass("if10secbox"); $('#if10secbox').css( "display", "block" );
                }
function sndYes(){
          document.getElementById('start_player').play(); 
          //$.get("core/_set_jp_isget.php", { "is_get": "1" });
          $('#ifyesbox').addClass("ifyesbox"); $('#ifyesbox').css( "display", "block" );
          setTimeout(function () { location.reload(true);  }, 9000);
                  }   
</script>


<audio id="30sec_player" src="../snd/30sec.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="10sec_player" src="../snd/10sec.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="stop_player" src="../snd/no.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="start_player" src="../snd/yes.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndStart1" src="../snd/btnclick.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndStart2" src="../snd/btnclick.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndStop" src="../snd/btnclick.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndAdd" src="../snd/jchik.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndDel" src="../snd/jchik.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>
<audio id="sndSkip" src="../snd/_30sec.mp3" type="audio/mpeg">
Your browser does not support the audio element.</audio>

</body>
</html>