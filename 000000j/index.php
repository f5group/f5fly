<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/f5judge.css">
  <title>f5.func - Судейство - Выбор</title>
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/jmenu.js"></script>
</head>

<body id='mainz'>
<?php include "../v/nb-f5judge.php";  ?>
        
<div id="mainbody">
<div class="container">

<br><br>
<div class="wjumbotron">
<br><p class="ttheright"><?php echo '1l_motto'; ?></p>
<button type="button" onClick="location.replace('j.php?j1');" class="btn3d btn btn-white btn-lg"><span class="dbl_imgmbtn"><br>Судья #1</span></button><br>
<button type="button" onClick="location.replace('j.php?j2');" class="btn3d btn btn-white btn-lg"><span class="signal_imgmbtn"><br>Главный судья</span></button><br>
<button type="button" onClick="location.replace('j.php?j3');" class="btn3d btn btn-white btn-lg"><span class="judas_imgmbtn"><br>Судья #2</span></button><br>


</div> <!-- /wjumbotron -->
 
</div><!-- /container -->
</div><!-- /mainbody -->

</body>   
</html>
