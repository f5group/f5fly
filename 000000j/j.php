<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/f5judge.css">
  <title><?php echo $pageTitle; ?></title>
    <script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="../js/jmenu.js"></script>
    <script src="../js/judas.js"></script> 
    <script src="../js/fancywebsocket.js"></script>
   
</head>
<body id="mainz">
<div id="mboolko">
 
<?php include_once "../v/nb-f5judge.php"; include_once "../core/model-mngmt.php"; 
$get_f5cfg = new f5config();
$get_ipv4 =$get_f5cfg->getCore_ipv4('show');
$get_port =$get_f5cfg->getCore_port('show');
$coreSocketStr = 'ws://'.$get_ipv4.':'.$get_port;
?>
  

<div id="mainbody">
  <br><br>
  <section>
<div class="container" onload="fadeIn(mainbody)">
  <div id="jname" onclick="jclick2play(); jxdefa(); fadeIn(mainbody)">
    <script>
      try {
        str_num = "<?php echo $coreSocketStr ?>"; // STRING !!!!
        localStorage.setItem('socketURL',str_num);
      } catch (e) {
        if (e == QUOTA_EXCEEDED_ERR) {
          alert('localStorage QUOTA_EXCEEDED_ERROR!');
        }
      }
    let url = window.location.href.split("?");
    j = url[1];
    if(j.indexOf('j1') + 1) { document.write('судья &#10102;'); document.getElementById('mainz').style.backgroundImage = 'url("../img/bg/f5j1bg.png")';  }
    if(j.indexOf('j2') + 1) { document.write('Главный судья'); document.getElementById('mainz').style.backgroundImage = 'url("../img/bg/f5j2bg.png")';  }
    if(j.indexOf('j3') + 1) { document.write('судья &#10103;'); document.getElementById('mainz').style.backgroundImage = 'url("../img/bg/f5j3bg.png")';  }
</script></div>
  <div class="left-div" id="y" onclick="jclick2play();fadeIn(this.id);jxyes();">&nbsp;</div>
  <div class="right-div"  id="n" onclick="jclick2play();fadeIn(this.id);jxnot();">&nbsp;</div>
</div>
<hr>
<!-- get CONFIG var 4 judge and rendering-->
<?php $getAttInfo = new f5mngmt(); $getJconf = new f5config();
if($getJconf->getJudge_opt03(1)==1) {
echo '<sup class="att2j">ATT<small># </small>';?> <?php $getAttInfo->getTryNumNow(1); ?> <?php echo '&nbsp;</sup>';
}
if($getJconf->getJudge_opt04(1)==1) {
echo '<span class="container2tryNum">';?> <?php $getAttInfo->getWeightNow(1); echo "&#13199;"; ?> <?php echo '</span>';
}
echo '<div class="container1attName" onload="fadeIn(mainbody)">';
if($getJconf->getJudge_opt01(1)==1) {
echo '<sup class="attnum2j">';?> <?php $getAttInfo->getWhoopId(1);?> <?php echo '#</sup>';
}
if($getJconf->getJudge_opt02(1)==1) {
$getAttInfo->getWhoopFname1s(1); echo ". "; $getAttInfo->getWhoopSname(1);
}
?>
</div>
  </section>
  </div>

  </div>
    <div id="outspace" style="display: none">
    <div id='rulzzz'>
    <textarea style="display: none" id='log' name='log' readonly='readonly'></textarea>
    <input type='text' id='message' name='message' autofocus/>
    <input type='text' id='j1' name='message' />
    <input type='text' id='j2' name='message' />
    <input type='text' id='j3' name='message' />
    <input type='text' id='final' name='message' />
  </div>
  <input id="j11" type="button" value="j1yes" onclick="judasVotes('j11')">
  <input id="j18" type="button" value="j1def" onclick="judasVotes('j18')">
  <input id="j10" type="button" value="j1not" onclick="judasVotes('j10')"> |
  <input id="j21" type="button" value="j2yes" onclick="judasVotes('j21')">
  <input id="j28" type="button" value="j2def" onclick="judasVotes('j28')">
  <input id="j20" type="button" value="j2not" onclick="judasVotes('j20')"> |
  <input id="j31" type="button" value="j3yes" onclick="judasVotes('j31')">
  <input id="j38" type="button" value="j3def" onclick="judasVotes('j38')">
  <input id="j30" type="button" value="j3not" onclick="judasVotes('j30')">

    <audio id="j_chik_snd" src="../../snd/jchik.mp3" type="audio/mpeg">
    Your browser does not support the audio element.</audio>
  </div>


  </div>

</body>
</html>
