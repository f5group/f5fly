$(document).ready(function(){  
  $('#flagimg').css('opacity','0.2');
  document.getElementById('addModalInformTxt').style.display='none';
  //document.getElementById('outspace').style.display='none';
  document.getElementById('wcatDiv').style.display="none";
  document.getElementById('addModalInformTxt').style.display='none';
  document.getElementById('preloaderimg').style.display='none';
  $( '#topheader .navbar-nav a' ).on( 'click', function () {
    $( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
    $( this ).parent( 'li' ).addClass( 'active' );
  });    

$(function(){
  $.fn.editable.defaults.mode='inline';
  $.fn.editable.defaults.emptytext='...';
  $.fn.editable.defaults.savenochange=true;
  $.fn.editable.defaults.autotext='auto';
  $.fn.editable.defaults.showbuttons=true;
  $.fn.editable.defaults.inputclass ="j1p1w";
  //$.fn.editable.defaults.autotext = 'auto'; //$.fn.editable.defaults.enablefocus = 'true'; //$.fn.editable.defaults.onblur = 'onblur'; 
  $('#a_m666_kat1a').editable({ 
        enablefocus: true, 
        source: [
{text: "Men's category", children: [{value: 34, text: "34 kG"}, {value: 38, text: "38 kG"},{value: 42, text: "42 kG"},{value: 46, text: "46 kG"},{value: 50, text: "50 kG"},{value: 56, text: "56 kG"},{value: 62, text: "62 kG"},{value: 69, text: "69 kG"},{value: 6969, text: "+69 kG"},{value: 77, text: "77 kG"},{value: 85, text: "85 kG"},{value: 94, text: "94 kG"},{value: 9494, text: "+94 kG"},{value: 105, text: "105 kG"},{value: 105105, text: "+105 kG"} ]},
           ]
    });
$('#a_f666_kat1a').editable({ 
        enablefocus: true, 
        source: [
{text: "Woman's category", children: [{value: 34, text: "34 kG"}, {value: 36, text: "36 kG"},{value: 40, text: "40 kG"},{value: 44, text: "44 kG"},{value: 48, text: "48 kG"},{value: 53, text: "53 kG"},{value: 58, text: "58 kG"},{value: 63, text: "63 kG"},{value: 69, text: "69 kG"},{value: 6969, text: "+69 kG"},{value: 75, text: "75 kG"},{value: 90, text: "90 kG"},{value: 9090, text: "+90 kG"}]}
           ]
    });  
  $("#country1a").editable({ value: 1,source: [
    {value: 1, text: "RUS"},{value: 2, text: "KAZ"},{value: 3, text: "BEL"},{value: 4, text: "UKR"},{value: 5, text: "UZB"},{value: 6, text: "TJK"},{value: 7, text: "ARM"},{value: 8, text: "GRG"}
    ] });
  $("#name1a").editable({ url: "../core/model-db.php?get_name1=tmpChange"  });
  $("#name2a").editable({ url: "../core/model-db.php?get_name1=tmpChange"  });
  $("#j1i").editable({ url: "../core/model-db.php?get_name1=tmpChange"  });
  $("#p1i").editable({ url: "../core/model-db.php?get_name1=tmpChange"  });
}); 

$(function () {
    $('.btn-radio').click(function(e) {
        $('.btn-radio').not(this).removeClass('active')
        .siblings('input').prop('checked',false)
            .siblings('.img-radio').css('opacity','0.2');
      $(this).addClass('active')
            .siblings('input').prop('checked',true)
        .siblings('.img-radio').css('opacity','0.9');
    });
});


let timerIdpwdiv = setTimeout(function hide0pwdDiv() { document.getElementById('pwddiv').style.display='none'; timerId = setTimeout(hide0pwdDiv, 7234); }, 7234);

    function uploadFile(file){
    let url = '../core/f5uploader.php';
    let xhr = new XMLHttpRequest();
    let fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            console.log(xhr.responseText); // handle response.
        }
    };
    fd.append("upload_file", file);
    xhr.send(fd);
}


let uploadfiles = document.getElementById('image_uploads');
uploadfiles.addEventListener('change', function () {
    let files = this.files;
    for(let i=0; i<files.length; i++){
        uploadFile(this.files[i]); // call the function to upload the file
    }
}, false);


//let input = document.querySelector('input');
let input = document.getElementById('image_uploads');
let preview = document.querySelector('.preview');

//input.style.opacity = 0;

input.addEventListener('change', updateImageDisplay); function updateImageDisplay() {
  while(preview.firstChild) {
    preview.removeChild(preview.firstChild);
  }

  let curFiles = input.files;
  if(curFiles.length === 0) {
    let para = document.createElement('p');
    para.textContent = 'No files currently selected for upload';
    preview.appendChild(para);
  } else {
    let list = document.createElement('ol');
    preview.appendChild(list);
    for(let i = 0; i < curFiles.length; i++) {
      let listItem = document.createElement('li');
      let para = document.createElement('p');
      if(validFileType(curFiles[i])) {
        //para.textContent = 'File name ' + curFiles[i].name + ', file size ' + returnFileSize(curFiles[i].size) + '.';
        
        let image = document.createElement('img');
        image.src = window.URL.createObjectURL(curFiles[i]);
        image.setAttribute("id", "inf0");

        listItem.appendChild(image);
        listItem.appendChild(para);

      } else {
        para.textContent = 'File name ' + curFiles[i].name + ': Not a valid file type.';
        listItem.appendChild(para);
      }

      list.appendChild(listItem);
    }
  }
}

let fileTypes = [
  'image/jpeg',
  'image/pjpeg',
  'image/png'
]

function validFileType(file) {
  for(let i = 0; i < fileTypes.length; i++) {
    if(file.type === fileTypes[i]) {
      return true;
    }
  }

  return false;
}

function returnFileSize(number) {
  if(number < 1024) {
    return number + 'bytes';
  } else if(number > 1024 && number < 1048576) {
    return (number/1024).toFixed(1) + 'KB';
  } else if(number > 1048576) {
    return (number/1048576).toFixed(1) + 'MB';
  }
}

    });






/*
      function sh2the_flag() { 
      $( "#counryid li" ).click( function() {
      let ccode = this.id; ccode = '../img/flags/'+ccode+'.png';
      $( "#flag_country" ).prop('src', ccode);
    //console.log(ccode);
    })
}
*/

// Get the modal
let modal = document.getElementById('addAthletemd');
// Get the button that opens the modal
let btn = document.getElementById("addAthtelle");
// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];
let btnSave = document.getElementById("btnModalSaveadd");
// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
    let btn = document.getElementById("addAthtelle");
    let fadediv = document.getElementById("mainbody");
    fadediv.className = "blurBg";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
    let fadediv = document.getElementById("mainbody");
    fadediv.className -= "blurBg";
    fadediv.class   -= "blurBg";
}
// btnModalClose
btnSave.onclick = function() {
  setTimeout(function () { let url='m'; location.replace(url);  }, 1234);
  let loader = document.getElementById("preloaderimg");
    loader.style.display = "block";
    let fadediv     = document.getElementById("mainz");
    fadediv.className += "blurBg";
    //let fName1val   = document.getElementById("name1a").text;
    //let fName2val   = document.getElementById("name2a").text;
    //let j1val       = document.getElementById("j1i").text;
    //let p1val       = document.getElementById("p1i").text;
    //let sex         = document.getElementById("sex").text;
    
    let fName1val   = 'Name0';
    let fName2val   = 'Name1';
    let j1val       = '48';
    let p1val       = '58';
    let sex         = '1';
    let avatar      = document.getElementById("image_uploads");
    //let weight_cat  = document.getElementById("a_666_kat1a").text;
    let weight_cat ='77 KG';
    
    //if (document.getElementById('rsexmale_add').checked) sex = 1; //else sex = 0;
    //if (document.getElementById('rsexfele_add').checked) sex = 0;
    

    if (sex == 1) weight_cat = document.getElementById("a_m666_kat1a").text;
    if (sex == 0) weight_cat = document.getElementById("a_f666_kat1a").text;

    let bkpCmd      = 'fullbkp';
    //let input = document.getElementById('image_uploads');
    //let curFiles = input.files;
    /*
    if (curFiles.name==='undefined') {
  function getRandomFloat(min, max) {
let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}
getRandomFloat(1,17);

    }
else {
  function getRandomFloat(min, max) {
let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}
    let fname = getRandomFloat(1,5)+'.png';
  if (typeof(curFiles.name) == 'undefined') FileAvaName = fname; else let FileAvaName = curFiles[0].name;
}  
*/

function getRandomFloat(min, max) {
let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}
FileAvaName = getRandomFloat(1,5)+'.png';
$.get("../core/model-db.php", { "newBackup":bkpCmd,"set_sex": sex,"set_name1": fName1val,"set_name2": fName2val,"set_j1": j1val,"set_p1":p1val, "set_ava": FileAvaName, "weight_cat": weight_cat },
function(data) { console.log("response: " + data); });
                      //let fadediv = document.getElementById("addModalInformTxt");
                      //$('td').removeClass('selectednowtcico');
                      document.getElementById('addModalInformTxt').style.display='block';
                      $('#addModalInformTxt').addClass('blink_me');

}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        let fadediv = document.getElementById("mainbody");
        fadediv.className -= "blurBg";

    }
}
        function DropD(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('.dropd a');
        this.val = '';
        this.index = -1;
        this.initEvents();
      }
      DropD.prototype = {
        initEvents : function() {
          let obj = this;

          obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
          });

          obj.opts.on('click',function(){
            let opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
          });
        },
        getValue : function() {
          return this.val;
        },
        getIndex : function() {
          return this.index;
        }
      }

      $(function() {

        let dd = new DropD( $('#dd') );
      });