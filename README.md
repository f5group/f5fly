Мы любим то, что происходит! Что создаем, улучшаем и радуемся всему это все вместе! 

Мы, **f5fly**, спорт - это происходящее!

---
*Делаем это с лёгкостью и любовью!*
---
Некоторое отличия и возможности f5fly systems среди прочего:*

1. Отсутствие (минимум) сотен метров "лапши" **проводов**.
2. Отчетность PDF, карточки, протокола, команды, рекорды.
3. Сохраняем **природу!** Минимизируем расход бумаги.
4. Полная информативность! Зрители, атлеты, руководители, тренера.
5. Всегда online 24/7 - реально привлекаем в **СПОРТ**. *(it&finances)*
6. Доступная цена*! Гибкие условия в системе подпискок и версий.
7. Это очень просто! Экономим время, мозг :) **f5fly** - *работает начиная с 2012 года

[*актуальный набор на сайте f5fly.ru*](https://f5fly.ru/)

---




## editing

12345
---


Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).